<!-- Content Header (Page header) -->
<section class="content-header">
	<div class="headingdata" style="position: relative;">
		<h1 style=""><?=$propertydata->property_name?> Details</h1>
		<div class="prlogo" style="position: absolute; top: 10px; right: 55px; width: 200px; height: 150px; overflow: hidden; border: 1px solid black;">
			<img src="<?=LIVE_SITE_URL.'/images/logos/'.$propertydata->property_logo?>" style="width: 100%;"/>
		</div>
		<br style="clear: left;"/>
	</div>
	<?php if(isset($errors) && $errors == true): ?>
      <div class="alert alert-warning alert-dismissible" style="position: absolute; top: 5px; right: 10px;">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-warning"></i> Alert!</h4>
        There were one or more errors in your request. Please correct and resubmit!
      </div>		
	<?php endif; ?>
	<!--
	<ol class="breadcrumb">
	    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
	    <li class="active">Here</li>
	</ol>-->
</section>

<!-- Main content -->
<section class="content" style="min-height: 600px;">
<form id="frmpropertydetails" method="post" action="" enctype="multipart/form-data">

	<div class="left" style="float: left; width: 37%; margin-right: 16px;">
		<h3>Contact Details</h3>
		<div class="section1">
			<div class="form-group">
				<label for="address">Address&nbsp;<span class="errnotify"></span></label>
				<textarea name="address" class="form-control" id="address" rows="5" cols="50"><?php echo (isset($_POST['address']) ? $_POST['address'] : (isset($propertydata->property_address) ? $propertydata->property_address : '')) ?></textarea>
			</div>
			<div class="form-group">
				<label for="telephone">Telephone&nbsp;<span class="errnotify"></span></label>
				<input type="text" class="form-control" name="telephone" id="telephone" value="<?php echo (isset($_POST['telephone']) ? $_POST['telephone'] : (isset($propertydata->property_telephone) ? $propertydata->property_telephone : '')) ?>"/>
			</div>
			<div class="form-group">
				<label for="fax">Fax&nbsp;<span class="errnotify"></span></label>
				<input type="text" class="form-control" name="fax" id="fax" value="<?php echo (isset($_POST['fax']) ? $_POST['fax'] : (isset($propertydata->property_fax) ? $propertydata->property_fax : '')) ?>"/>
			</div>
			<div class="form-group">
				<label for="email">Email&nbsp;<span class="errnotify"></span></label>
				<input type="email" class="form-control" name="email" id="email" value="<?php echo (isset($_POST['email']) ? $_POST['email'] : (isset($propertydata->property_email) ? $propertydata->property_email : '')) ?>"/>
			</div>
			<div class="form-group">
				<label for="logofile">Logo&nbsp;<span class="errnotify"></span></label>
				<input type="file" name="logofile" id="logofile" />
				<p class="help-block">Example block-level help text here.</p>
			</div>
		</div>
	</div>
	<div class="right" style="float: left; width: 60%;">
		<h3>Google Map Coordinates</h3>
		<div class="section2">
			<div class="form-group" style="width: 250px;">
				<label for="longitude">Longitude&nbsp;<span class="errnotify"></span></label>
				<input type="text" class="form-control" name="longitude" id="longitude" value="<?php echo (isset($_POST['longitude']) ? $_POST['longitude'] : (isset($propertydata->property_map_longitude) ? $propertydata->property_map_longitude : '')) ?>"/>
			</div>
			<div class="form-group" style="width: 250px;">
				<label for="latitude">Latitude&nbsp;<span class="errnotify"></span></label>
				<input type="text" class="form-control" name="latitude" id="latitude" value="<?php echo (isset($_POST['latitude']) ? $_POST['latitude'] : (isset($propertydata->property_map_latitude) ? $propertydata->property_map_latitude : '')) ?>"/>
			</div>

			<div id="gmappreview" style="width: 94%; height: 251px; /*background: gray;*/ margin-top: 36px;">
			<!--<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15844.459500556344!2d79.891789!3d6.876838!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x340e6b6e616d820b!2s3CS!5e0!3m2!1sen!2slk!4v1491300751342" width="610" height="251" frameborder="0" style="border:0" allowfullscreen></iframe>-->
			</div>
			<div class="field" style="margin-top: 20px;">
				<input type="hidden" name="propertyid" value="<?=$propertydata->id?>"/>
				<input type="submit" class="btn btn-primary" name="update" value="Update Details"/>
			</div>
		</div>
	</div>
</form>
</section>
<!-- /.content -->