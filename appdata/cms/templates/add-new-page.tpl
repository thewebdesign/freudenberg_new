<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><?=$pageTitle?></h1>
  <!--
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
  -->
</section>

<!-- Main content -->
<section class="content">
<?php if($errors): ?>
<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<h4><i class="icon fa fa-ban"></i> Alert!</h4>
	<ul style="list-style: none; padding-left: 0px;">
	<?php foreach($errmsg as $err): ?>
		<li><?=$err?></li>
	<?php endforeach; ?>
	</ul>
</div>	
<?php elseif(isset($_GET['pg'])): ?>
<div class="alert alert-success alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<h4><i class="icon fa fa-check"></i> Alert!</h4>
	A new page '<?=htmlspecialchars(urldecode($_GET['pg']), ENT_QUOTES)?>' is added successfully!
</div>
<?php endif; ?>
<div class="box box-primary">
<div class="box-header with-border">
	<h3 class="box-title">Add a new page</h3>
</div>
<form method="post" action="" role="form">
	<div class="box-body">
		<div class="form-group">
			<label for="pagename">Page Name</label>
			<input type="text" class="form-control" id="pagename" name="pagename" value="<?=(isset($_POST['pagename']) ? htmlspecialchars($_POST['pagename'], ENT_QUOTES) : '')?>" placeholder="Page Name" />
		</div>
		<div class="form-group">
			<label for="pageslug">Page Slug</label>
			<input type="text" class="form-control" id="pageslug" name="pageslug" value="<?=(isset($_POST['pageslug']) ? htmlspecialchars($_POST['pageslug'], ENT_QUOTES) : '')?>" placeholder="Page Slug" />
		</div>
		<div class="form-group">
			<label for="pagetemplate">Page Template</label>
			<select class="form-control" name="pagetemplate" id="pagetemplate" disabled>
					<?php foreach($tplfiles as $file): ?>
						<option value="<?=$file?>" <?= ($file == 'standard-page.tpl' ? 'selected="selected"' : '')?>><?=$file?></option>
					<?php endforeach; ?>
			</select>
		</div>
		<div class="form-group">
			<div class="checkbox">
				<label>
					<input type="checkbox" name="pgoptions[]" value="desc" checked="checked" disabled/>
					Description Section
				</label>
			</div>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="pgoptions[]" value="nav"/>
					Navigation Section
				</label>
			</div>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="pgoptions[]" id="pgslideropt" value="slider"/>
					Page Slider (Top)
				</label>
			</div>
			<div class="slider-option-wrapper">
                <div class="form-group" style="padding-left: 20px;">
                  <div class="radio">
                    <label>
                      <input type="radio" name="slideropt" id="slideropt1" value="1" checked>
                      <span style="vertical-align: middle;">Default Slider</span>
                    </label>
                  </div>
                  <div class="radio">
                    <label>
                      <input type="radio" name="slideropt" id="slideropt2" value="2">
                      <span style="vertical-align: middle;">Custom Slider</span>
                    </label>
                  </div>
                </div>				
			</div>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="pgoptions[]" value="gallery"/>
					Image Gallery
				</label>
			</div>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="pgoptions[]" value="sub"/>
					Allow Sub Pages
				</label>
			</div>			
		</div>
	</div>
	<!-- /.box-body -->
	<div class="box-footer">
		<?php if(isset($parentid)): ?>
		<input type="hidden" name="parentpg" value="<?=$parentid?>"/>
		<?php endif; ?>
		<input type="hidden" name="pgoptions[]" value="desc"/>
		<input type="hidden" name="pagetemplate" value="standard-page.tpl"/>
		<button type="submit" name="submit" class="btn btn-primary">Add Page!</button>
	</div>
	</form>
</div>
</section>
<!-- /.content -->