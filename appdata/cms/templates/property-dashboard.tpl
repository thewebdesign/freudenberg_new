<?php if(isset($_GET['pageadded']) && isset($_SESSION['_msg'])): ?>
	<div style="width: 50%; padding: 5px; background: green; color: red; height: 100px;">
		<?=$_SESSION['_msg']?>
	</div>
<?php endif; ?>

<h1><?=$pagetitle?></h1>
<div class="wrapper">
	<div class="left" style="float: left; width: 40%;">
		<h3>Update Contact Details</h3>
		<div class="section1">
			<form>
				<table>
					<tr>
						<td>
							<label for="address">Address</label>
							<br>
							<textarea name="address" id="address" rows="5" cols="50"></textarea>
						</td>
					</tr>
					<tr>
						<td>
							<label for="telephone">Telephone</label>
							<br>
							<input type="text" name="telephone" id="telephone" value=""/>
						</td>
					</tr>
					<tr>
						<td>
							<label for="fax">Fax</label>
							<br>
							<input type="text" name="fax" id="fax" value=""/>
						</td>						
					</tr>
					<tr>
						<td>
							<label for="email">Email</label>
							<br>
							<input type="text" name="email" id="email" value=""/>
						</td>					
					</tr>
					<tr>
						<td>
							<label for="logofile">Logo</label>
							<br>
							<input type="file" name="logofile" id="logofile" />
						</td>
					</tr>
					<tr>
						<td><button type="button" name="update">Update</button></td>
					</tr>
				</table>
			</form>
		</div>		
	</div>
	<div class="right" style="float: left; width: 60%;">
		<h3>Update Map Location</h3>
		<div class="section2">
			<form>
				<table>
					<tr>
						<td>
							<label for="longitude">Longitude</label>
							<br>
							<input type="text" name="longitude" id="longitude" value=""/>
						</td>					
					</tr>
					<tr>
						<td>
							<label for="latitude">Latitude</label>
							<br>
							<input type="text" name="latitude" id="latitude" value=""/>
						</td>					
					</tr>
					<tr>
						<td><button type="button" name="update2">Update</button></td>
					</tr>														
				</table>
			</form>
		</div>
		<div class="map_preview">
			map preview
		</div>		
	</div>
</div>