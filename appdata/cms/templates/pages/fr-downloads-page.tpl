<div class="tab">
  <button class="tablinks" onclick="openTab(event, 'Randholee')">Randholee</button>
  <button class="tablinks" onclick="openTab(event, 'Firs')">Firs</button>
  <button class="tablinks" onclick="openTab(event, 'Ellens')">Ellens</button>
  <button class="tablinks" style="float: right; color: white; background: green;" onclick="window.location = '<?=SITE_URL.'/manage/'.$property_slug.'/addpage/'.urlencode(base64_encode($pageid))?>'">+ Add Sub Page</button>
</div>

<div id="Randholee" class="tabcontent">
  <h1>Manage <?=$pagedata->page_name?> Page Contents for Randholee</h1>
  <form id="desc">
  	<table>
  		<tr>
  			<td><label for="randholee-file-name">Select File To Upload:</label></td>
  			<td><input type="file" name="randholee-file-name" id="randholee-file-name" value=""/></td>
  		</tr>
  		<tr>
   			<td><label for="randholee-file-title">File Title</label></td>
  			<td><input type="text" name="randholee-file-title" id="randholee-file-title" value=""/></td> 		
  		</tr> 
  		<tr>
  			<td></td>
  			<td><button type="button" id="updbtn" name="updbtn">Upload</button></td>
  		</tr> 		
  	</table>
  </form>
</div>

<div id="Firs" class="tabcontent">
  <h1>Manage <?=$pagedata->page_name?> Page Contents for Firs</h1>
  <form id="desc">
    <table>
      <tr>
        <td><label for="firs-file-name">Select File To Upload:</label></td>
        <td><input type="file" name="firs-file-name" id="firs-file-name" value=""/></td>
      </tr>
      <tr>
        <td><label for="firs-file-title">File Title</label></td>
        <td><input type="text" name="firs-file-title" id="firs-file-title" value=""/></td>    
      </tr> 
      <tr>
        <td></td>
        <td><button type="button" id="updbtn" name="updbtn">Upload</button></td>
      </tr>     
    </table>
  </form>
</div>

<div id="Ellens" class="tabcontent">
  <h1>Manage <?=$pagedata->page_name?> Page Contents for Ellens</h1>
  <form id="desc">
    <table>
      <tr>
        <td><label for="firs-file-name">Select File To Upload:</label></td>
        <td><input type="file" name="firs-file-name" id="firs-file-name" value=""/></td>
      </tr>
      <tr>
        <td><label for="firs-file-title">File Title</label></td>
        <td><input type="text" name="firs-file-title" id="firs-file-title" value=""/></td>    
      </tr> 
      <tr>
        <td></td>
        <td><button type="button" id="updbtn" name="updbtn">Upload</button></td>
      </tr>     
    </table>
  </form>
</div>


<!--
<div id="Tokyo" class="tabcontent">
  <h3>Tokyo</h3>
  <p>Tokyo is the capital of Japan.</p>
</div>-->