<?php $token = NoCSRF::generate('token'); ?>

<nav id="nav-primary-wrapper" role="navigation">
    <a class="skiptomain" href="#main">Skip to Main Content</a>
    <div class="logo"><a href="" class="logo_bg">Ellen's Place - Home</a></div>
    <div class="mobile-toggle">MENU<span></span></div>    
    <ul class="nav-primary">


        <div class="accordion">
            <dl>
                <dt><a href="" aria-controls="accordion1" class="accordion-title accordionTitle">Home</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion1" aria-hidden="true">&nbsp;</dd>

                <dt>
                    <a href="accommodation" aria-expanded="false" aria-controls="accordion2" class="accordion-title accordionTitle js-accordionTrigger">Accommodation</a>
                </dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion2" aria-hidden="true">
                <li><a href="accommodation/super-deluxe-rooms">Super Deluxe</a></li>
                <li><a href="accommodation/deluxe-rooms">Deluxe</a></li>
                <li><a href="accommodation/standard-rooms">Standard</a></li>
                </dd>

                <dt><a href="facilities" aria-controls="accordion5" class="accordion-title accordionTitle js-accordionTrigger">Facilities</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion5" aria-hidden="true">
                <li><a href="facilities/restaurant">Restaurant</a></li>
                <li><a href="facilities/swimming-pool">Swimming Pool</a></li>
                </dd>

                <dt><a href="promotions" aria-controls="accordion4" class="accordion-title accordionTitle">Promotions</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion4" aria-hidden="true">&nbsp;</dd>

                <dt><a href="gallery" aria-controls="accordion6" class="accordion-title accordionTitle">Gallery</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion6" aria-hidden="true">&nbsp;</dd>

                <dt><a href="location" aria-controls="accordion6" class="accordion-title accordionTitle">Location</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion6" aria-hidden="true">&nbsp;</dd>

                <dt><a href="about-us" aria-controls="accordion6" class="accordion-title accordionTitle">About Us</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion6" aria-hidden="true">&nbsp;</dd>

                <dt><a href="contact-us" aria-controls="accordion7" class="accordion-title accordionTitle">Contact Us</a></dt>
                <dd class="accordion-content accordionItem is-collapsed" id="accordion7" aria-hidden="true">&nbsp;</dd> 
                <div class="search">
                    <form>
                        <input name="search" type="text" size="15" class="input">
                        <input type="submit" class="submit" value="SEARCH" style="background:none; border: none;">
                        <div class="clear"></div>
                    </form>
                </div>         
            </dl>
        </div>



        <li class="prime-main-link active-here ddd"><a href="">Home</a></li>   
        <li class="prime-main-link active-act ddd"><a href="accommodation">Accommodation</a>
            <ul>
                <li><a href="accommodation/super-deluxe-rooms">Super Deluxe</a></li>
                <li><a href="accommodation/deluxe-rooms">Deluxe</a></li>
                <li><a href="accommodation/standard-rooms">Standard</a></li>
            </ul>
        </li>
        <li class="prime-main-link active-act ddd"><a href="facilities">Facilities</a>
            <ul>
                <li><a href="facilities/restaurant">Restaurant</a></li>
                <li><a href="facilities/swimming-pool">Swimming Pool</a></li>
            </ul>
        </li>   
        <li class="prime-main-link active-accom ddd"><a href="promotions">Promotions</a></li>  
        <li class="nav-logo-center"><a href="">Ellen's Place - Home</a></li>      
        <li class="prime-main-link active-accom ddd"><a href="gallery">Gallery</a></li>
        <li class="prime-main-link active-accom ddd"><a href="location">Location</a></li>
        <li class="prime-main-link active-resort ddd"><a href="about-us">About Us</a></li>
        <li class="prime-main-link active-here ddd"><a href="contact-us">Contact Us</a></li>
        <li class="prime-quick-link quick-link-contact nav-cont-no ddd">E-mail: info@ellensplace.lk &nbsp;&nbsp;&nbsp; Tel: +94 112 680 062</li>
    </ul>
    <ul class="prime-quick-link quick_nav">

        <li class="prime-quick-link quick-link-contact ddd">
            <div class="search">
                <form method="POST" action="search-results.php">
                    <input name="search" type="text" size="15" class="input" required>
                    <input type="hidden" name="token" value="<?php echo $token ?>">
                    <input type="submit" class="submit" value="SEARCH" style="background:none; border: none;">
                    <div class="clear"></div>
                </form>
            </div>
        </li>

        <li class="prime-quick-link quick-link-contact logo_main"><a href="<?php echo MAIN_URL ?>"><img src="<?php echo HTTP_PATH ?>assets/img/logo_main.png" /></a></li>

        <div class="prime-quick-link quick-link-contact lanuge ddd">
            <div id="google_translate_element"></div>
            <script type="text/javascript">
                function googleTranslateElementInit() {
                    new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL, autoDisplay: false}, 'google_translate_element');
                }
            </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
        </div>       
    </ul>
</nav>
