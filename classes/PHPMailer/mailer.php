<?php

require_once 'class.phpmailer.php';

/**
 * A helper function to send an email
 * @param  Array or String  $to        Recipiention email address(es)
 * @param  string  $subject   Email Subject
 * @param  string  $body      Email Body
 * @param  String $from      Overwriting default from email address
 * @param  String $reply     Overwriting default replay_to address
 * @param  Array or String $attchment Has any attachment(s)
 * @return Boolean               Success or Faild
 *
 * @author Mifas
 */
function send_email($to, $subject = 'Subject' ,$body = '' ,$from = false ,$reply = false ,$attchment = false)
{

  $CI =& get_instance();

  $CI->load->library('email');
  $email_config = $CI->config->item('email_config');


    if( is_array($from) )
    {
      $CI->email->from(key($from), current($from));
    }
    else if( filter_var($from,FILTER_VALIDATE_EMAIL) )
    {
      $CI->email->from($from);
    }
    else
    {
      if( $email_config['use_email_name'] && is_array($email_config['from']) )
      {
        $CI->email->from(key($email_config['from']), current($email_config['from']));
      }
      else
      {
        $CI->email->from($email_config['from']);
      }
    }


    if( is_array($reply) )
    {
      $CI->email->from(key($reply), current($reply));
    }
    else if( filter_var($reply,FILTER_VALIDATE_EMAIL) )
    {
      $CI->email->from($reply);
    }
    else
    {
      if( $email_config['use_email_name'] && is_array($email_config['reply_to']) )
      {
        $CI->email->reply_to(key($email_config['reply_to']), current($email_config['reply_to']));
      }
      else
      {
        $CI->email->reply_to( $email_config['reply_to'] );
      }
    }

    if( ! is_array($attchment) )
      $attchment = array($attchment);



    foreach( $attchment as $file )
    {
      if( $file && is_file($file) && file_exists($file) && is_readable($file) )
      {
        $CI->email->attach($file);
      }
    }


    if( $email_config['use_smpt'] )
    {
      $CI->email->smtp_host = $email_config['host'];
      $CI->email->smtp_user = $email_config['username'];
      $CI->email->smtp_pass = $email_config['password'];
      // $CI->email->smtp_port = $email_config['host'];
    }


  if( is_array($to) )
  {
    foreach( $to as $email )
    {
      $CI->email->to($email);
    }
  }
  else
  {
    $CI->email->to($to);
  }

  $CI->email->bcc($email_config['email_list_bcc'],50);

  $CI->email->set_mailtype('html');



  // $CI->email->cc('another@another-example.com');
  // $CI->email->bcc('them@their-example.com');

  $CI->email->subject($subject);
  $CI->email->message($body);

  $CI->email->send();

  // echo $CI->email->print_debugger();

}
