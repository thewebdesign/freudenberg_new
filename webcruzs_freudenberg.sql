-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 14, 2019 at 09:06 AM
-- Server version: 10.1.40-MariaDB-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `webcruzs_freudenberg`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbldescriptions`
--

CREATE TABLE `tbldescriptions` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `sub_title` varchar(255) DEFAULT NULL,
  `main_title` varchar(255) DEFAULT NULL,
  `body_text` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbldescriptions`
--

INSERT INTO `tbldescriptions` (`id`, `page_id`, `sub_title`, `main_title`, `body_text`) VALUES
(4, 4, '', 'Experiences', '<p style=\"text-align:justify; font-size:17px;\">Our three indulgence way outs pack a potent dose in terms of unique experiences won’t be anywhere else while enjoying the best hospitality and world class service. When you are staying in the dense, high-rise city of Colombo at Ellen’s Place you can visit Royal Colombo Golf club for a round of golf or enjoy panoramic views and wonderful photo opportunities, shopping and nightlife with a joyful mix of cultures and cuisines. One good way to experience the splendor of Kandy is to stay at the Randholee Resort, a great entry point to view Kandy Perehera, the Temple of the Tooth Relic, the Royal Botanical Gardens or the Hantane Mountain Range. Lastly, for a totally different experience to your urban adventure you must stay at Firs Bungalow in Nuwara Eliya to use the Victoria Golf Course, go horse riding, visit the Hakgala Botanical Gardens, hike on Horton Plains or take in the breathtaking views of World’s End.</p>'),
(7, 1, 'FREUDENBERG LEISURE', 'Upscale Small Hotel Experiences', '<p style=\"text-align:justify; font-size:17px;\">Freudenberg Leisure, with a chain of small upscale hotels in Colombo, the capital of Sri Lanka, Kandy, the UNESCO nominated world heritage&nbsp;city and Nuwara Eliya, the &lsquo;city on the plain&rsquo; or &lsquo;city of light&rsquo; offers one-of-a-kind experiences. Come to us with your wish list, our hotels will fulfill all your needs.</p><p style=\"text-align:justify; font-size:17px;\">The Randholee Resort and spa&nbsp;is an ideal choice for a fun family holiday, honeymoon getaways or business retreat for discerning modern travelers. Fascinating, unruffled and peaceful, the Firs Nuwara Eliya offers the travelers, serenity and indulgence for a relaxing holiday. Ellen&rsquo;s Place, named after Ellen Senanayake, a leader in Sri Lanka&rsquo;s independence struggle, is a upscale hotel in Colombo with unique and delightful facilities of a home away from home.</p>'),
(8, 6, 'ROOMS, SUITES & PRIVATE VILLAS', 'Accommodation', '<p style=\"text-align:justify; font-size:16px;\">We offer delightful accommodation facilities in Colombo, Kandy and Nuwara Eliya. Contact us to arrange a round-trip package  if you want to stay in one or more cities during  your visit to Sri Lanka. Enjoy the rooms and suites at Ellen’s Place in Colombo, at the Randholee Resort or suites at Firs.</p>'),
(9, 7, '', 'About Us', '<p style=\"text-align:justify; font-size:16px;\">Freudenberg Leisure, one of Sri Lanka’s top hospitality groups has grown significantly from its first hotel in Kandy to its current portfolio of three small upscale hotels in Colombo, Kandy and Nuwara Eliya. With service at the heart of our hospitality, we endeavour to create unique and memorable travel experiences, and to bring joy to our guests.                                   \r\n<br><br>\r\nThe Randholee Resort in Kandy, is a perfect honeymoon getaway and the ideal hotel to stay at for the Kandy Perehera. Firs is a 5 suite bungalow in Nuwara Eliya. It the former holiday bungalow of Sri Lanka’s first Prime Minister, Hon. D. S. Senanayake. Ellen’s Place, named after Ellen Senanayake, a leader in Sri Lanka’s independence struggle, is an upscale boutique hotel in Colombo, located in close proximity to the Royal Colombo Golf Club.                                      \r\n<br><br>\r\nRandholee Resort in Kandy provides unique opportunities to view the Kandy Esala Perahera and  Sri Dalada Maligawa, the Temple of the Sacred Tooth Relic.\r\n</p>'),
(10, 24, 'T one content sub', 'T one maIn', 'T one BoDy teXtdd'),
(11, 40, 'fds', 'sdff', '<p>ddsssssssssssfsdfsff fsd sdff&nbsp;</p><p><br></p><p><br></p><p><br></p>'),
(12, 43, 'RANDHOLEE RESORT AND SPA', '“Perfect Hide-Out For Those Looking For<br>Rest & Relaxation!”', '<p style=\"text-align: justify;\">Randholeee Resort features a unique blend of culture, cuisine and corporate comforts at the melting pot of cultural diversity and a rich heritage in Kandy an ideal destination for a fun family holiday or business retreat. It is located in the backdrop of fascinating mountains at the heart of this world heritage city. This resort is close to the sacred temple of the tooth relic, Sri Dalada Maligawa and several ancient and historic temples including Gadaladeniya and Lankatillake temples and Peradeniya Botanical Gardens. Randholee Resort also offers boat rides in Kandy Lake and nature trails. It provides body and beauty treatments and visitors can take advantage of recreational amenities as well.</p>'),
(13, 44, 'HONEYMOON OF A LIFETIME', 'Honeymoon', '<p style=\"text-align: justify;\"><span style=\"font-size: 18px;\">The excellent mix of adventure and relaxation at Randholee Luxury Resort makes the perfect honeymoon combination. Begin your honeymoon in the hill country city of Kandy; here you can discover the temple of sacred tooth relic, remote villages and lush jungle clad mountains. This scenic honeymoon&rsquo;s a classic! Randholee is a wonderful place to start your vacation with your beloved, and the best place in the paradise island that should be on everyone&rsquo;s honeymoon wish list!</span></p>\r\n<h1><span style=\"font-size: 13px; color: #e25041;\">PACKAGE INCLUDES:</span></h1>\r\n<ul>\r\n<li><span style=\"font-size: 14px;\">Exotic welcome cocktail/mocktail on arrival.</span></li>\r\n<li><span style=\"font-size: 14px;\">Framed honeymoon photograph or a romantic souvenir will be given as memorabilia prior to departure from the hotel.</span></li>\r\n<li><span style=\"font-size: 14px;\">A private candlelit four-course dinner will be served by the pool (with an option of ordering dinner from the special &lsquo;Honeymooner&rsquo;s ala carte\'</span></li>\r\n<li><span style=\"font-size: 14px;\">A one-year anniversary dinner and special concessions.</span></li>\r\n<li><span style=\"font-size: 14px;\">A bottle of wine will be served during dinner.</span></li>\r\n<li><span style=\"font-size: 14px;\">Decorated room with scented candles &amp; Chocolate coated strawberries.</span></li>\r\n<li><span style=\"font-size: 14px;\">Liqueur scented evening chocolates.</span></li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>'),
(14, 45, '', 'Infinity Pool', '<p>An endless stretch of water merges into the sky to make you feel as though you are swimming in the clouds. Immerse yourself in the refreshing water and spend a nice, relaxing day in the pool. When mouth-watering aromas waft by from the nearby dining area, order refreshments and enjoy them besides the pool. The elegant pool-side is filled with chaise longues and fresh towels for your comfort.</p>'),
(15, 47, 'ACCOMMODATION', 'Deluxe Mountain View & Non View', '<p style=\"text-align: justify;\">Step into an extraordinary world fashioned for uninterrupted peace. Nestled in the heart of Mount Pleasant and surrounded by its beauty, the views offered from our rooms are nothing short of breathtaking. The earthy tones of the rooms, the Kandyan paintings and the rich mahogany furniture add to this enchanting atmosphere. The palatial beds are fitted with soft cotton sheets that are cool and crisp to the touch. Our spacious bathrooms include a large bathtub and a commodious counter for your comfort. Equipped with amenities such as cable television and tea and coffee making facilities, the rooms combine the opulence of a palace with the comforts of home.</p>'),
(16, 48, '', 'Super Deluxe', '<p style=\"text-align: justify; font-size: 18px;\">Relax in the contemporary comforts of your new rooms coloured with black, pink and red wall pars, a bright and airy haven to uplift your body and spirit with a panoramic view of mountains. Luxuriate in the deep-soaking bathtub, and relax with an array of convenient amenities including free WiFi and room service on request. Enjoy a fantastic slice of Sri Lanka&rsquo;s heritage city of Kandy&ndash;skyline.<br /><br /> Carefully designed with modern facilities this sanctuary is the perfect combination of convenience and comfort, offering all your living needs in one space. The Room has a king size bed, unwind on the plush sofa for movie night with two 48-inch LCD TV, satellite/cable channels , then indulge in a relaxing bathtub soak before retreating under the covers to refresh &mdash; there&rsquo;s plenty to explore in and around Randhoolee resort</p>\r\n<p>&nbsp;</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 13px;\">ROOM INCLUDES:</h1>\r\n<p style=\"text-align: left; font-size: 13px;\">King Size Bed<br />Panoramic view of Mountains</p>\r\n<ul class=\"accom-list\" style=\"float: none !important;\">\r\n<li>Tea and coffee making facilities</li>\r\n<li>Writing table</li>\r\n<li>Minibar</li>\r\n<li>Safety box</li>\r\n<li>220V electrical sockets</li>\r\n<li>Private Balcony</li>\r\n<li>Separate sitting area with a coffee table</li>\r\n<li>Wall to wall carpet</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 13px;\">Communication and Technology:</h1>\r\n<ul class=\"accom-list\" style=\"float: none !important;\">\r\n<li>Free WiFi</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 13px;\">Entertainment:</h1>\r\n<ul class=\"accom-list\" style=\"float: none !important;\">\r\n<li>Two 48-inch LCD TVs</li>\r\n<li>Satellite/cable channels</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 13px;\">Bathroom Facilities:</h1>\r\n<ul class=\"accom-list\" style=\"float: none !important;\">\r\n<li>Spacious s bathroom</li>\r\n<li>Jacuzzi bathtub and walk-in shower</li>\r\n<li>Hair dryer</li>\r\n<li>Shaver point</li>\r\n<li>Bathrobe and slippers</li>\r\n</ul>\r\n<p>&nbsp;</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 13px;\">Housekeeping and In-room services:</h1>\r\n<ul class=\"accom-list\" style=\"float: none !important;\">\r\n<li>Complimentary bottled water</li>\r\n<li>Daily housekeeping service</li>\r\n<li>Fruit Basket and Chocolates</li>\r\n<li>DVD player on request</li>\r\n<li>Umbrella on request</li>\r\n<li>Ironing facilities on request</li>\r\n</ul>'),
(17, 49, '', 'Deluxe Mountain View', '<p style=\"text-align:justify; font-size:18px;\">Step into an extraordinary world fashioned for uninterrupted peace. Nestled in the heart of Mount Pleasant and surrounded by its beauty, the views offered from our rooms are nothing short of breathtaking. The earthy tones of the rooms, the Kandyan paintings and the rich mahogany furniture add to this enchanting atmosphere. <br />The palatial beds are fitted with soft cotton sheets that are cool and crisp to the touch. Our spacious bathrooms include a large bathtub and a commodious counter for your comfort. Equipped with amenities such as cable television, an international phone line and tea and coffee making facilities, the rooms combine the opulence of  a palace with the comforts of home.</p><br>\r\n\r\n                                <h1 class=\"hdr-seven\" style=\"text-align:left; font-size:13px;\"> ROOM INCLUDES:</h1>\r\n                                <ul class=\"accom-list\">\r\n                                    <li>Air-conditioning</li>\r\n                                    <li>Tea and coffee making facilities</li>\r\n                                    <li>In-room dining</li>\r\n                                    <li>Colour TV with cable</li>\r\n                                    <li>DVD Player (on request)</li>\r\n                                    <li>In-room safe</li>\r\n                                    <li>Writing tables and chairs</li>\r\n                                    <li>Bathroom with tub and shower</li>\r\n                                    <li>Hot and cold water</li>\r\n                                </ul>\r\n                                <ul class=\"accom-list\">\r\n                                    <li>Wireless Internet Access/ ADSL</li>\r\n                                    <li>Laundry service</li>\r\n                                    <li>Baby cot available (on request)</li>\r\n                                    <li>Wardrobe with hangers</li>\r\n                                    <li>Iron and board available (on request)</li>\r\n                                    <li>Hair-dryer available (on request)</li>\r\n                                </ul>'),
(18, 50, '', 'Standard ', '<p style=\"text-align: justify; font-size: 18px;\">Step into an extraordinary world fashioned for uninterrupted peace. Nestled in the heart of Mount Pleasant and surrounded by its beauty. The earthy tones of the rooms, the Kandyan paintings and the rich mahogany furniture add to this enchanting atmosphere. The palatial beds are fitted with soft cotton sheets that are cool and crisp to the touch. Our spacious bathrooms include a large bathtub and a commodious counter for your comfort.</p>\r\n<p>&nbsp;</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 13px;\">ROOM INCLUDES:</h1>\r\n<ul class=\"accom-list\">\r\n<li>Air-conditioning</li>\r\n<li>Tea and coffee making facilities</li>\r\n<li>In-room dining</li>\r\n<li>Colour TV with cable</li>\r\n<li>DVD Player (on request)</li>\r\n<li>In-room safe</li>\r\n<li>Writing tables and chairs</li>\r\n<li>Bathroom with tub and shower</li>\r\n<li>Hot and cold water</li>\r\n</ul>\r\n<ul class=\"accom-list\">\r\n<li>Wireless Internet Access/ ADSL</li>\r\n<li>Laundry service</li>\r\n<li>Baby cot available (on request)</li>\r\n<li>Wardrobe with hangers</li>\r\n<li>Iron and board available (on request)</li>\r\n<li>Hair-dryer available (on request)</li>\r\n</ul>'),
(19, 51, '', 'Cuisine', '<p align=\"justify\">Dining at Randholee Luxury Resort is a sumptuous experience fit for royalty. Savour fragrant rice from India, creamy pasta from Italy or soft couscous from the Middle East during our International Buffet nights. Our Sri Lankan buffet allows guests to experience the texture and taste of traditional foods rich with spices and coconut milk. If you are in the mood for something unique, order from our a la carte menu which has an array of local and international choices.</p>\r\n\r\n<p align=\"justify\">During the weekends, a variety of theme nights are organized. Among them, guest favourites include Barbecue Nights, Sri Lankan Nights and Mongolian Nights. Theme nights, weather permitting, are usually held by the pool and include a calypso band.</p>'),
(20, 52, '', 'Bar', '<p style=\"text-align:justify; font-size:18px;\">The bar at Randholee Resort is an ideal place to relax, watch TV or rendezvous with guests from all over the world. Decorated with simple furniture, the bar is informal and comfortable. Housing a variety of beer, local liquor and foreign liquor, the bar is the perfect place to enjoy an insouciant evening with friends. Sip a beer and watch a sporting event on the 42-inch flat screen TV or have a lively chat with guests from different countries. </p>'),
(21, 53, '', 'Signature Dining', '<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:17px; padding:10px\"><strong>Dining with Nature</strong></h1>\r\n<p style=\"text-align:justify; font-size:18px;\">Have a picnic in a mystical Kandyan forest. Surround yourself with the beautiful colours of nature - the greens and browns of the trees and shrubs, the blues and whites of the skies and clouds and the yellows and reds of the various birds and other woodland creatures that can be found in the forest. Pick an isolated location to spread your blanket and enjoy a day of good food, relaxation and nature. Experience peace and tranquillity amidst the Kandy woods.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:17px;padding:10px\"><strong>Dining Pool-side</strong></h1>\r\n<p style=\"text-align:justify; font-size:18px;\">Watch the reflection of the stars shimmer on the water as you dine beside the pool. Titillate the most discerning palate with a candle - lit dinner as the sun sets over the distant hills. Have dessert as the sky darkens and the lights of the homes in the village below turn on one by one. Dining beside the pool, on top of Mount Pleasant, is a romantic experience that will become a treasured memory. </p>'),
(22, 54, '', 'Buffet', '<p style=\"text-align:justify; font-size:18px;\">Dining at Randholee Resort is a sumptuous experience fit for royalty. Savour fragrant rice from India, creamy pasta from Italy or soft couscous from the Middle East during our International Buffet nights. Our Sri Lankan buffet allows guests to experience the texture and taste of traditional foods rich with spices and coconut milk. If you are in the mood for something unique, order from our a la carte menu which has an array of local and international choices.</p>'),
(23, 55, '', 'Facilities', '<p style=\"text-align:justify; font-size:17px;\">Randholee Luxury Resort offers various facilities to make your stay with us more pleasurable. Work out in the gym, swim a lap in the pool or schedule a massage at the Ayurveda spa. Our sophisticated Business Centre includes computers with high speed Internet access and the entire premise of the hotel is wi-fi enabled so that you may browse the Internet from any corner of the hotel. Hike up a misty mountains during the day and spend your evenings playing a game of darts or pool in the comforts of the hotel. Should you run out of local currency, head straight to our Currency Exchange Centre where you can buy local currency at competitive bank rates. With so many facilities within the hotel you will never want to check-out.</p>'),
(24, 56, '', 'Squash Court', '<p style=\"text-align:justify; font-size:16px;\">Randholee offers guests a state-of-the-art squash court ideal for both training and matches. The glass-backed, indoor court is fully equipped with racquets, balls and gear, and features an ample viewing area to facilitate sporting events. The services of a personal trainer are also offered by the hotel to beginners.\r\n<br>\r\n<br>\r\nSpend a few minutes practising your serve or sportily challenge another guest to a friendly match. This popular activity provides guests with an excellent opportunity to get to know each other and have fun.</p>'),
(25, 57, '', 'Badminton And Table Tennis', '<p style=\"text-align:justify; font-size:16px;\">Indulging in a game of badminton or table tennis is an excellent way to stay fit during your stay at Randholee. Ideal for family and friends, the table tennis area and the badminton court at the hotel are located in close proximity to each other for convenience and have been designed to offer both a stimulating and relaxing experience. Enjoy complimentary use of badminton racquets, table tennis paddles, shuttlecocks and ping pong balls offered by the hotel. Training can also be provided to beginners on request.</p>'),
(26, 58, '', 'Shopping Boutique', '<p style=\"text-align:justify; font-size:16px;\">Choose from an assortment of glamourous jewellery and gemstones at Randholee\'s gem boutique and take back a few mementos of your visit to Kandy.</p>'),
(27, 59, '', 'Spa', '<p style=\"text-align:justify; font-size:16px;\">Revel in a blissful sensory experience at the Randholee Spa as you awaken your inner spirit amidst a warm and nurturing environment. We offer an array of treatments, massages and rituals that have been designed to pamper the body and replenish the soul, leaving you blessed with a wonderful feeling of inner peace and harmony.<br><br> \r\nLuxuriate in a soothing warm Jacuzzi or simply succumb to the hands of our expert masseurs as you let them whisk you away to a state of inner harmony. Each moment spent at the spa will leave you feeling refreshed and fully invigorated. </p>'),
(28, 60, 'THE REMARKABLE CONFERENCE CENTRE WITH A RESPLENDENT VIEW.', 'Conferences And Workshops', '<p style=\"text-align:justify; font-size:16px;\">Randholee Resorts is a wonderful location for modest conferences and corporate events. The exceptional location and the brilliant Kandy climate contribute to the unique environment in which to set your conference. We offer outstanding services and extensive facilities to make your conference successful and memorable. We are able to accommodate approximately 50 people for various functions and the seating and hall arrangements can be altered to your specifications. The location, service and facilities we offer make our conference centre one of the finest venues available in Kandy.</p>'),
(29, 61, '', 'Fitness Centre', '<p style=\"text-align:justify; font-size:16px;\">Equipped with weight lifting machines, treadmills and steppers, Randholee Luxury Resort offers you a state-of-the-art gym to get a fabulous workout during your holidays.</p>'),
(30, 62, '', 'Business Centre', '<p style=\"text-align:justify; font-size:16px;\">Randholee Resort offers our esteemed guests various facilities to utilize when conducting business from our hotel.</p>'),
(31, 63, '', 'About Us', '<p style=\"text-align:justify; font-size:16px;\">In May 2007, Randholee Luxury Resorts opened its doors to the world. Built on two acres of land, Randholee Luxury Resorts was the brain-child of Mrs. Suwanitha Senanayake, the Managing Director. The Randholee theme, the uniqueness of the hotel, the interior designing, the landscaping and so much more, are her marvellous ideas which she executed to perfection. All of the artistic influence in the hotel - the paintings in the rooms and the lobby are the works of Mrs. Suwanitha Senanayake. Late Mr. Ranjith Senanayake was the Chairman of Randholee Luxury Resorts and Freudenburg Shipping Agency; he was also the grandson of Mr. Don Stephen Senanayake, the first Prime Minister of Ceylon (now Sri Lanka) from 1947 to 1952.</p>\r\n       \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:13px;\"><strong>Kandy Esala Perahera</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">Considered one of the most colourful religious pageants in Asia, Kandy\'s ten-day Esala Perahera is one of Sri Lanka\'s most spectacular festivals. The perahera (procession) is held to honour the sacred tooth enshrined in the Temple of the Sacred Tooth Relic. <br><br>\r\nAccording to Buddhist history, Sri Lanka was chosen as the new home for the tooth relic of Lord Buddha because he had declared that his religion would be protected and preserved  there (in Sri Lanka) for over   2,500 years.<br><br>\r\nThe perahera is traditionally held over the last nine days of the lunar month of Esala, finishing on Nikini Poya day. This usually falls during late July or early August through the exact dates vary according to the vagaries of the lunar calendar; the authorities sometimes opt for slightly different dates depending on practical and astrological considerations. Filled with brilliant colour, dance and tradition, the essence of Sri Lankan history is captured in the annual Kandy Esala Perahera.</p>\r\n     \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:13px;\"><strong>Temple of the Tooth Relic</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">When Buddha attained nirvana his body was cremated and his tooth was acquired from the ashes. The tooth soon became an object of veneration and a belief grew that whoever had possession of the tooth would have the power to rule that land. In the ancient times , Sri Lanka was threatened with many foreign invasions and the seat of the kingdom was moved from city to city. Upon each change of capital, a new palace was built to enshrine the relic. Finally, it was bought to Kandy where it is at present in the Temple of the Sacred Tooth Relic.<br><br>\r\nThe temple which houses the sacred tooth relic is built in the royal palace complex in Kandy, which was the last capital of the Sinhalese Kings. The temple now comprises the royal palace, an audience hall and the mahamaluwa, which is a lawn area situated in front of the temple compound. The Royal Palace used to be the official residence of the government agent, Sir John D\'Oyly and is now preserved as an archaeological museum. The audience hall is where the Kandyan kings held their royal court and was where the Kandyan Convention was drawn up and read out to the people. It is now used for state ceremonies and conserved under the department of archaeology.</p>\r\n     \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:13px;\"><strong>About Kandy</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">Situated at an elevation of 500m above sea level, the historic city of Kandy is nestled among the misty hills in the central region of Sri Lanka. The shrine holding the Sacred Tooth Relic of the Lord Buddha (The Temple of the Tooth) is located in the centre of the city, making Kandy the most venerated city in Sri Lanka. Yearly, the Sacred Tooth Relic is honoured with a ten-day procession including lavishly decorated elephants, drummers, dancers, acrobats and baton twirlers. Classified as a World Heritage City by UNESCO, many Sinhalese traditions are kept alive in the city’s distinctive music, dance and architecture. The Temple of the Tooth, Peradeniya Botanical Gardens and the Riverside Elephant Park are some of the remarkable sites of this city.<br><br>\r\nKandy is considered the cultural capital of Sri Lanka and has an abundance of history and nature to offer. It is only 115km away from Colombo and has many sites to enjoy.</p>'),
(32, 64, '', 'Terms and Conditions', '<p style=\"text-align:justify; font-size:15px;\">This Web Site is owned and operated by Randholee Resorts (Private) Limited, a company duly incorporated under the Companies Law of Sri Lanka, having its registered office at 103/14 Dharmapala Mawatha,Colombo 7, in the Democratic Socialist Republic of Sri Lanka (hereinafter, as and when the context so requires, called and referred to as \"Randholee Resorts\").<br><br>\r\n\r\nBy accessing and using this Web Site, you agree to be bound by the terms and conditions of use, set out herein. The attached terms of online reservations shall apply, when you make your reservations or purchases online and shall form part and parcel of this Agreement. The Principals of Data Protection of Randholee Resorts is also annexed hereto for your reference.</p>\r\n        \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:16px; text-transform:none;\"><strong>Terms and Conditions of Use</strong></h1>\r\n            \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Acceptable Use</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">You may use this Web Site in accordance with these terms and conditions for lawful and proper purposes complying with all applicable laws, regulations and codes of practice in Sri Lanka and/or other jurisdictions from which you are accessing the Web Site.</p>\r\n                \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\">In particular you agree you will not -</h1>\r\n<ul class=\"priv-poly\">\r\n      <li style=\"margin-bottom: 10px;\">Post, transmit or disseminate any information via this Web Site which is or may be harmful, obscene or otherwise illegal.</li>\r\n      <li style=\"margin-bottom: 10px;\">Use this Web Site in a manner which causes or may cause infringement of the rights of another.</li>\r\n      <li style=\"margin-bottom: 10px;\">Use any software, routine, procedure or device to interfere or attempt to interfere with the operation or functionality of this Web Site including but not limited to making available corrupt and/or fraudulent data, viruses via whatever means or interfere with its availability to others.</li>\r\n      <li style=\"margin-bottom: 10px;\">Deface, alter or interfere with the look and feel of this Web Site or underlying software code.</li>\r\n      <li style=\"margin-bottom: 10px;\">Take any action that imposes an unreasonable or disproportionately large load on the Web Site or related infrastructure.</li>\r\n      <li style=\"margin-bottom: 10px;\">Obtain or attempt to obtain unauthorized access by whatsoever means to any of our networks.</li>\r\n</ul>\r\n<p style=\"text-align:justify; font-size:14px;\">Where Randholee Resorts believes in its absolute discretion that you are in breach of any of these terms and conditions, without prejudice to any of our rights at law or otherwise, we reserve the right to deny you access to this Web Site.</p>\r\n                \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Copyright and Restrictions on Use</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">All trademarks, service marks, copyright, database rights, and other intellectual property rights in materials on this Web Site as well as the organization and layout of this Web Site together with the underlying software belong to Randholee Resorts or its licensors. The trademark, service mark and logos of Randholee Resorts and its licensors must not be used, modified or adapted in any way without the prior written authorization. The material on this Web Site and the underlying software codes may not be copied, modified, altered, published, broadcast, distributed, linked to another Web Site, sold or transferred in whole or in part without an express written permission. However, the information contained here may be downloaded, printed or copied for your personal non-commercial use.</p>\r\n                \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Disclaimer, Limitation of Liability and Indemnity</strong></h1>\r\n<ul class=\"priv-poly\">\r\n      <li style=\"margin-bottom: 10px;\">This Web Site has been compiled in good faith by Randholee Resorts. No representation is made or warranty given, either express or implied as to the completeness or accuracy of the information that it contains, that it will be uninterrupted or error free or that any information is free of bugs, viruses, worms, trojan horses or other harmful components.</li>\r\n      <li style=\"margin-bottom: 10px;\">To the maximum extent permitted by law, Randholee Resorts disclaims all implied warranties with regard to information, products, services and material provided through this Web Site. All such information, products, services and materials are provided \"as is\" and \"as available\" without warranty of any kind.</li>\r\n      <li style=\"margin-bottom: 10px;\">This Web Site may contain links to Web Sites operated by third parties. Such links are provided for your reference only and your use of these sites may be subject to terms and conditions posted on them. Randholee Resorts inclusion of links to other Web Sites does not imply any endorsement of the material on such Web Sites and Randholee Resorts accepts no liability for their contents.</li>\r\n      <li style=\"margin-bottom: 10px;\">By accessing this Web Site you agree that Randholee Resorts will not be liable for any direct, indirect or consequential loss or damages of any nature arising from the use of this Web Site, including information and material contained in it, from your access of other material on the Internet via web links from this Web Site, delay or inability to use this Web Site or the availability and utility of the products and services.</li>\r\n      <li style=\"margin-bottom: 10px;\">You further agree to indemnify, hold Randholee Resorts, and its service providers harmless from and covenant not to sue Randholee Resorts for any claims based on using this Web Site.</li>\r\n</ul>\r\n<div style=\"clear:both\"></div>\r\n                \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Modification of Terms</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">Randholee Resorts may make improvements and/or changes in the products, services and prices described in this Web Site at anytime without notice. Changes are periodically made to the Web Site. Randholee Resorts may amend these terms by posting the amended terms on this Web Site. Continued use of this Web Site following such changes shall constitute your acceptance of such changes.</p>\r\n             \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Age and Responsibility</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">You represent you are of sufficient legal age to use this Web Site and you possess the legal right and ability to enter into this Agreement and use this site in accordance with all of the applicable terms and conditions and create binding legal obligations for any liability you may incur as a result of the use of this Web Site under your name or account including, without limitation, all use of your account by others including minors living with you.</p>\r\n              \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Child Policy </strong></h1>\r\n<ul class=\"priv-poly\">\r\n      <li>Children between 0-5.99 years will be accommodated in the parents/adults room free of charge</li>\r\n      <li>Children between 6-11.99 years will be charged a child supplement of 25% of the base category double room rate on the booked boarding basis (charged per child per night). The child supplement will be offered a maximum of one extra bed per room.</li>\r\n</ul> \r\n              \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Cancellation Policy </strong></h1>\r\n<ul class=\"priv-poly\">\r\n      <li>Cancelations made fourteen (14) or more days prior to the date of arrival will incur a 0% charge. (100 % Refund)</li>\r\n      <li>Cancelations made three (3) to fourteen (14) days prior to the date of arrival will incur a 50% of first night Room charge.</li>\r\n      <li>Cancelations made less than two (2) days prior to the arrival date will incur a 100% of First Night room charge.</li>\r\n      <li>All No shows will be charged at 100% of their full stay. (Include meal plans).</li>\r\n      <li>If the room you are booking is labeled as non-refundable, non-cancellable or similar, all cancellations will incur a 100% charge, regardless of the date in which the cancellation is requested.</li>\r\n</ul>\r\n<p style=\"text-align:justify; font-size:14px;\">Cancellation policies can vary seasonally or depending on the hotel room type.</p> \r\n                \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Special value added services :</strong></h1>\r\n<ul class=\"priv-poly\">\r\n      <li>Child care services – US$ 15 net per day.</li>\r\n      <li>Guide services for excursions – US$ 25 net per day</li>\r\n      <li>Kandy city & shopping tour – US$ 60 net per day.</li>\r\n</ul>\r\n'),
(33, 65, '', 'Privacy Policy', '<p style=\"text-align:justify; font-size:15px;\">This Privacy Policy governs the manner in which Randholee Resorts collects, uses, maintains and discloses information collected from users (each, a \"User\") of the randholeeresorts.lk website (\"Site\"). This privacy policy applies to the Site and all products and services offered by Randholee Resorts.</p>\r\n        \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Personal identification information</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, place an order, fill out a form, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address, mailing address, phone number, credit card information. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>\r\n                \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Non-personal identification information</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</p>\r\n                \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Web browser cookies</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">Our Site may use \"cookies\" to enhance User experience. User\'s web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>\r\n                \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>How we use collected information</strong></h1>\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\">Randholee Resorts may collect and use Users personal information for the following purposes:</h1>\r\n\r\n<ul class=\"priv-poly\">\r\n      <li>- <em>To improve customer service</em>\r\n              Information you provide helps us respond to your customer service requests and support needs more efficiently.\r\n      </li>\r\n      <li>- <em>To improve our Site</em>\r\n              We may use feedback you provide to improve our products and services.\r\n      </li>\r\n      <li>- <em>To process payments</em>\r\n              We may use the information Users provide about themselves when placing an order only to provide service to that order. We do not share this information with outside parties except to the extent necessary to provide the service.\r\n      </li>\r\n      <li>- <em>To run a promotion, contest, survey or other Site feature</em></li>\r\n      <li>- <em>To send periodic emails</em>\r\n              We may use the email address to send User information and updates pertaining to their order. It may also be used to respond to their inquiries, questions, and/or other requests.\r\n      </li>\r\n</ul>\r\n<div style=\"clear:both\"></div>\r\n             \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>How we protect your information</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.</p>\r\n                \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Sharing your personal information</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above.</p>\r\n                \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Third party websites</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">Users may find advertising or other content on our Site that link to the sites and services of our partners, suppliers, advertisers, sponsors, licensors and other third parties. We do not control the content or links that appear on these sites and are not responsible for the practices employed by websites linked to or from our Site. In addition, these sites or services, including their content and links, may be constantly changing. These sites and services may have their own privacy policies and customer service policies. Browsing and interaction on any other website, including websites which have a link to our Site, is subject to that website\'s own terms and policies.</p>\r\n                \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Changes to this privacy policy</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">Randholee Resorts has the discretion to update this privacy policy at any time. When we do, we will post a notification on the main page of our Site, revise the updated date at the bottom of this page. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>\r\n                \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Your acceptance of these terms</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">By using this Site, you signify your acceptance of this policy and terms of service. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.</p>\r\n'),
(34, 79, 'LONG STAY OFFER', '6 Days &amp; 5 Nights In Sri Lanka For £650 (Twin Sharing)', '<h1 class=\"hdr-seven hdr-seven-ect\">Day 01</h1>\r\n<ul class=\"priv-poly\">\r\n      <li> Arrival at Colombo International Airport, after clearing the immigration we shall meet by your representative and transfer to Colombo and check in at Ellen’s place. Site scene in Colombo in the after noon </li>\r\n</ul>\r\n\r\n<h1 class=\"hdr-seven hdr-seven-ect\">Day 02 & 03</h1>\r\n<ul class=\"priv-poly\">\r\n      <li>After breakfast check out from the hotel and proceed for sightseeing tour of Kandy (a UNESCO World Heritage Site). The Kandyan kingdom, the last bastion of the Sri Lankan political power against the colonial forces, is the home for the Temple of the Sacred Tooth Relic and the site of one of the world’s most impressive annual festivals – the Esala Perahera. The Temple contains the most sacred relic of the Buddhists today and the most precious of Sinhalese pride. The upper storey houses the sacred relic and a library containing 800 year old palm leaf religious manuscripts. The Pooja (ceremonies of veneration) are held at dawn and dusk daily and these are moments when the Temple comes to life with pilgrims making offerings of flowers amidst clouds of incense and the beating of traditional ritual drums.  The city is visually rich with its narrow streets lined with characterful old buildings. The market has a good display of traditional arts and craft, textiles and gems & jewellery. The lake built by the last Sinhala King Sri Wickrama Rajasinghe in 1798 is still a focal attraction.</li>\r\n      <li>Proceed to Pussellawa. Visit the Anjaneear Temple in Pussellawa housing a 16 feet image of Lord Hanuman. The Sri Baktha Hanuman Temple was built with Hanuman as the presiding deity on this hill of Ramboda, where Hanuman was believed to be searching for Sita Devi.</li>\r\n      <li>Proceed to Nuwara Eliya on route witnessing the Ramboda waterfalls and visiting a tea plantation and a tea factory. There will be the opportunity to observe all about the process of manufacturing and grading tea and taste a cup of fresh Ceylon Tea in the factory itself. Check in at the hotel and rest of the day free at leisure. Overnight stay at the hotel in Nuwara Eliya.</li>\r\n</ul>\r\n                                \r\n<!--<p style=\"text-align:justify; font-size:14px;\">Room rates starting from USD 140/- Per room per night</p>-->\r\n\r\n<h1 class=\"hdr-seven hdr-seven-ect\">Day 4  Nuwara Eliya </h1>\r\n<ul class=\"priv-poly\">\r\n      <li>After Breakfast check out from the hotel and proceed for the tour of Sita Amman Temple. According to legend, the temple is believed to mark the spot where Sita was held captive by King Rawana. Ancient statues of Rama and Sita can be seen with paintings, pillars decorated with sculptures, all depicting the tale of Rama and Sita.  The rear of the temple overlooks a stream, while from its front Hakgala peaks can be viewed.</li>\r\n      <li>Thereafter, visit the Hakgala Botanic Gardens. This Botanic Garden was one of the pleasure gardens of King Ravana of the epic Ramayanaya.  According to the epic, this was here where the heart-warming meeting took place between Sita and Hanuman, who brought her Rama’s ring.</li>\r\n      <li>Thereafter visit Isthripura. This exotic cave still boasts the picturesque cascading waterfall that was in existence 2000 years ago. It was within this cave that Sita was held with over 1500 women to attend to her every need.</li>\r\n      <li>Thereafter proceed to Welimada and visit the Divurumpola Temple. It is believed that this Temple has been since the time of Rama, Rawana. Still revered by thousands, this was believed to be the place where Sita performed her Agni Pariksha (proving her chastity) to her husband Rama.  Since that time this place has been respected and worshiped by people. It is said that this place was given the name ‘Divurumpola’ (the place of making a wow) not only in legends, but it has been inscribed in the moonstone guarding the Devale in the premises.</li>\r\n</ul>\r\n                                \r\n<h1 class=\"hdr-seven hdr-seven-ect\">Day 05 & 06</h1>\r\n<ul class=\"priv-poly\">\r\n      <li>Heading back from Nuwara eliya to Colombo rest at Ellen’s place departure after breakfast on the day 06 and transfer to airport. </li>\r\n      &nbsp;\r\n      <li style=\"list-style-type: square;font-size:18px;\">For more information</li>\r\n      <li class=\"fa fa-envelope-o\" style=\"font-size:20px;\">&emsp;mgr.sales@freudenbergleisure.com (Maheel)</li>\r\n      <li class=\"fa fa-phone\" style=\"font-size:20px;\">&emsp;+94 777 385 064</li>\r\n</ul>');
INSERT INTO `tbldescriptions` (`id`, `page_id`, `sub_title`, `main_title`, `body_text`) VALUES
(35, 8, '', 'Terms and Conditions', '<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:16px; text-transform:none;\"><strong>Web Site User Agreement</strong></h1>         \r\n<p style=\"text-align:justify; font-size:14px;\">This Web Site is owned and operated by Freudenberg Leisure (Private) Limited, a company duly incorporated under the Companies Law of Sri Lanka, having its registered office at 103/14 Dharmapala Mawatha, Colombo 7, in the Democratic Socialist Republic of Sri Lanka (hereinafter, as and when the context so requires, called and referred to as \" Freudenberg Leisure\").\r\n<br><br>                                    \r\nBy accessing and using this Web Site, you agree to be bound by the terms and conditions of use, set out herein. The attached terms of online reservations shall apply, when you make your reservations or purchases online and shall form part and parcel of this Agreement. The Principals of Data Protection of Freudenberg Leisure is also annexed hereto for your reference.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:16px; text-transform:none;\"><strong>Terms and Conditions of Use</strong></h1>\r\n                                \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Acceptable Use</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">You may use this Web Site in accordance with these terms and conditions for lawful and proper purposes complying with all applicable laws, regulations and codes of practice in Sri Lanka and/or other jurisdictions from which you are accessing the Web Site.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\">In particular you agree you will not -</h1>\r\n<ul class=\"priv-poly\">\r\n      <li style=\"margin-bottom: 10px;\">Post, transmit or disseminate any information via this Web Site which is or may be harmful, obscene or otherwise illegal.</li>\r\n      <li style=\"margin-bottom: 10px;\">Use this Web Site in a manner which causes or may cause infringement of the rights of another.</li>\r\n      <li style=\"margin-bottom: 10px;\">Use any software, routine, procedure or device to interfere or attempt to interfere with the operation or functionality of this Web Site including but not limited to making available corrupt and/or fraudulent data, viruses via whatever means or interfere with its availability to others.</li>\r\n      <li style=\"margin-bottom: 10px;\">Deface, alter or interfere with the look and feel of this Web Site or underlying software code.</li>\r\n      <li style=\"margin-bottom: 10px;\">Take any action that imposes an unreasonable or disproportionately large load on the Web Site or related infrastructure.</li>\r\n      <li style=\"margin-bottom: 10px;\">Obtain or attempt to obtain unauthorized access by whatsoever means to any of our networks.</li>\r\n</ul>\r\n                        \r\n<p style=\"text-align:justify; font-size:14px;\">Where Freudenberg Leisure believes in its absolute discretion that you are in breach of any of these terms and conditions, without prejudice to any of our rights at law or otherwise, we reserve the right to deny you access to this Web Site.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Copyright and Restrictions on Use</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">All trademarks, service marks, copyright, database rights, and other intellectual property rights in materials on this Web Site as well as the organization and layout of this Web Site together with the underlying software belong to Freudenberg Leisure or its licensors. The trademark, service mark and logos of Freudenberg Leisure and its licensors must not be used, modified or adapted in any way without the prior written authorization. The material on this Web Site and the underlying software codes may not be copied, modified, altered, published, broadcast, distributed, linked to another Web Site, sold or transferred in whole or in part without an express written permission. However, the information contained here may be downloaded, printed or copied for your personal non-commercial use.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Disclaimer, Limitation of Liability and Indemnity</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">This Web Site has been compiled in good faith by Freudenberg Leisure. No representation is made or warranty given, either express or implied as to the completeness or accuracy of the information that it contains, that it will be uninterrupted or error free or that any information is free of bugs, viruses, worms, trojan horses or other harmful components.</p>\r\n                                \r\n<ul class=\"priv-poly\">\r\n      <li style=\"margin-bottom: 10px;\">To the maximum extent permitted by law, Freudenberg Leisure disclaims all implied warranties with regard to information, products, services and material provided through this Web Site. All such information, products, services and materials are provided \"as is\" and \"as available\" without warranty of any kind.</li>\r\n      <li style=\"margin-bottom: 10px;\">This Web Site may contain links to Web Sites operated by third parties. Such links are provided for your reference only and your use of these sites may be subject to terms and conditions posted on them. Freudenberg Leisure inclusion of links to other Web Sites does not imply any endorsement of the material on such Web Sites and Freudenberg Leisure accepts no liability for their contents.</li>\r\n      <li style=\"margin-bottom: 10px;\">By accessing this Web Site you agree that Freudenberg Leisure will not be liable for any direct, indirect or consequential loss or damages of any nature arising from the use of this Web Site, including information and material contained in it, from your access of other material on the Internet via web links from this Web Site, delay or inability to use this Web Site or the availability and utility of the products and services.</li>\r\n      <li style=\"margin-bottom: 10px;\">You further agree to indemnify, hold Freudenberg Leisure, and its service providers harmless from and covenant not to sue Freudenberg Leisure for any claims based on using this Web Site.</li>\r\n</ul>\r\n                                \r\n<div style=\"clear:both\"></div>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Modification of Terms</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">Freudenberg Leisure may make improvements and/or changes in the products, services and prices described in this Web Site at any time without notice. Changes are periodically made to the Web Site. Freudenberg Leisure may amend these terms by posting the amended terms on this Web Site. Continued use of this Web Site following such changes shall constitute your acceptance of such changes.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Age and Responsibility</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">You represent you are of sufficient legal age to use this Web Site and you possess the legal right and ability to enter into this Agreement and use this site in accordance with all of the applicable terms and conditions and create binding legal obligations for any liability you may incur as a result of the use of this Web Site under your name or account including, without limitation, all use of your account by others including minors living with you.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Cancellation Policy </strong></h1>\r\n       \r\n<ul class=\"priv-poly\">\r\n      <li>Cancelations made fourteen (14) or more days prior to the date of arrival will incur a 0% charge. (100 % Refund)</li>\r\n      <li>Cancelations made three (3) to fourteen (14) days prior to the date of arrival will incur a 50% of first night Room charge.</li>\r\n      <li>Cancelations made less than two (2) days prior to the arrival date will incur a 100% of First Night room charge.</li>\r\n      <li>All No shows will be charged at 100% of their full stay. (Include meal plans).</li>\r\n</ul>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Check-in and Check-out Times</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">Check-in time is from 02.00 PM (14:00 hrs) onwards and check out time is 12.00 noon.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Early Check-in</strong></h1>\r\n<ul class=\"priv-poly\">\r\n      <li>Early arrivals will be on availability and needs to be informed at time of reservation. Arrivals earlier than 10:00 AM will be charged a 50% of the room rate and will only be on availability.</li>\r\n      <li>Early arrivals ahead of 6:00 AM will be charged an extra night’s rate and will be confirmed in advance if requested.</li>\r\n</ul>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Late Departure</strong></h1>\r\n<ul class=\"priv-poly\">\r\n      <li>Late departure up to 2:30 PM will be on availability and needs to be informed at time of arrival.</li>\r\n      <li>Late departure up to 6:00 PM can be arranged at a 50% of the room rate and will only be offered on availability.</li>\r\n      <li>Late departures beyond 6:00 PM will be charged as an extra night and will be confirmed in advanced.</li>\r\n</ul>\r\n \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Child Policy</strong></h1>\r\n<ul class=\"priv-poly\">\r\n      <li>Children between 0-5.99 years will be accommodated in the parents/adults room free of charge</li>\r\n      <li>Children between 6-11.99 years will be charged a child supplement of 25% of the base category double room rate on the booked boarding basis (charged per child per night). The child supplement will be offered a maximum of one extra bed per room.</li>\r\n</ul>\r\n                                \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Extra Bed</strong></h1>\r\n<ul class=\"priv-poly\">\r\n      <li>Extra beds & baby cots can only be arranged in Suites rooms & Deluxe Rooms.</li>\r\n      <li>Extra beds can only be arranged for a double room and an adult is charged $ 15.00 (US Dollar) of the room stay per night in an extra bed.</li>\r\n      <li>Maximum capacity of extra beds in a room is 1.</li>\r\n      <li>Maximum capacity of baby cots in a room is 1.</li>\r\n</ul>\r\n                                \r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Pet Policy</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">No pets allowed</p>'),
(36, 9, '', 'Privacy Policy', '<p style=\"text-align:justify; font-size:14px;\">This Privacy Policy governs the manner in which Freudenberg Leisure collects, uses, maintains and discloses information collected from users (each, a \"User\") of the www.freudenbergleisure.com website (\"Site\"). This privacy policy applies to the Site and all products and services offered by Freudenberg Leisure.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Personal identification information</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, place an order, fill out a form, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address, mailing address, phone number, credit card information. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Non-personal identification information</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Web browser cookies</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">Our Site may use \"cookies\" to enhance User experience. User\'s web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>How we use collected information</strong></h1>\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\">Freudenberg Leisure may collect and use Users personal information for the following purposes:</h1>\r\n                                \r\n<ul class=\"priv-poly\">\r\n      <li>To improve customer service - Information you provide helps us respond to your customer service requests and support needs more efficiently.</li>\r\n      <li>To improve our Site - We may use feedback you provide to improve our products and services.</li>\r\n      <li>To process payments - We may use the information Users provide about themselves when placing an order only to provide service to that order. We do not share this information with outside parties except to the extent necessary to provide the service.</li>\r\n      <li>To run a promotion, contest, survey or other Site feature - To send Users information they agreed to receive about topics we think will be of interest to them.</li>\r\n      <li>To send periodic emails - We may use the email address to send User information and updates pertaining to their order. It may also be used to respond to their inquiries, questions, and/or other requests.</li>\r\n</ul>\r\n                                \r\n<div style=\"clear:both\"></div>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>How we protect your information</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Sharing your personal information</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Third party websites</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">Users may find advertising or other content on our Site that link to the sites and services of our partners, suppliers, advertisers, sponsors, licensors and other third parties. We do not control the content or links that appear on these sites and are not responsible for the practices employed by websites linked to or from our Site. In addition, these sites or services, including their content and links, may be constantly changing. These sites and services may have their own privacy policies and customer service policies. Browsing and interaction on any other website, including websites which have a link to our Site, is subject to that website\'s own terms and policies.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Changes to this privacy policy</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">Freudenberg Leisure has the discretion to update this privacy policy at any time. When we do, we will post a notification on the main page of our Site, revise the updated date at the bottom of this page. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Your acceptance of these terms</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">By using this Site, you signify your acceptance of this policy and terms of service. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Contacting us</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us at:<br>\r\n   <br><strong>Freudenberg Leisure</strong>\r\n   <br>Freudenberg Leisure(Pvt.) Ltd\r\n   <br>103/14 Dharmapala Mawatha,Colombo 7, Sri Lanka\r\n   <br>+94 (0) 11 2445282\r\n</p>'),
(37, 10, '', 'Downloads', '<div style=\"padding: 30px; padding-top: 45px;\">\r\n<p style=\"text-align: left; font-size: 16px;\">For further information about Freudenberg Leisure, you are welcome to download our brochure and fact sheet. Simply enter your email address to make your requestt.</p>\r\n</div>'),
(38, 80, '', 'Wedding', '<p style=\"text-align:justify; font-size:16px;\">Exchange your wedding vows as the sun sets behind the misty mountains of Kandy. Have your first dance as a newly wed couple beneath the divine clouds. Feel the aura of mother nature as you step in matrimony under the vast blue sky. Randholee Luxury Resort offers an elegant setting for a royal wedding experience. <br />Our wedding specialists at Randholee Luxury Resort have vast experience in organizing, planning and arranging weddings. Our staff will ensure that your wedding is handled with precise detail and care. All aspects, including legal procedures, will be taken care of prior to the wedding day to guarantee that everything runs smoothly for a hassle-free event. A wedding at Randholee Luxury Resort will definitely be a memorable and treasured event.</p>\r\n\r\n<span style=\"font-size:15px; text-align:center;\"><em><strong>\"It’s your special day so tell us what you want and how you want it. Leave the rest to us.\"</strong></em></span><br><br>\r\n\r\n                                <ul class=\"wed-list\">\r\n                                    <li>Location you find most suitable on hotel premises (outdoors or indoors)</li>\r\n                                    <li>Overnight stay on full board basis on your wedding day</li>\r\n                                    <li>Additional day room for arrangements and bridal dressing</li>\r\n                                    <li>A basket of fresh fruits in the room </li>\r\n                                    <li>Beautifully frilled table for the cake structure</li>\r\n                                    <li>Antique settee with coffee table</li>\r\n                                    <li>Registration table 5½ ft traditional oil lamp (if required)</li>\r\n                                    <li>A bottle of sparkling wine</li>\r\n                                    <li>Professional and dedicated staff</li>\r\n                                    <li>All legal requirements will be handled before the day of the wedding</li>\r\n                                    <li>Special requirements could be met if required</li>\r\n                                </ul>\r\n\r\n<div style=\"clear:both;\"></div>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:14px; text-transform:none; margin-top: 20px;\"><strong>Foreign Wedding package</strong></h1> \r\n<p class=\"wed-d\">Cost – US$ 2000.00</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:14px; text-transform:none; margin-top: 20px;\"><strong>Stipulations and Conditions</strong></h1>\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:14px; text-transform:none; margin-top: 20px;\"><strong>The package is inclusive of prevailing taxes & service charges</strong></h1>\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:14px; text-transform:none; margin-top: 20px;\"><strong>Validity</strong> - 06 – six months</h1>\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:14px; text-transform:none; margin-top: 20px;\"><strong>Couple & other invitees should venerate the local culture & law</strong></h1>\r\n<p class=\"wed-d\">Wedding experts at Randholee have the right to organize the wedding ceremony at the location they find most suitable on hotel premises.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:14px; text-transform:none; margin-top: 20px;\"><strong>Venue</strong> - Subject to change due to the existing weather conditions</h1>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:14px; text-transform:none; margin-top: 20px;\"><strong>Wedding Package Includes:</strong></h1>\r\n                                <ul class=\"wed-list\">\r\n                                    <li>Pre wedding briefing by a professional event co ordinator and a guest relations officer</li>\r\n                                    <li>Embellished wedding location</li>\r\n                                    <li>Traditional ceremony with drummers and dancers</li>\r\n                                    <li>Master of the ceremonies to conduct the event</li>\r\n                                    <li>Traditional hymns [ psalm ] of blessing by girls</li>\r\n                                    <li>Two tier wedding cake structure</li>\r\n                                    <li>Bouquet & button hole</li>\r\n                                    <li>Traditional oil lamp</li>\r\n                                    <li>Champagne & canapes</li>\r\n                                    <li>Event on a DVD </li>\r\n                                    <li>Photographs in an album with negatives</li>\r\n                                    <li>01 - framed photograph of the couple</li>\r\n                                    <li>Going away by the bullock cart</li>\r\n                                    <li>Marriage certificate [ in English ]</li>\r\n                                    <li>Witnesses & assistance locally if required</li>\r\n                                    <li>Full body massage at the ayurveda center during the stay</li>\r\n                                    <li>Decorated room</li>\r\n                                    <li>Special 05 course candle light dinner by the pool / in the balcony</li>\r\n                                    <li>Fruits, flowers, special honeymoon chocolates and a bottle of wine in the room</li>\r\n                                    <li>Gifts from the hotel</li>\r\n                                </ul>\r\n\r\n<div style=\"clear:both;\"></div>\r\n\r\n                                <table style=\"width: 95%;\" border=\"0\" cellspacing=\"2\" cellpadding=\"0\">\r\n                                    <tbody>\r\n                                        <tr>\r\n                                            <td class=\"hdr-seven\"><strong>Supplementary</strong></td>\r\n                                            <td class=\"hdr-seven\" align=\"right\"><strong>Cost US$</strong></td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td>Live music during the ceremony [ calypso/ organ / violinist ]</td>\r\n                                            <td align=\"right\">175.00</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td>Fire works with names of bride &amp; groom</td>\r\n                                            <td align=\"right\">250.00</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td>Traditional dress for the couple</td>\r\n                                            <td align=\"right\">250.00</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td>Beautician</td>\r\n                                            <td align=\"right\">150.00</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td>Traditional wedding jewelery for the bride – hiring</td>\r\n                                            <td align=\"right\">130.00</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td class=\"hdr-seven\"><strong>Extra meals [ covers ]</strong></td>\r\n                                            <td align=\"right\">&nbsp;</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td>Dinner</td>\r\n                                            <td align=\"right\">20.00</td>\r\n                                        </tr>\r\n                                        <tr>\r\n                                            <td>Lunch</td>\r\n                                            <td align=\"right\">16.00</td>\r\n                                        </tr>\r\n                                    </tbody>\r\n                                </table>'),
(39, 81, 'LONG STAY OFFER', '-Stay 4 nights and get the one night free!-', '<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:22px; padding:10px; font-weight:100;\"><em>Deluxe Mountain View & Deluxe Rooms</em></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">Book four consecutive nights at Randholee. Receive our best available rate and your fourth night free!</p>\r\n<h1 class=\"hdr-seven hdr-seven-ect\">Special Offer Include</h1>\r\n<ul class=\"priv-poly\">\r\n      <li>GET THE 4th NIGHT FREE on Room only / Bed & Breakfast plans.</li>\r\n      <li>Promotion applicable for minimum & maximum of 4 nights stay only</li>\r\n</ul>\r\n<h1 class=\"hdr-seven hdr-seven-ect\">Special Offer Duration</h1>\r\n<ul class=\"priv-poly\">\r\n      <li>21st October 2015 to 29th February 2016</li>\r\n</ul>\r\n<p style=\"text-align:justify; font-size:14px;\">Room rates starting from USD 140/- Per room per night</p>\r\n<h1 class=\"hdr-seven hdr-seven-ect\">CONDITIONS</h1>\r\n<ul class=\"priv-poly\">\r\n      <li>Rates indicated above include a 10% service charge, & other government taxes.</li>\r\n      <li>Rates above are indicative rates per room per night and are subject to change without notice.</li>\r\n      <li>This offer is on a non-refundable cancellation policy. If canceled, modified or in case of no-show, the    total price of the reservation will be charged.</li>\r\n      <li>Total amount of the reservation will be charged at the time of the reservation.</li>\r\n      <li>Bookable period is from  21st October 2015 to 29th February 2016 with the stay period falling 30 days forward, until  29th February 2016.</li>\r\n      <li>The travel/stay period for this offer will be 6th January 2015 to 31st August 2015.</li>\r\n      <li>This promotion cannot be combined with any other promotion or discount and is subject to availability.</li>\r\n</ul>'),
(40, 82, 'The Firs', 'Heritage Upscale Bungalow', '<p align=\"justify\">The Firs Nuwara Eliya upscale bungalow in a little hillock overlooking Lake Gregory in a picturesque location offers holiday makers an exclusive holiday experience. This old bungalow built over one and half centuries ago has undergone a transformation and renovations to suite the modern era.</p>\r\n\r\n<p align=\"justify\">It has now been transformed to a true Sri Lankan up-market ‘heritage bungalow’ and conceptualised according to the British Colonial era with the intention of providing the same experience that was available during the British monarchy. Rooms consist of two suites displaying old charm and elegance and three other beautifully furnished deluxe rooms.</p>\r\n                                        \r\n<p align=\"justify\">The Firs offers many tours which cover the natural beauty of Nuwara Eliya including adventurous hikes to World’s End, and Sita Eliya (also known as ‘The Glade of Sita’), which is connected to the historical Ramayana story or a casual game of golf at the Famous Victoria Golf Club, St. Andrew’s Church and the spectacular Haggala botanical gardens.</p>'),
(41, 85, 'Five Tastefully - Furnished &amp; Upscale Super Deluxe', 'Accommodation', '<p style=\"text-align: justify; font-size: 17px;\">Accommodation options at The Firs consist of five tastefully furnished and upscale Super Deluxe that emanate old world charm and elegance. Exquisitely designed, each room reflects a sense of quiet comfort and an air of Richness that create an atmosphere of ultimate serenity and respite. Every minute detail has been delicately refined to suit you just right.<br /><br /> In-room amenities at The Firs include LCD televisions with satellite channels, room heaters, tea and coffee making facilities, as well as complimentary Wi-Fi Internet access. All Super Deluxes are located on the ground floor and are capable of accommodating up to two or three people, with an option of adding an extra bed if necessary.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 14px; text-transform: none;\">Each accommodation option at The Firs offers the following in-room amenities and services:</h1>\r\n<ul class=\"accom-list\">\r\n<li>Complimentary Mineral Water</li>\r\n<li>Room Service</li>\r\n<li>Laundry Service</li>\r\n<li>Daily Housekeeping</li>\r\n<li>In-room Dining</li>\r\n<!--<li>Complimentary Butler Service</li>-->\r\n<li>Spacious Wardrobe</li>\r\n<li>IDD Activated Telephones</li>\r\n<li>Room Heaters</li>\r\n<li>LCD Televisions</li>\r\n<li>Satellite Channels</li>\r\n<li>DVD Players</li>\r\n<li>Complimentary Newspaper</li>\r\n</ul>\r\n<ul class=\"accom-list\"><!-- <li>Access to CD/DVD libraries</li> -->\r\n<li>Writing Table and Chair</li>\r\n<li>Bathroom Toiletries</li>\r\n<li>Jacuzzi</li>\r\n<li>Extra Blankets and Towels</li>\r\n<li>Iron and Board</li>\r\n<li>Extra Bed</li>\r\n<li>Baby Crib</li>\r\n<li>Tea and Coffee-Making Facilities</li>\r\n<li>Complimentary Wi-Fi Internet Access</li>\r\n</ul>\r\n<!-- section separator -->\r\n<p style=\"text-align: justify; font-size: 17px;\">Bask in the opulence and grandeur of one of The Firs\' magnificent Super Deluxe Rooms. Located on the ground floor, Every Super Deluxe Room &nbsp;can accommodate up to 3 guests, with the option of adding an extra bed if necessary, and offer spectacular views of the surrounding tea estates and greenery in the distance. Guests may also take advantage of a number of in-room amenities and services including complimentary Wi-Fi internet access, personalised butler service and in-room dining.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 14px; text-transform: none;\">Super Deluxe options at The Firs include:</h1>\r\n<ul class=\"accom-list\">\r\n<li>The Stephan&nbsp;Room</li>\r\n<li>The Molly&nbsp;Room</li>\r\n<li>The Reinhart Room</li>\r\n<li>The Winfried Room</li>\r\n<li>The Siegmund Room</li>\r\n</ul>'),
(42, 86, '', 'Bar', '<p style=\"text-align: justify; font-size: 16px;\">Sip on a tantalising cocktail as you sit back and unwind on a quiet evening at The Firs. Uniquely located in the hotel\'s attic, the bar is capable of accommodating up to 10 people and is the ideal venue to spend a few leisurely moments relaxing or chatting with other guests over a glass of wine. Choose from an extensive range of foreign and local liquors, as well as a array of the finest wines, champagnes and beers offered by The Firs. Adventurous guests are invited to sample the taste of arrack - a local alcoholic drink made from the fermented sap of coconut flowers.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 13px;\">Bar Overview</h1>\r\n<table style=\"width: 75%; margin-left: auto; margin-right: auto;\" border=\"0\" cellspacing=\"2\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td><strong>Bar Capacity:</strong></td>\r\n<td style=\"text-align: left;\" align=\"right\">10 Guests</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Location:</strong></td>\r\n<td style=\"text-align: left;\" align=\"right\">Attic</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Available Beverages:</strong></td>\r\n<td style=\"text-align: left;\" align=\"right\">Foreign &amp; Local Liquor, Beer and Wine</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(43, 88, '', 'Restaurant', '<p style=\"text-align: justify; font-size: 16px;\">Tantalise your taste-buds with a symphony of flavours from around the globe. Blending rustic tastes with modern flair, The Firs offers a delightful range of fusion cuisine ranging from Western to Indian to Continental and Eastern fare to appease the palate of even the most discerning guest. Each impeccably-presented dish prepared by the hotel\'s experienced team of culinary experts will take guests on an unforgettable gastronomic journey. <br /><br /> Capable of accommodating up to 15 guests, the hotel\'s main restaurant is an alluring/ideal venue to relish scrumptious breakfasts, lunches and dinners. The bright, yet intimate atmosphere of the venue creates the perfect setting to indulge in a culinary feast. In-room dining is also available for guests wishing to indulge in the privacy of their room.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 13px;\">Restaurant Overview</h1>\r\n<table style=\"width: 75%; border: none; margin-left: auto; margin-right: auto;\" border=\"0\" cellspacing=\"2\" cellpadding=\"0\">\r\n<tbody>\r\n<tr style=\"border: none;\">\r\n<td><strong>Restaurant Capacity:</strong></td>\r\n<td style=\"text-align: left;\" align=\"right\">15 Guests</td>\r\n</tr>\r\n<tr>\r\n<td><strong>Cuisine Available:</strong></td>\r\n<td style=\"text-align: left;\" align=\"right\">Fusion Cuisine (Western, Indian, Continental, Eastern)</td>\r\n</tr>\r\n</tbody>\r\n</table>'),
(44, 89, '', 'Room Service', '<p style=\"text-align:justify; font-size:16px;\">The Firs strives to provide superior levels of comfort and all-round impeccable service to guests during their stay at the bungalow. Our multilingual staff will also be at hand and can also be contacted at any time.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:13px;\">Additional Services Offered by The Firs:</h1>\r\n\r\n                                <ul class=\"accom-list\">\r\n                                    <li>In-room Dining Service on request</li>\r\n                                    <li>Daily Housekeeping</li>\r\n                                </ul>'),
(45, 87, '', 'Facilities', '<p style=\"text-align:justify; font-size:17px;\">Revel in the true essence of opulence and richness at The Firs, where a host of facilities, services and amenities await guests to ensure that there is never a dull moment during their stay.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:13px;\">Our services and facilities include:</h1>\r\n\r\n                            <ul class=\"accom-list\">\r\n                                <li>Dining Facilities</li>\r\n                                <li>Room Service</li>\r\n                                <li>Laundry Service</li>\r\n                                <li>Daily Housekeeping</li>\r\n                                <li>In-room Dining Service</li>\r\n                                <!--<li>Complimentary Butler Service</li>-->\r\n                                <li>Complimentary Wi-Fi Internet Access</li>\r\n                            </ul>\r\n'),
(46, 90, 'ADVANCE PURCHASE DISCOUNT', '-Save 20% with Advance Purchase Discount-', '<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:22px; padding:10px; font-weight:100;\"><em>The Firs Nuwara Eliya  Early Bird Deal</em></h1>\r\n\r\n<p style=\"text-align:justify; font-size:14px;\">Time to think ahead! Make your booking 30 days in advance and enjoy 20% off on any room or meal plan. <br><br>Make your plans early, place your booking 30 days or more in advance, and you will be rewarded with a 20% discount on any room on any basis, be it Room only or B&B.</p>\r\n\r\n<h1 class=\"hdr-seven hdr-seven-ect\">Special Offer Include</h1>\r\n\r\n                                <ul class=\"priv-poly\">\r\n                                    <li>20% discount for reservations 30 days forward on all meal plans for all room types</li>\r\n                                    <li>Free Wifi</li>\r\n                                </ul>\r\n\r\n<h1 class=\"hdr-seven hdr-seven-ect\">Special Offer Duration</h1>\r\n\r\n                                <ul class=\"priv-poly\">\r\n                                    <li>21st October 2015 to 29th February 2016</li>\r\n                                </ul>\r\n\r\n<p style=\"text-align:justify; font-size:14px;\">Suite Room rates starting from USD 170/- Per room per night</p>\r\n\r\n<h1 class=\"hdr-seven hdr-seven-ect\">CONDITIONS</h1>\r\n\r\n                                <ul class=\"priv-poly\">\r\n                                    <li>Rates indicated above include a 10% service charge, & other government taxes.</li>\r\n                                    <li>Rates above are indicative rates per room per night and are subject to change without notice.</li>\r\n                                    <li>This offer is on a non-refundable cancellation policy. If canceled, modified or in case of no-show, the    total price of the reservation will be charged.</li>\r\n                                    <li>Total amount of the reservation will be charged at the time of the reservation.</li>\r\n                                    <li>Bookable period is from  21st October 2015 to 29th February 2016 with the stay period falling 30 days forward, until  29th February 2016.</li>\r\n                                    <li>The travel/stay period for this offer will be 6th January 2015 to 31st August 2015.</li>\r\n                                    <li>This promotion cannot be combined with any other promotion or discount and is subject to availability.</li>\r\n                                </ul>\r\n\r\n'),
(47, 83, '', 'Location', '<p style=\"text-align:justify; font-size:16px;\">Located in Nuwara Eliya aloft a hillock overlooking Lake Gregory, The Firs offers breathtaking vistas of the surrounding tea estates and greenery. The hotel is ideally located within an hour of some of the most picturesque sights and historical attractions of the hill country - the Hakgala Botanical Gardens, Queen Victoria Park, as well as the Horton Plains National Park.<br><br>\r\nNuwara Eliya is also commonly known as \"Little England\" since the British had once tried to turn it into a typical English village; evidence of this can be seen in the English-style lawns and gardens that are still being maintained by private homes. With the essence of British colonial style living clearly evident, visitors to the city will be fascinated by the nostalgia of bygone days.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:13px;\"><strong>Location Highlights:</strong></h1>\r\n\r\n                                <ul class=\"accom-list\">\r\n                                    <li>Gregory Lake</li>\r\n                                    <li>Hakgala Botanical Gardens</li>\r\n                                    <li>Queen Victoria Park</li>\r\n                                    <li>Horton Plains National Park</li>\r\n                                    <li>World\'s End</li>\r\n                                </ul>'),
(48, 91, '', 'About Us', '<p style=\"text-align:justify; font-size:16px;\">‘The Firs’ is an authentic Sri Lankan up-market Heritage Bungalow which was originally built over a century ago by Philip Freudenberg - Ceylon’s first International Counsel General for Germany. It later became the holiday home of Hon. D. S. Senanayake, Sri Lanka’s first Prime Minister, who visited it quite often in pursuance of golf, bridge-playing and photography. Ownership of the bungalow then passed on to his two sons Dudley and Robert Senanayake and the bungalow has remained with the family for over 100 years to date.</p>\r\n\r\n<p style=\"text-align:justify; font-size:16px;\">Beckoning historical value, The Firs has been conceptualized according to the British Colonial era with the intention of providing the same experience during the British Monarchy. After continuous upgrades, it now follows the very same concept, ensuring that every aspect of the property exudes comfort to reflect the unique spirit and soul of its location, providing guests with a one-of-a-kind experience.</p>'),
(49, 92, '', 'Downloads', '<p style=\"text-align:left; font-size:16px;\">For further information about The Firs, you are welcome to download our brochure and fact sheet.</p>');
INSERT INTO `tbldescriptions` (`id`, `page_id`, `sub_title`, `main_title`, `body_text`) VALUES
(50, 93, '', 'Terms and Conditions', '<p style=\"text-align:justify; font-size:15px;\">This Web Site is owned and operated by The Firs (Private) Limited, a company duly incorporated under the Companies Law of Sri Lanka, having its registered office at 103/14 Dharmapala Mawatha,Colombo 7, in the Democratic Socialist Republic of Sri Lanka (hereinafter, as and when the context so requires, called and referred to as \"The Firs\").<br><br>\r\nBy accessing and using this Web Site, you agree to be bound by the terms and conditions of use, set out herein. The attached terms of online reservations shall apply, when you make your reservations or purchases online and shall form part and parcel of this Agreement. The Principals of Data Protection of The Firs is also annexed hereto for your reference.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:16px; text-transform:none;\"><strong>Terms and Conditions of Use</strong></h1>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Acceptable Use</strong></h1>\r\n\r\n<p style=\"text-align:justify; font-size:14px;\">You may use this Web Site in accordance with these terms and conditions for lawful and proper purposes complying with all applicable laws, regulations and codes of practice in Sri Lanka and/or other jurisdictions from which you are accessing the Web Site.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\">In particular you agree you will not -</h1>\r\n\r\n                                <ul class=\"priv-poly\">\r\n                                    <li style=\"margin-bottom: 10px;\">Post, transmit or disseminate any information via this Web Site which is or may be harmful, obscene or otherwise illegal.</li>\r\n                                    <li style=\"margin-bottom: 10px;\">Use this Web Site in a manner which causes or may cause infringement of the rights of another.</li>\r\n                                    <li style=\"margin-bottom: 10px;\">Use any software, routine, procedure or device to interfere or attempt to interfere with the operation or functionality of this Web Site including but not limited to making available corrupt and/or fraudulent data, viruses via whatever means or interfere with its availability to others.</li>\r\n                                    <li style=\"margin-bottom: 10px;\">Deface, alter or interfere with the look and feel of this Web Site or underlying software code.</li>\r\n                                    <li style=\"margin-bottom: 10px;\">Take any action that imposes an unreasonable or disproportionately large load on the Web Site or related infrastructure.</li>\r\n                                    <li style=\"margin-bottom: 10px;\">Obtain or attempt to obtain unauthorized access by whatsoever means to any of our networks.</li>\r\n                                </ul>\r\n\r\n<p style=\"text-align:justify; font-size:14px;\">Where The Firs believes in its absolute discretion that you are in breach of any of these terms and conditions, without prejudice to any of our rights at law or otherwise, we reserve the right to deny you access to this Web Site.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Copyright and Restrictions on Use</strong></h1>\r\n\r\n<p style=\"text-align:justify; font-size:14px;\">All trademarks, service marks, copyright, database rights, and other intellectual property rights in materials on this Web Site as well as the organization and layout of this Web Site together with the underlying software belong to The Firs or its licensors. The trademark, service mark and logos of The Firs and its licensors must not be used, modified or adapted in any way without the prior written authorization. The material on this Web Site and the underlying software codes may not be copied, modified, altered, published, broadcast, distributed, linked to another Web Site, sold or transferred in whole or in part without an express written permission. However, the information contained here may be downloaded, printed or copied for your personal non-commercial use.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Disclaimer, Limitation of Liability and Indemnity</strong></h1>\r\n\r\n<p style=\"text-align:justify; font-size:14px;\">Our Site may use \"cookies\" to enhance User experience. Users\' browsers places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>\r\n\r\n                                <ul class=\"priv-poly\">\r\n                                    <li style=\"margin-bottom: 10px;\">This Web Site has been compiled in good faith by The Firs. No representation is made or warranty given, either express or implied as to the completeness or accuracy of the information that it contains, that it will be uninterrupted or error free or that any information is free of bugs, viruses, worms, trojan horses or other harmful components.</li>\r\n                                    <li style=\"margin-bottom: 10px;\">To the maximum extent permitted by law, The Firs disclaims all implied warranties with regard to information, products, services and material provided through this Web Site. All such information, products, services and materials are provided \"as is\" and \"as available\" without warranty of any kind.</li>\r\n                                    <li style=\"margin-bottom: 10px;\">This Web Site may contain links to Web Sites operated by third parties. Such links are provided for your reference only and your use of these sites may be subject to terms and conditions posted on them. The Firs inclusion of links to other Web Sites does not imply any endorsement of the material on such Web Sites and The Firs accepts no liability for their contents.</li>\r\n                                    <li style=\"margin-bottom: 10px;\">By accessing this Web Site you agree that The Firs will not be liable for any direct, indirect or consequential loss or damages of any nature arising from the use of this Web Site, including information and material contained in it, from your access of other material on the Internet via web links from this Web Site, delay or inability to use this Web Site or the availability and utility of the products and services.</li>\r\n                                    <li style=\"margin-bottom: 10px;\">You further agree to indemnify, hold The Firs, and its service providers harmless from and covenant not to sue The Firs for any claims based on using this Web Site.</li>\r\n                                </ul>\r\n\r\n<div style=\"clear:both\"></div>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Modification of Terms</strong></h1>\r\n\r\n<p style=\"text-align:justify; font-size:14px;\">The Firs may make improvements and/or changes in the products, services and prices described in this Web Site at anytime without notice. Changes are periodically made to the Web Site. The Firs may amend these terms by posting the amended terms on this Web Site. Continued use of this Web Site following such changes shall constitute your acceptance of such changes.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Age and Responsibility</strong></h1>\r\n\r\n<p style=\"text-align:justify; font-size:14px;\">You represent you are of sufficient legal age to use this Web Site and you possess the legal right and ability to enter into this Agreement and use this site in accordance with all of the applicable terms and conditions and create binding legal obligations for any liability you may incur as a result of the use of this Web Site under your name or account including, without limitation, all use of your account by others including minors living with you.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Child Policy </strong></h1>\r\n\r\n<p style=\"text-align:justify; font-size:14px;\">0 - 2 years     -  FOC <br>2 - 12 years   - 50% discount on the per person sharing DBL rate</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Cancellation Policy </strong></h1>\r\n                                <ul class=\"priv-poly\">\r\n                                    <li>Cancelations made fourteen (14) or more days prior to the date of arrival will incur a 0% charge. (100 % Refund)</li>\r\n                                    <li>Cancelations made three (3) to fourteen (14) days prior to the date of arrival will incur a 50% of first night Room charge.</li>\r\n                                    <li>Cancelations made less than two (2) days prior to the arrival date will incur a 100% of First Night room charge.</li>\r\n                                    <li>All No shows will be charged at 100% of their full stay. (Include meal plans).</li>\r\n                                    <li>If the room you are booking is labeled as non-refundable, non-cancellable or similar, all cancellations will incur a 100% charge, regardless of the date in which the cancellation is requested.</li>\r\n                                </ul>\r\n                                \r\n<p style=\"text-align:justify; font-size:14px;\">Cancellation policies can vary seasonally or depending on the hotel room type.</p> \r\n'),
(51, 94, '', 'Privacy Policy', '<p style=\"text-align:justify; font-size:15px;\">This Privacy Policy governs the manner in which The Firs collects, uses, maintains and discloses information collected from users (each, a \"User\") of the website (\"Site\"). This privacy policy applies to the Site and all the services offered by The Firs.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Personal identification information</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, subscribe to the newsletter, fill out a form, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address, mailing address, phone number, credit card information. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Non-personal identification information</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Web browser cookies</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">Our Site may use \"cookies\" to enhance User experience. Users\' browsers places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>How we use collected information</strong></h1>\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\">The Firs may collect and use Users\' personal information for the following purposes:</h1>\r\n\r\n                                <ul class=\"priv-poly\">\r\n                                    <li>To improve customer service - Information you provide helps us respond to your customer service requests and support needs more efficiently.</li>\r\n                                    <li>To improve our Site - We may use feedback you provide to improve our products and services.</li>\r\n                                    <li>To run a promotion, contest, survey or other Site feature - To send Users information they agreed to receive about topics we think will be of interest to them.</li>\r\n                                    <li>To send periodic emails - We may use the email address to send User information and updates pertaining to their order. It may also be used to respond to their inquiries, questions, and/or other requests. If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email or User may contact us via our Site.</li>\r\n                                </ul>\r\n\r\n<div style=\"clear:both\"></div>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>How we protect your information</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">Our Site may use \"cookies\" to enhance User experience. Users\' browsers places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.<br><br>\r\n                                    \r\nSensitive and private data exchange between the Site and its Users happens over a SSL secured communication channel and is encrypted and protected with digital signatures.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Sharing your personal information</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Third party websites</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">Users may find advertising or other content on our Site that link to the sites and services of our partners, suppliers, advertisers, sponsors, licensors and other third parties. We do not control the content or links that appear on these sites and are not responsible for the practices employed by websites linked to or from our Site. In addition, these sites or services, including their content and links, may be constantly changing. These sites and services may have their own privacy policies and customer service policies. Browsing and interaction on any other website, including websites which have a link to our Site, is subject to that website\'s own terms and policies.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Changes to this privacy policy</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">The Firs has the discretion to update this privacy policy at any time. When we do, we will post a notification on the main page of our Site, revise the updated date at the bottom of this page. We encourage users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>\r\n\r\n<h1 class=\"hdr-seven\" style=\"text-align:left; font-size:15px; text-transform:none;\"><strong>Your acceptance of these terms</strong></h1>\r\n<p style=\"text-align:justify; font-size:14px;\">By using this Site, you signify your acceptance of this policy and terms of service. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.</p>\r\n'),
(52, 95, 'Test gg sub title', 'Test GG Main title', '<p>tesfsfaeresfdsfsf<strong>b sdfsfersf</strong>va d<span style=\"text-decoration: underline;\">sfdsfs</span></p>'),
(53, 101, 'Ellen\'s Place', 'Upscale accommodation at Ellen\'s Place', '<p align=\"justify\">Housed in a Colombo mansion, Ellen\'s Place Hotel combines classic and modern d&eacute;cor and it is elegantly designed and delicately furnished to create an atmosphere of grandeur and opulence. The hotel provides guests pleasant accommodation with a myriad of amenities In-room including&nbsp; a mini-fridge, an LCD TV with satellite channels, a digital safe, as well as tea and coffee making facilities. and blissful comforts and ample living space. It offers a wide array of facilities, services and activities to ensure that guests of all ages are well looked after during their stay. The swimming pool built underneath fragrant Araliya (temple) trees at Ellen\'s Place is the ideal place for guests to relax and laze the hours away.</p>'),
(54, 102, 'ROOMS & SUITES', 'Accommodation', '<p style=\"text-align: justify; font-size: 16px;\">Ellen\'s Place offers guests accommodation perfected by a myriad of amenities and blissful comforts. Offering ample living space, all accommodation options at the hotel have been elegantly designed and delicately furnished to create an atmosphere of grandeur and opulence. Relaxation emanates from every guest room and suite and every detail has been thoughtfully refined to highlight the spirit of the hotel.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 13px;\"><strong>Guests may choose from the following accommodation options at Ellen\'s Place:</strong></h1>\r\n<ul class=\"accom-list\">\r\n<li>2 Standard Rooms</li>\r\n<li>4 Deluxe Rooms</li>\r\n<li>1 Suite</li>\r\n</ul>'),
(55, 103, '', 'Super Deluxe', '<p style=\"text-align: justify; font-size: 18px;\">The magnificent Super Deluxe at \'Ellen\'s Place\' epitomises the grand and graceful style that is evident throughout the property and reflects the essence of living like royalty. Tastefully furnished and lavishly decorated, the Super Deluxe offers a wealth of modern amenities including a private jacuzzi for guests to unwind in, within the comfort of their room.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 13px; text-transform: none;\">The Super Deluxe at \'Ellen\'s Place\' offers the following in-room amenities and services:</h1>\r\n<ul class=\"accom-list\">\r\n<li>Complimentary Mineral Water</li>\r\n<li>Room Service</li>\r\n<li>Daily Housekeeping</li>\r\n<li>In-room Dining</li>\r\n<li>Complimentary Butler Service</li>\r\n<li>Spacious Wardrobe</li>\r\n<li>Air Conditioning with Temperature Control</li>\r\n<li>LCD Televisions</li>\r\n<li>Satellite Channels</li>\r\n<li>DVD Players - On Request</li>\r\n<li>Electric Adapters</li>\r\n</ul>\r\n<ul class=\"accom-list\">\r\n<li>Writing Table and Chair</li>\r\n<li>Mini-fridge</li>\r\n<li>Bathroom Toiletries</li>\r\n<li>Extra Blankets and Towels</li>\r\n<li>Iron and Board - On Request</li>\r\n<li>Extra Bed - On Request</li>\r\n<li>Baby Crib - On Request</li>\r\n<li>Tea and Coffee-Making Facilities</li>\r\n<li>Wi-Fi Internet Access</li>\r\n<li>Private Jacuzzi</li>\r\n</ul>'),
(56, 104, '', 'Deluxe Rooms', '<p style=\"text-align: justify; font-size: 18px;\">Four majestic Deluxe Rooms await guests at \'Ellen\'s Place\', offering 25x14 sq.ft. of unparalleled comfort. Rooms are located on the ground floor and the first floor offering delightful views of the poolside, garden or city.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 13px; text-transform: none;\">Deluxe Rooms at Ellen\'s Place offers the following in-room amenities and services:</h1>\r\n<ul class=\"accom-list\">\r\n<li>Complimentary Mineral Water</li>\r\n<li>Room Service</li>\r\n<li>Daily Housekeeping</li>\r\n<li>In-room Dining</li>\r\n<li>Complimentary Butler Service</li>\r\n<li>Spacious Wardrobe</li>\r\n<li>Air Conditioning with Temperature Control</li>\r\n<li>LCD Televisions</li>\r\n<li>Satellite Channels</li>\r\n<li>DVD Players - On Request</li>\r\n</ul>\r\n<ul class=\"accom-list\">\r\n<li>Electric Adapters</li>\r\n<li>Writing Table and Chair</li>\r\n<li>Mini-fridge</li>\r\n<li>Bathroom Toiletries</li>\r\n<li>Extra Blankets and Towels</li>\r\n<li>Iron and Board - On Request</li>\r\n<li>Extra Bed - On Request</li>\r\n<li>Baby Crib - On Request</li>\r\n<li>Tea and Coffee-Making Facilities</li>\r\n<li>Wi-Fi Internet Access</li>\r\n</ul>'),
(57, 105, '', 'Standard Rooms', '<p style=\"text-align: justify; font-size: 18px;\">Located on the ground floor, the two Standard Rooms at \'Ellen\'s Place\' have been elegantly decorated and tastefully furnished to create an atmosphere of absolute peace and harmony for an unforgettable stay.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 13px; text-transform: none;\">Standard Rooms at Ellen\'s Place offers the following in-room amenities and services:</h1>\r\n<ul class=\"accom-list\">\r\n<li>Complimentary Mineral Water</li>\r\n<li>Room Service</li>\r\n<li>Daily Housekeeping</li>\r\n<li>In-room Dining</li>\r\n<li>Complimentary Butler Service</li>\r\n<li>Spacious Wardrobe</li>\r\n<li>Air Conditioning with Temperature Control</li>\r\n<li>LCD Televisions</li>\r\n<li>Satellite Channels</li>\r\n<li>DVD Players - On Request</li>\r\n</ul>\r\n<ul class=\"accom-list\">\r\n<li>Electric Adapters</li>\r\n<li>Writing Table and Chair</li>\r\n<li>Mini-fridge</li>\r\n<li>Bathroom Toiletries</li>\r\n<li>Extra Blankets and Towels</li>\r\n<li>Iron and Board - On Request</li>\r\n<li>Extra Bed - On Request</li>\r\n<li>Baby Crib - On Request</li>\r\n<li>Tea and Coffee-Making Facilities</li>\r\n<li>Wi-Fi Internet Access</li>\r\n</ul>'),
(58, 106, '', 'Facilities', '<p style=\"text-align: justify; font-size: 17px;\">Ellen\'s Place offers a wide array of facilities, services and activities to ensure that guests of all ages are well pampered during their stay. Bask in the true essence of peace at Ellen\'s Place and lose track of time as you indulge in an unforgettable stay at the hotel.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 13px; text-transform: none;\">Ellen\'s Place offers:</h1>\r\n<ul class=\"accom-list\">\r\n<li>An outdoor swimming pool</li>\r\n<li>High speed Wi-Fi</li>\r\n<li>Private function areas</li>\r\n</ul>\r\n<ul class=\"accom-list\">\r\n<li>In-room dining</li>\r\n<li>Room service</li>\r\n<li>Housekeeping</li>\r\n</ul>'),
(59, 107, '', 'Restaurant', '<p style=\"text-align: justify; font-size: 18px;\">Appease your taste-buds with flavours from around the globe. Dining at Ellen\'s Place will take guests on a memorable gastronomical journey, with dishes to please every palate. Choose from a wide range of Sri Lankan and International cuisine that have been exquisitely prepared by our team of culinary experts and relish every meal you have here.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 18px;\">Restaurant Overview</h1>\r\n<p style=\"text-align: left; font-size: 18px;\">Capable of accommodating up to 16 guests, the main restaurant is located on the ground floor and offers delightful views of the pool area and entrance. Breakfast is available from 6:00 am - 11:00 am and la carte menus are available for both lunch and dinner. <br /><br /> Private dining can also be arranged within the hotel premises on request.</p>'),
(60, 108, '', 'Swimming Pool', '<p style=\"text-align: justify; font-size: 18px;\">The swimming pool at Ellen\'s Place is the ideal place for guests to unwind and laze the hours away. Facing the main restaurant, the pool is located underneath fragrant Araliya trees and provides delightful views of the hotel\'s perfectly manicured garden. Guests who simply wish to sit back and relax without getting into the water may laze the hours away in one of the swing chairs by the side of the pool.<br /><br /> A pool bar is also available next to the pool, offering guests a number of mocktails, juices and snacks to choose from. Simply order a cup of tea, coffee or your favourite snack, and our attentive pool staff will be more than delighted to attend to your request.</p>'),
(61, 109, '', 'Location', '<p style=\"text-align: justify; font-size: 16px;\">Home to nearly 800,000 inhabitants, Colombo is the commercial capital and the largest city in Sri Lanka. It is an ideal base for both business and leisure travellers and the vibrant and busy city has a lot to offer, including a mixture of modern life and colonial buildings.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 13px;\"><strong>Attractions in the vicinity include:</strong></h1>\r\n<ul class=\"accom-list\">\r\n<li>The National Museum of Colombo</li>\r\n<li>The Dutch Hospital</li>\r\n<li>The Galle Face Green</li>\r\n<li>The Gangaramaya Temple</li>\r\n<li>Independence Memorial Hall</li>\r\n</ul>'),
(62, 110, '', 'About Us', '<p style=\"text-align: justify; font-size: 17px;\">Ellen\'s Place was named after Ellen Senanayake - the youngest child of Mudaliyar Don Charles Gemoris Attygalle and his wife Peternella Abeykoon. Ellen Senanayake contributed much to Ceylon&rsquo;s independence struggle, being the supportive wife of F. R. Senanayake, one of the post independence national heroes who propelled the Independence movement.<br /><br /> Ellen Senanayake, who at one point owned all the houses and land down Shady Grove Avenue in Colombo 8, built a house for her personal physician who was expected to live there and treat her and other members of her family. Years later, the house was inherited by her great grandson Hon. Vasantha Senanayake, a young and dynamic parliamentarian who decided to convert the beautiful house to a modern boutique hotel, yet retaining some features of its original decor. <br /><br /> He decided to name the property &ldquo;Ellen&rsquo;s Place&rdquo; in memory of his great grandmother, an ardent Buddhist, generous Philanthropist, astute businesswoman and a devoted mother to her six children..</p>'),
(63, 111, '', 'Privacy Policy', '<p style=\"text-align: justify; font-size: 15px;\">This Privacy Policy governs the manner in which Ellen\'s Place collects, uses, maintains and discloses information collected from users (each, a \"User\") of the website (\"Site\"). This privacy policy applies to the Site and all the services offered by Ellen\'s Place.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 15px; text-transform: none;\"><strong>Personal identification information</strong></h1>\r\n<p style=\"text-align: justify; font-size: 14px;\">We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, fill out a form, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address, mailing address, phone number, credit card information. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 15px; text-transform: none;\"><strong>Non-personal identification information</strong></h1>\r\n<p style=\"text-align: justify; font-size: 14px;\">We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 15px; text-transform: none;\"><strong>Web browser cookies</strong></h1>\r\n<p style=\"text-align: justify; font-size: 14px;\">Our Site may use \"cookies\" to enhance User experience. Users\' browsers places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 15px; text-transform: none;\"><strong>How we use collected information</strong></h1>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 15px; text-transform: none;\">Ellen\'s Place may collect and use Users\' personal information for the following purposes:</h1>\r\n<ul class=\"priv-poly\">\r\n<li>To improve customer service - Information you provide helps us respond to your customer service requests and support needs more efficiently.</li>\r\n<li>To improve our Site - We may use feedback you provide to improve our products and services.</li>\r\n<li>To run a promotion, contest, survey or other Site feature - To send Users information they agreed to receive about topics we think will be of interest to them.</li>\r\n<li>To send periodic emails - We may use the email address to send User information and updates pertaining to their order. It may also be used to respond to their inquiries, questions, and/or other requests. If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email or User may contact us via our Site.</li>\r\n</ul>\r\n<div style=\"clear: both;\">&nbsp;</div>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 15px; text-transform: none;\"><strong>How we protect your information</strong></h1>\r\n<p style=\"text-align: justify; font-size: 14px;\">We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.<br /><br /> Sensitive and private data exchange between the Site and its Users happens over a SSL secured communication channel and is encrypted and protected with digital signatures.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 15px; text-transform: none;\"><strong>Sharing your personal information</strong></h1>\r\n<p style=\"text-align: justify; font-size: 14px;\">We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 15px; text-transform: none;\"><strong>Third party websites</strong></h1>\r\n<p style=\"text-align: justify; font-size: 14px;\">Users may find advertising or other content on our Site that link to the sites and services of our partners, suppliers, advertisers, sponsors, licensors and other third parties. We do not control the content or links that appear on these sites and are not responsible for the practices employed by websites linked to or from our Site. In addition, these sites or services, including their content and links, may be constantly changing. These sites and services may have their own privacy policies and customer service policies. Browsing and interaction on any other website, including websites which have a link to our Site, is subject to that website\'s own terms and policies.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 15px; text-transform: none;\"><strong>Changes to this privacy policy</strong></h1>\r\n<p style=\"text-align: justify; font-size: 14px;\">Ellen\'s Place has the discretion to update this privacy policy at any time. When we do, we will post a notification on the main page of our Site, revise the updated date at the bottom of this page. We encourage users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 15px; text-transform: none;\"><strong>Your acceptance of these terms</strong></h1>\r\n<p style=\"text-align: justify; font-size: 14px;\">By using this Site, you signify your acceptance of this policy and terms of service. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.</p>'),
(64, 112, '', 'Terms and Conditions', '<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 16px; text-transform: none;\"><strong>Web Site User Agreement</strong></h1>\r\n<p style=\"text-align: justify; font-size: 15px;\">This Web Site is owned and operated by Ellen\'s Place (hereinafter, as and when the context so requires, called and referred to as \"Ellen\'s Place\").<br /><br /> By accessing and using this Web Site, you agree to be bound by the terms and conditions of use, set out herein. The attached terms of online reservations shall apply, when you make your reservations or purchases online and shall form part and parcel of this Agreement. The Principals of Data Protection of Ellen\'s Place is also annexed hereto for your reference.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 16px; text-transform: none;\"><strong>Terms and Conditions of Use</strong></h1>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 15px; text-transform: none;\"><strong>Acceptable Use</strong></h1>\r\n<p style=\"text-align: justify; font-size: 14px;\">You may use this Web Site in accordance with these terms and conditions for lawful and proper purposes complying with all applicable laws, regulations and codes of practice in Sri Lanka and/or other jurisdictions from which you are accessing the Web Site.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 15px; text-transform: none;\">In particular you agree you will not -</h1>\r\n<ul class=\"priv-poly\">\r\n<li style=\"margin-bottom: 10px;\">Post, transmit or disseminate any information via this Web Site which is or may be harmful, obscene or otherwise illegal.</li>\r\n<li style=\"margin-bottom: 10px;\">Use this Web Site in a manner which causes or may cause infringement of the rights of another.</li>\r\n<li style=\"margin-bottom: 10px;\">Use any software, routine, procedure or device to interfere or attempt to interfere with the operation or functionality of this Web Site including but not limited to making available corrupt and/or fraudulent data, viruses via whatever means or interfere with its availability to others.</li>\r\n<li style=\"margin-bottom: 10px;\">Deface, alter or interfere with the look and feel of this Web Site or underlying software code.</li>\r\n<li style=\"margin-bottom: 10px;\">Take any action that imposes an unreasonable or disproportionately large load on the Web Site or related infrastructure.</li>\r\n<li style=\"margin-bottom: 10px;\">Obtain or attempt to obtain unauthorized access by whatsoever means to any of our networks.</li>\r\n</ul>\r\n<p style=\"text-align: justify; font-size: 14px;\">Where Ellen\'s Place believes in its absolute discretion that you are in breach of any of these terms and conditions, without prejudice to any of our rights at law or otherwise, we reserve the right to deny you access to this Web Site.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 15px; text-transform: none;\"><strong>Copyright and Restrictions on Use</strong></h1>\r\n<p style=\"text-align: justify; font-size: 14px;\">All trademarks, service marks, copyright, database rights, and other intellectual property rights in materials on this Web Site as well as the organization and layout of this Web Site together with the underlying software belong to Ellen\'s Place or its licensors. The trademark, service mark and logos of Ellen\'s Place and its licensors must not be used, modified or adapted in any way without the prior written authorization. The material on this Web Site and the underlying software codes may not be copied, modified, altered, published, broadcast, distributed, linked to another Web Site, sold or transferred in whole or in part without an express written permission. However, the information contained here may be downloaded, printed or copied for your personal non-commercial use.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 15px; text-transform: none;\"><strong>Disclaimer, Limitation of Liability and Indemnity</strong></h1>\r\n<p style=\"text-align: justify; font-size: 14px;\">This Web Site has been compiled in good faith by Ellen\'s Place. No representation is made or warranty given, either express or implied as to the completeness or accuracy of the information that it contains, that it will be uninterrupted or error free or that any information is free of bugs, viruses, worms, trojan horses or other harmful components.</p>\r\n<ul class=\"priv-poly\">\r\n<li style=\"margin-bottom: 10px;\">To the maximum extent permitted by law, Ellen\'s Place disclaims all implied warranties with regard to information, products, services and material provided through this Web Site. All such information, products, services and materials are provided \"as is\" and \"as available\" without warranty of any kind.</li>\r\n<li style=\"margin-bottom: 10px;\">This Web Site may contain links to Web Sites operated by third parties. Such links are provided for your reference only and your use of these sites may be subject to terms and conditions posted on them. Ellen\'s Place inclusion of links to other Web Sites does not imply any endorsement of the material on such Web Sites and Ellen\'s Place accepts no liability for their contents.</li>\r\n<li style=\"margin-bottom: 10px;\">By accessing this Web Site you agree that Ellen\'s Place will not be liable for any direct, indirect or consequential loss or damages of any nature arising from the use of this Web Site, including information and material contained in it, from your access of other material on the Internet via web links from this Web Site, delay or inability to use this Web Site or the availability and utility of the products and services.</li>\r\n<li style=\"margin-bottom: 10px;\">You further agree to indemnify, hold Ellen\'s Place, and its service providers harmless from and covenant not to sue Ellen\'s Place for any claims based on using this Web Site.</li>\r\n</ul>\r\n<div style=\"clear: both;\">&nbsp;</div>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 15px; text-transform: none;\"><strong>Modification of Terms</strong></h1>\r\n<p style=\"text-align: justify; font-size: 14px;\">Ellen\'s Place may make improvements and/or changes in the products, services and prices described in this Web Site at anytime without notice. Changes are periodically made to the Web Site. Ellen\'s Place may amend these terms by posting the amended terms on this Web Site. Continued use of this Web Site following such changes shall constitute your acceptance of such changes.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 15px; text-transform: none;\"><strong>Age and Responsibility</strong></h1>\r\n<p style=\"text-align: justify; font-size: 14px;\">You represent you are of sufficient legal age to use this Web Site and you possess the legal right and ability to enter into this Agreement and use this site in accordance with all of the applicable terms and conditions and create binding legal obligations for any liability you may incur as a result of the use of this Web Site under your name or account including, without limitation, all use of your account by others including minors living with you.</p>\r\n<h1 class=\"hdr-seven\" style=\"text-align: left; font-size: 15px; text-transform: none;\"><strong>Cancellation Policy </strong></h1>\r\n<ul class=\"priv-poly\">\r\n<li>Cancelations made fourteen (14) or more days prior to the date of arrival will incur a 0% charge. (100 % Refund)</li>\r\n<li>Cancelations made three (3) to fourteen (14) days prior to the date of arrival will incur a 50% of first night Room charge.</li>\r\n<li>Cancelations made less than two (2) days prior to the arrival date will incur a 100% of First Night room charge.</li>\r\n<li>All No shows will be charged at 100% of their full stay. (Include meal plans).</li>\r\n<li>If the room you are booking is labeled as non-refundable, non-cancellable or similar, all cancellations will incur a 100% charge, regardless of the date in which the cancellation is requested.</li>\r\n</ul>\r\n<p style=\"text-align: justify; font-size: 14px;\">Cancellation policies can vary seasonally or depending on the hotel room type.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tbldownloaditems`
--

CREATE TABLE `tbldownloaditems` (
  `id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  `item_title` varchar(255) NOT NULL,
  `item_file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbldownloaditems`
--

INSERT INTO `tbldownloaditems` (`id`, `property_id`, `item_title`, `item_file`) VALUES
(3, 2, 'Brochure', 'b6f6139840642d776a5c8e0801f434a5_2094286116.exe'),
(5, 2, 'Fact Sheet', '10d842c4a38a8cfa0dde6395812f18cc_519893970.pdf'),
(6, 3, 'Fact Sheet', '142dab33ef6eeabf01a4074b851f64eb_814321401.pdf'),
(7, 4, 'Fact Sheet', 'd9af4974d1dc1e7473990fe62cd3b1df_1078010051.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tblexperiences`
--

CREATE TABLE `tblexperiences` (
  `id` int(11) NOT NULL,
  `tagline` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `destination_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblexperiences`
--

INSERT INTO `tblexperiences` (`id`, `tagline`, `description`, `image`, `destination_id`) VALUES
(5, 'The Gangaramaya Temple', 'Witness the Gangaramaya, a temple with unique architecture and beautiful surroundings in the heart of Colombo', '3a85bd493b08d83f4bc265c813671b81_2065460481.jpg', 1),
(6, 'The Galle Face Green', 'An Urban Park Facing The Glorious Indian Ocean', '36cecab8457cab1ac3195649636546d3_595110871.jpg', 1),
(7, 'Independence Memorial Hall', 'A Beautiful Monument Commemorating Sri Lanka\'s Independence ', 'db94325aa3385d750d0decec729c7f59_1358098337.jpg', 1),
(8, 'Viharamahadevi Park', 'A Public Park Located A Few Minutes Away From The Sri Lanka National Museum ', 'cc999083d458e25a52585ab41347a31b_1999697034.jpg', 1),
(9, 'Colombo Golf Club', 'Play A Game Of Golf At The Colombo Golf Club', 'd494716693a6e71b90897553a11c081f_653100711.jpg', 1),
(10, 'Esala Perahera', 'Witness a majestic elephant pageant', 'b90f872dcef41dfa8a087aacfa965f45_1215976796.jpg', 2),
(11, 'Temple of the Tooth Relic', 'The Sri Dalada Maligawa houses the tooth relic of Lord Buddha', 'a46de24031bb84aba6e5574d22f68cad_972301920.jpg', 2),
(12, 'Peradeniya Botanical Garden', 'A spectacular botanical garden with a variety trees and flowers', 'ebdd948622bcb53fbdada9986093e66e_1978577761.jpg', 2),
(13, 'Pinnawala Elephant Orphange', 'Learn all about the lives of the World’s largest land animals', '015d4023f15b9eb3fbdfa2ce90c707f9_1397601076.jpg', 2),
(14, 'Kandy Lake', 'A beautiful lake in the vibrant and exotic locale of Kandy', '89e1fc30c23f3e16ca6332ffa30842df_1118970805.jpg', 2),
(15, 'Hantana', 'Hantana, the scenic paradise of the Sri Lankan hill country awaits you', '97fb3a31dec0fcea015f8fdc771b4c3c_249302514.jpg', 2),
(16, 'Queen Victoria Park', 'Named after Queen Victoria to commemorate her diamond jubilee. A gorgeous public park in Nuwara Eliya', '5e2847efb60049a33eff66c219b6da29_468080088.jpg', 3),
(17, 'Gregory Lake', 'A lake surrounded by breathtaking mountain ranges', '96f2f58daf9ecae6c05579c70d8c5e8a_636247734.jpg', 3),
(18, 'Horton Plains National Park', 'Witness a stunning 880m plunge, commonly referred to as “the world’s end”', '84a7257f672450ea819ad1be3444eabf_836904270.jpg', 3),
(19, 'Botanical Garden Hakgala', 'A temperate wonderland that flaunts a variety of plant species', 'b3b66045e70dd90d5b478fa91686ad2f_649693701.jpg', 3),
(20, 'Golf Club Nuwara Eliya', 'Play an enthralling and entertaining game of golf in Nuwara Eliya', '390c836c4ca070ea00a6c60a05cd860c_579411820.jpg', 3),
(21, 'Maskeliya', 'Maskeliya Reservoir from Sri Pada', 'ac08cf0eccb7cc45b15d705ab3319d00_2074842981.jpg', 3),
(22, 'St. Clair\'s Waterfall', 'A site that is alluring, jaw-dropping and spectacular', '609cf3b558f62afbd9724c661d2e2a58_826522277.jpg', 3),
(23, 'Independence Arcade', 'The ideal place for shopping, fun and food in the heart of Colombo', '1ef22544861d5b28c219f19b3273286d_163556636.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblnavigation`
--

CREATE TABLE `tblnavigation` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `target_pg` int(11) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblnavigation`
--

INSERT INTO `tblnavigation` (`id`, `page_id`, `title`, `description`, `target_pg`, `image`) VALUES
(39, 1, 'ROMANCE & RELAXATION', 'Romance, Privacy and Opulence\r\nRead More', 44, '60e397242800a50df00276577433ab7f_1366634232.jpg'),
(40, 43, 'Romance & Relaxation', 'Peace And Quite Romantic\r\nSee All Experiences', 44, '18437af6163f7ce174475de320d4c25f_1763476931.jpg'),
(41, 43, 'Infinity Pool', 'Immerse Yourself In The Refreshing Water\r\nRead More', 45, 'd8a2b87413e7ba4e24aa370b88a80c51_873178978.jpg'),
(42, 43, 'Promotions', 'See All Promotions', 70, 'd2c243897013a83aedf5d49bf4088cf7_442941691.jpg'),
(43, 43, '360 Experience', 'View', 46, 'c99921aaae84b7cc46b433a868333861_577765765.jpg'),
(44, 43, 'Gallery', 'View', 41, '036955aeca37310e5d5e8699dcb54484_576484767.jpg'),
(45, 47, 'Suites', '<div class=\"hdr-two fadeitem\">The magnificent suite</div>      \r\n<p class=\"fadeitem\">The grand and graceful style that is evident throughout the property and reflects the essence of living like royalty</p>', 48, '50a7afc5bcedc1da893b274cb86463aa_1021849027.jpg'),
(46, 47, 'Deluxe Mountain View', '<div class=\"hdr-two fadeitem\">Upscale Comfort</div>      \r\n<p class=\"fadeitem\">Step into an extraordinary world fashioned for uninterrupted and peace.</p> ', 49, '1b8cc816a2d2e91e2c620b73c344cf2b_1907684800.jpg'),
(47, 47, 'Deluxe', '<div class=\"hdr-two fadeitem\">Absolutely Breathtaking</div> \r\n<p class=\"fadeitem\">Nestled in the heart of Mount Pleasant and surrounded by its beauty.</p>', 50, 'd0cd1c95bd4168b93f5220b1bb531776_618190892.jpg'),
(48, 51, 'Bar', '<div class=\"hdr-two\">Bar</div>\r\n<p class=\"fadeitem\">Step into an extraordinary world fashioned for uninterrupted, luxurious peace.</p>', 52, '9a39766d0b0b1f254b62c2928ca07db0_612896143.jpg'),
(49, 51, 'Signature Dinning', '<div class=\"hdr-two\">Absolutely Breathtaking</div>\r\n<p class=\"fadeitem\">Nestled in the heart of Mount Pleasant and surrounded by its beauty.</p>', 53, 'c77dd84bbdbd3ed12b05754574f2234f_1785646307.jpg'),
(50, 1, 'RANDHOLEE PROMOTIONS', 'Exciting Deals & Offers\r\nRead More', 70, '01d394bdc6826468b5ea03e413b29acc_941200811.jpg'),
(51, 1, 'FIRS PROMOTIONS', 'Great Savings And Promotions\r\nRead More', 71, '8d60e2760cfdcdabe5b73afcefff2a26_1582480878.jpg'),
(52, 1, 'EXPERIENCES', 'Explore The Perl Of The Indian Ocean\r\nRead More', 4, 'ed1df408dbf2adee809ddc793ba762be_55682808.jpg'),
(53, 1, 'ELLEN\'S PROMOTIONS', 'Amazing Discounts & Promotions\r\nRead More', 72, 'd67356f7055cdd30fbee5d3bf49f8486_814536741.jpg'),
(54, 55, 'Squash Court', 'a state-of-the-art squash court', 56, 'b471b348fbd71829e2d79bddf79a3c1c_1485376499.jpg'),
(55, 55, 'Badminton & Table Tennis', 'Ideal for family and friends', 57, '01e3d34eb0af226de843897b4b8be232_235974397.jpg'),
(56, 55, 'Shopping Boutique', 'Choose from an assortment of souvenirs and precious gemstones at Randholee\'s shopping boutique and take back a few mementos of your visit to Kandy.', 58, 'a1f84b18b3d87c45b7a373dc3366cba8_1254595893.jpg'),
(57, 55, 'Infinity Pool', 'Immerse yourself in the refreshing water', 45, '1bfb82a6ce4af450ad96cbbc5f7c8209_1172424134.jpg'),
(58, 55, 'Wedding', 'A royal wedding experience', 80, '23348bcad0a64fa25a3c14b566ff6d16_820379356.jpg'),
(59, 55, 'Spa', 'Revel in a blissful sensory experience at spa', 59, 'e3c2e25140d230abfd1dd6388e8944c1_2133532535.jpg'),
(60, 55, 'Conferences and Workshops', 'The remarkable conference centre with a resplendent view.', 60, 'c2340f7129a8da5dbaa40d01d00c3a0f_215145447.jpg'),
(61, 55, 'Fitness Centre', 'A state-of-the-art gym', 61, 'f554e43d1a7c987ebc2da04a382101bd_1036629430.jpg'),
(62, 55, 'Business Centre', 'Randholee Luxury Resort offers our esteemed guests various facilities to utilize when conducting business from our hotel, leaving you free to leave your laptops and cell phones at home.', 62, '770458cbcd0d055f2fa501b443818e1a_948444702.jpg'),
(63, 82, 'Upscale Suites', 'Five Tastefully - Furnished and Upscale Suites\r\nRead More', 85, 'a0862c2bf6f6ea3b9bb53da285dcd1d6_1772783675.jpg'),
(64, 82, 'Highlights', 'The most picturesque sights and historical attractions\r\nRead More', 83, '434bff1656dbea98d0bfa5760f61345e_1518758579.jpg'),
(65, 82, 'Promotions', ' See All Promotions', 71, '79b8e830e930bf701f614c8896ddafe9_1134961938.jpg'),
(66, 82, '360 Experience', ' View', 84, '608ddd08b1562693435dadc5fa5ee8dc_558748523.jpg'),
(67, 82, 'Getting Here', ' View', 83, 'c34eb256196ae651dd0b1cbad8e99f4d_1589976321.jpg'),
(69, 87, 'Restaurant', 'Tantalise your taste-buds with a symphony of flavours from around the globe\r\nRead More', 88, '3bb60a0cfa5186fe082a19903a6748bb_319275162.jpg'),
(70, 87, 'Room Services', 'The Firs strives to provide superior levels of comfort and all-round impeccable service to guests during their stay at the hotel\r\nRead More', 89, '391800e08a0e6865242c605dab0fec64_1175125357.jpg'),
(71, 6, 'Warm and Welcoming', 'The Kandyan paintings and the rich mahogany furniture add to this enchanting atmosphere', 47, '0ec30d4117fad78f2b3945c3a35de42f_1320642432.jpg'),
(72, 6, 'Old World Charm and Elegance', 'An air of luxury that create an atmosphere of ultimate serenity and respite', 85, '5105b21607636702b3220289d4d56b74_782599913.jpg'),
(73, 6, 'An Ample Living Space', 'Offers guests luxurious accommodations perfected by a myriad of amenities and blissful comforts', 102, '3398effccd3dc5eecf2f8064747f459a_1092317006.jpg'),
(74, 101, 'Golf Club', 'Read More', 101, '3fbc7d7fe490100d11653cfc08629b37_1551329972.jpg'),
(75, 101, 'Swimming Pool', 'To unwind and laze the hours away\r\nRead More', 108, 'dfa477e344cda820ac265b42888467ff_266179301.jpg'),
(76, 101, 'Promotions', 'See All Promotions', 72, '5144b1ed902b4a8442d6f2e8bd6e17a8_637807513.jpg'),
(77, 101, 'Suites', 'Te grand and graceful style\r\nRead More', 103, 'e6b4320eb70c13a8243ff5ab8781ee91_655724356.jpg'),
(78, 101, 'Getting Here', 'View', 101, 'd5b0c6df1d6333ca9736cefcd6b15f5c_850281682.jpg'),
(79, 102, 'Suites', 'The magnificent suite\r\nThe grand and graceful style that is evident throughout the property and reflects the essence of living like royalty\r\nView All', 103, '382539aa9243bcecc9014659f15d8aa5_238661993.jpg'),
(80, 102, 'Standard Rooms', 'An atmosphere of absolute peace and harmony\r\nView All', 105, '7fef3f5ef6e4e2d10379dd0b147e2f79_1684896238.jpg'),
(81, 102, 'Deluxe Rooms', 'Majestic Deluxe Rooms\r\nView All', 104, 'b751a8e4c981d77d9d04586f46fce76d_44507278.jpg'),
(82, 106, 'Restaurant', 'Sri Lankan and International cuisine\r\nA memorable gastronomical journey, with dishes to please every palate\r\nView All', 107, '0232742900af79d4255d5e65ec3c7fd7_2046958800.jpg'),
(83, 106, 'Swimming Pool', 'Unwind and Laze the hours away\r\nThe ideal place for guests to unwind and laze the hours away\r\nView All', 108, '56d2087d24a147cd4e08649409c0b8e1_578686205.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tblpages`
--

CREATE TABLE `tblpages` (
  `id` int(11) NOT NULL,
  `page_name` varchar(50) NOT NULL,
  `page_template` varchar(50) NOT NULL,
  `child_of` int(11) NOT NULL DEFAULT '0',
  `property_id` int(11) NOT NULL DEFAULT '0',
  `page_slug` varchar(25) NOT NULL,
  `section_desc` tinyint(1) DEFAULT '1',
  `section_nav` tinyint(1) DEFAULT '0',
  `section_slider` tinyint(1) DEFAULT '0',
  `section_gallery` tinyint(1) DEFAULT '0',
  `allow_subpages` tinyint(1) DEFAULT '0',
  `show_tabs` tinyint(1) DEFAULT '1',
  `visible_in` tinyint(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblpages`
--

INSERT INTO `tblpages` (`id`, `page_name`, `page_template`, `child_of`, `property_id`, `page_slug`, `section_desc`, `section_nav`, `section_slider`, `section_gallery`, `allow_subpages`, `show_tabs`, `visible_in`) VALUES
(1, 'Home', 'standard-page.tpl', 0, 1, 'home', 1, 1, 1, 0, 0, 1, 1),
(4, 'Experiences', 'standard-page.tpl', 0, 1, 'experience', 1, 0, 1, 0, 0, 1, 1),
(6, 'Accommodation', 'standard-page.tpl', 0, 1, 'accommodation', 1, 1, 1, 0, 0, 1, 1),
(7, 'About Us', 'standard-page.tpl', 0, 1, 'aboutus', 1, 0, 1, 0, 0, 1, 1),
(8, 'Terms and Conditions', 'standard-page.tpl', 0, 1, 'tandc', 1, 0, 0, 0, 0, 1, 1),
(9, 'Privacy Policy', 'standard-page.tpl', 0, 1, 'pp', 1, 0, 0, 0, 0, 1, 1),
(10, 'Downloads', 'standard-page.tpl', 0, 1, 'downloads', 1, 0, 1, 0, 0, 1, 1),
(27, 'Slider(Main)', 'standard-page.tpl', 0, 1, 'manage-slider', 0, 0, 2, 0, 0, 0, 2),
(28, 'Gallery(Main)', 'standard-page.tpl', 0, 1, 'manage-gallery', 0, 0, 1, 1, 0, 0, 2),
(29, 'Promotions', 'standard-page.tpl', 0, 0, 'manage-promotions', 0, 0, 1, 0, 0, 0, 2),
(34, 'Gallery(Firs)', 'standard-page.tpl', 0, 3, 'manage-gallery', 0, 0, 1, 1, 0, 0, 2),
(35, 'Gallery(Ellens)', 'standard-page.tpl', 0, 4, 'manage-gallery', 0, 0, 1, 1, 0, 0, 2),
(37, 'Slider(Firs)', 'standard-page.tpl', 0, 3, 'manage-slider', 0, 0, 2, 0, 0, 0, 2),
(38, 'Slider(Ellens)', 'standard-page.tpl', 0, 4, 'manage-slider', 0, 0, 2, 0, 0, 0, 2),
(41, 'Gallery(Randholee)', 'standard-page.tpl', 0, 2, 'manage-gallery', 0, 0, 1, 1, 0, 0, 2),
(42, 'Slider(Randholee)', 'standard-page.tpl', 0, 2, 'manage-slider', 0, 0, 2, 0, 0, 0, 2),
(43, 'Home', 'standard-page.tpl', 0, 2, 'home', 1, 1, 1, 0, 0, 1, 1),
(44, 'Honeymoon', 'standard-page.tpl', 0, 2, 'honeymoon', 1, 0, 2, 0, 0, 1, 0),
(45, 'Infinity Pool', 'standard-page.tpl', 55, 2, 'infinity-pool', 1, 0, 2, 1, 0, 1, 1),
(46, 'Randholee 360 Experience', 'standard-page.tpl', 0, 2, 'poolview', 1, 0, 0, 0, 0, 1, 0),
(47, 'Accommodation', 'standard-page.tpl', 0, 2, 'accommodation', 1, 1, 2, 0, 1, 1, 1),
(48, 'Super Deluxe', 'standard-page.tpl', 47, 2, 'super-deluxe', 1, 0, 2, 0, 0, 1, 1),
(49, 'Deluxe Mountain View', 'standard-page.tpl', 47, 2, 'delux-mountain-view', 1, 0, 2, 1, 0, 1, 1),
(50, 'Standard', 'standard-page.tpl', 47, 2, 'standard', 1, 0, 2, 0, 0, 1, 1),
(51, 'Cuisine', 'standard-page.tpl', 0, 2, 'cuisine', 1, 1, 2, 0, 1, 1, 1),
(52, 'Bar', 'standard-page.tpl', 51, 2, 'bar', 1, 0, 2, 1, 0, 1, 1),
(53, 'Signature Dining', 'standard-page.tpl', 51, 2, 'signature-dinning', 1, 0, 2, 1, 0, 1, 1),
(54, 'Buffet', 'standard-page.tpl', 51, 2, 'buffet', 1, 0, 2, 1, 0, 1, 1),
(55, 'Facilities', 'standard-page.tpl', 0, 2, 'facilities', 1, 1, 2, 0, 1, 1, 1),
(56, 'Squash Court', 'standard-page.tpl', 55, 2, 'squash-court', 1, 0, 2, 1, 0, 1, 1),
(57, 'Badminton And Table Tennis', 'standard-page.tpl', 55, 2, 'badminton-n-table-tennis', 1, 0, 2, 1, 0, 1, 1),
(58, 'Shopping Boutique', 'standard-page.tpl', 55, 2, 'shopping-boutique', 1, 0, 1, 1, 0, 1, 1),
(59, 'Spa', 'standard-page.tpl', 55, 2, 'spa', 1, 0, 2, 1, 0, 1, 1),
(60, 'Conferences And Workshops', 'standard-page.tpl', 55, 2, 'conferences-and-workshops', 1, 0, 1, 1, 0, 1, 1),
(61, 'Fitness Centre', 'standard-page.tpl', 55, 2, 'fitness-centre', 1, 0, 2, 1, 0, 1, 1),
(62, 'Business Centre', 'standard-page.tpl', 55, 2, 'business-centre', 1, 0, 1, 1, 0, 1, 1),
(63, 'About Us', 'standard-page.tpl', 0, 2, 'about-us', 1, 0, 1, 0, 0, 1, 1),
(64, 'Terms and Conditions', 'standard-page.tpl', 0, 2, 'terms-and-conditions', 1, 0, 0, 0, 0, 1, 1),
(65, 'Privacy Policy', 'standard-page.tpl', 0, 2, 'privacy-policy', 1, 0, 0, 0, 0, 1, 1),
(69, 'Downloads', 'standard-page.tpl', 0, 0, 'downloads', 0, 0, 0, 0, 0, 0, 2),
(70, 'Promotions', 'standard-page.tpl', 0, 2, 'promotions', 1, 0, 1, 0, 0, 1, 1),
(71, 'Promotions', 'standard-page.tpl', 0, 3, 'promotions', 1, 0, 1, 0, 0, 1, 1),
(72, 'Promotions', 'standard-page.tpl', 0, 4, 'promotions', 1, 0, 1, 0, 0, 1, 1),
(73, 'Promotions', 'standard-page.tpl', 0, 1, 'promotions', 1, 0, 1, 0, 0, 1, 1),
(79, 'Freudenberg Promotion', 'standard-page.tpl', 73, 1, 'freudenberg-promotion', 1, 0, 2, 0, 0, 1, 1),
(80, 'Wedding', 'standard-page.tpl', 55, 2, 'wedding', 1, 0, 2, 1, 0, 1, 1),
(81, 'Long Stay Offer', 'standard-page.tpl', 70, 2, 'long-stay-offer', 1, 0, 2, 0, 0, 1, 1),
(82, 'Home', 'standard-page.tpl', 0, 3, 'home', 1, 1, 1, 0, 0, 1, 1),
(83, 'Location', 'standard-page.tpl', 0, 3, 'location', 1, 0, 1, 1, 0, 1, 1),
(84, 'Lobby Home', 'standard-page.tpl', 0, 3, 'lobby-home', 1, 0, 0, 0, 0, 1, 1),
(85, 'Accommodation', 'standard-page.tpl', 0, 3, 'accommodation', 1, 0, 2, 1, 1, 1, 1),
(86, 'Bar', 'standard-page.tpl', 87, 3, 'bar', 1, 0, 2, 1, 0, 1, 0),
(87, 'Facilities', 'standard-page.tpl', 0, 3, 'facilities', 1, 1, 2, 0, 1, 1, 1),
(88, 'Restaurant', 'standard-page.tpl', 87, 3, 'restaurant', 1, 0, 2, 1, 0, 1, 1),
(89, 'Room Service', 'standard-page.tpl', 87, 3, 'room-service', 1, 0, 2, 1, 0, 1, 1),
(90, 'Advance Purchase', 'standard-page.tpl', 71, 3, 'advance-purchase', 1, 0, 2, 0, 0, 1, 1),
(91, 'About Us', 'standard-page.tpl', 0, 3, 'about-us', 1, 0, 2, 0, 0, 1, 1),
(92, 'Downloads', 'standard-page.tpl', 0, 3, 'downloads', 1, 0, 1, 0, 0, 1, 1),
(93, 'Terms and Conditions', 'standard-page.tpl', 0, 3, 'terms-and-conditions', 1, 0, 0, 0, 0, 1, 1),
(94, 'Privacy Policy', 'standard-page.tpl', 0, 3, 'privacy-policy', 1, 0, 0, 0, 0, 1, 1),
(98, 'Downloads', 'standard-page.tpl', 0, 2, 'downloads', 1, 0, 1, 0, 0, 1, 0),
(101, 'Home', 'standard-page.tpl', 0, 4, 'home', 1, 1, 1, 0, 0, 1, 1),
(102, 'Accommodation', 'standard-page.tpl', 0, 4, 'accommodation', 1, 1, 2, 0, 1, 1, 1),
(103, 'Suites', 'standard-page.tpl', 102, 4, 'super-deluxe-rooms', 1, 0, 2, 1, 0, 1, 1),
(104, 'Deluxe Rooms', 'standard-page.tpl', 102, 4, 'deluxe-rooms', 1, 0, 2, 1, 0, 1, 1),
(105, 'Standard Rooms', 'standard-page.tpl', 102, 4, 'standard-rooms', 1, 0, 2, 1, 0, 1, 1),
(106, 'Facilities', 'standard-page.tpl', 0, 4, 'facilities', 1, 1, 1, 0, 1, 1, 1),
(107, 'Restaurant', 'standard-page.tpl', 106, 4, 'restaurant', 1, 0, 2, 1, 0, 1, 1),
(108, 'Swimming Pool', 'standard-page.tpl', 106, 4, 'swimming-pool', 1, 0, 2, 1, 0, 1, 1),
(109, 'Location', 'standard-page.tpl', 0, 4, 'location', 1, 0, 1, 0, 0, 1, 1),
(110, 'About Us', 'standard-page.tpl', 0, 4, 'about-us', 1, 0, 1, 0, 0, 1, 1),
(111, 'Privacy Policy', 'standard-page.tpl', 0, 4, 'privacy-policy', 1, 0, 0, 0, 0, 1, 1),
(112, 'Terms and Conditions', 'standard-page.tpl', 0, 4, 'terms-and-conditions', 1, 0, 0, 0, 0, 1, 1),
(113, 'test promo page-rand', 'standard-page.tpl', 70, 1, 'test-promo-page', 1, 0, 0, 0, 0, 1, 1),
(114, 'demo-page', 'standard-page.tpl', 0, 1, 'demo-page', 0, 1, 1, 0, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblpromotionimages`
--

CREATE TABLE `tblpromotionimages` (
  `id` int(11) NOT NULL,
  `promotion_id` int(11) NOT NULL,
  `image_src` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblpromotionpages`
--

CREATE TABLE `tblpromotionpages` (
  `id` int(11) NOT NULL,
  `promotion_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tblpromotions`
--

CREATE TABLE `tblpromotions` (
  `id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `image` varchar(255) NOT NULL,
  `enabled` tinyint(1) DEFAULT '0',
  `trigger_action` varchar(10) NOT NULL,
  `page_id` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblpromotions`
--

INSERT INTO `tblpromotions` (`id`, `property_id`, `title`, `description`, `image`, `enabled`, `trigger_action`, `page_id`) VALUES
(54, 2, 'Honeymoon at <span>Randholee</span>', 'Wrapped in a pleasant climate, surrounded by the beauty of nature and pampered like royalty, have the most memorable honeymoon.', '4de95e21c6b4f20689547a354e6eb8f3_1854126968.jpg', 1, 'none', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblproperties`
--

CREATE TABLE `tblproperties` (
  `id` int(11) NOT NULL,
  `property_name` varchar(100) NOT NULL,
  `property_desc` text NOT NULL,
  `property_image` varchar(255) DEFAULT NULL,
  `property_tpl` varchar(50) NOT NULL,
  `property_slug` varchar(50) NOT NULL,
  `property_address` varchar(100) DEFAULT NULL,
  `property_telephone` varchar(50) DEFAULT NULL,
  `property_fax` varchar(50) DEFAULT NULL,
  `property_email` varchar(50) DEFAULT NULL,
  `property_logo` varchar(255) DEFAULT NULL,
  `property_map_longitude` varchar(100) DEFAULT NULL,
  `property_map_latitude` varchar(100) DEFAULT NULL,
  `property_map_enabled` tinyint(1) NOT NULL DEFAULT '0',
  `property_map_icon` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblproperties`
--

INSERT INTO `tblproperties` (`id`, `property_name`, `property_desc`, `property_image`, `property_tpl`, `property_slug`, `property_address`, `property_telephone`, `property_fax`, `property_email`, `property_logo`, `property_map_longitude`, `property_map_latitude`, `property_map_enabled`, `property_map_icon`) VALUES
(1, 'Freudenberg Leisure', 'desc', NULL, 'dashboard.tpl', 'freudenberg', '103/14,\r\nDharmapala Mawatha,\r\nColombo 7,\r\nSri Lanka.', '+94 (0) 11 2445282', '+94 (0) 11 2440083', '', '1496648658f02acf930ea6e67b5aad36044514fea9freudenberg.png', '79.8572658', '6.9141868', 0, ''),
(2, 'Randholee Resorts & Spa', 'desc', NULL, 'dashboard.tpl', 'randholee', 'Heerassagala Rd,\r\nBowalawatte,\r\nKandy,\r\nSri Lanka.', '+ 94 81 2217741 – 3', '+ 94 81 2217744', 'reservations@randholeeresorts.com', '14967182812f70094b99e4ab5ea0bbcf97c9874fa7randholee.png', '80.616914', '7.263146', 1, 'map_pin_randholee.png'),
(3, 'The Firs', 'desc', NULL, 'dashboard.tpl', 'firs', 'The Firs\r\n85/2,\r\nUpper Lake Road,\r\nNuwara Eliya,\r\nSri Lanka.', '+94 522 222 387', '+94 522051687', 'info@firs.lk', '1496743716437d03641434ffd2b858e42a4217302bfirs.png', '80.781524', '6.963518', 1, 'map_pin_firs.png'),
(4, 'Ellens Place', 'desc', NULL, 'dashboard.tpl', 'ellens', '31,\r\nShady grove avenue,\r\nColombo 08,\r\nSri Lanka.', '+94 112 680 062', '+94 112 860 063', 'info@ellensplace.lk', '1496972433286716bb5f16461ac5c82cf531192e12ellens.jpg', '79.881787', '6.911567', 0, 'map_pin_ellens.png');

-- --------------------------------------------------------

--
-- Table structure for table `tblslides`
--

CREATE TABLE `tblslides` (
  `id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `section_id` varchar(50) DEFAULT NULL,
  `image_name` varchar(255) NOT NULL,
  `alt_text` varchar(255) DEFAULT NULL,
  `desc_text` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblslides`
--

INSERT INTO `tblslides` (`id`, `page_id`, `section_id`, `image_name`, `alt_text`, `desc_text`) VALUES
(93, 33, 'gallery', 'db8e40a1d62e3b0c9ba695c875bb15dd.jpg', '', ''),
(106, 45, 'gallery', '2d2c4e0e9716123998a0f93231c76572.jpg', '', ''),
(107, 45, 'gallery', 'c5ea5b2650479053e0861f31c7f2e4dd.jpg', '', ''),
(108, 45, 'gallery', '64ad313899f6869588545d9fa903a077.jpg', '', ''),
(109, 45, 'gallery', '1e942027200385005ec4c8021b409536.jpg', '', ''),
(111, 43, 'slider', '6f71feddd101de454a88ca8cf6588672.jpg', '', ''),
(112, 43, 'slider', '1fb33fca8eb0f8b1fc717c84e9540f1a.jpg', '', ''),
(113, 43, 'slider', 'bfcff5f545e9cd1597ba0ba6a26fbd4e.jpg', '', ''),
(114, 43, 'slider', '2417c6e68ca0b8ba190feb3edf026ce4.jpg', '', ''),
(115, 49, 'gallery', '8686252cdae117f4590ee66b2dfa2978.jpg', '', ''),
(116, 49, 'gallery', 'ea8a514ddfce16fc75fe71c76de8c07c.jpg', '', ''),
(117, 49, 'gallery', '23e3ad78a79cf95ce20d2422581f0bde.jpg', '', ''),
(118, 49, 'gallery', '61b64a7eca459d58fa108da2725445b1.jpg', '', ''),
(119, 49, 'gallery', '705897ceaf24139e1bfc80c3a402324b.jpg', '', ''),
(120, 49, 'gallery', '915cecc84b120089baf04a98dd89b97f.jpg', '', ''),
(121, 49, 'gallery', '11e1d955aa6a9313723c264d882f30cc.jpg', '', ''),
(122, 49, 'gallery', '6679746ac316f9fc7fd13e05df843176.jpg', '', ''),
(123, 50, 'slider', 'ccf4f6feb43c2a7690ecadb21eb5586c.jpg', '', ''),
(124, 50, 'slider', '0d4bfc37039bc81bfc49079eb8e21195.jpg', '', ''),
(125, 50, 'slider', 'b19b869e3959ae239cdff59b20eae75e.jpg', '', ''),
(126, 49, 'slider', '888bf2f72581a0458139ff9a01738ed4.jpg', '', ''),
(127, 49, 'slider', 'e5958a1338c85821d4291e6063702160.jpg', '', ''),
(128, 49, 'slider', '44ee2f042c6ca7197e1a46e6316040b1.jpg', '', ''),
(129, 48, 'slider', '63fd45fae61a911b0ca4bb02624da7a9.jpg', '', ''),
(130, 48, 'slider', 'e7b37bcc5379a8ff2d3e8298a3f72d8c.jpg', '', ''),
(131, 48, 'slider', '2b9af377637ed8d825fcf8e1521aa0c6.jpg', '', ''),
(132, 47, 'slider', '121ea71993d7350d0e5b56aab5a068a2.jpg', '', ''),
(133, 47, 'slider', '41ec136d4cfaa9bed7183c2486828967.jpg', '', ''),
(134, 47, 'slider', 'f6e4fb966c81d023b731adcaf86ec0a2.jpg', '', ''),
(135, 47, 'slider', '9bff57b2e3a7de81f7cc3a0d36d1e0bf.jpg', '', ''),
(136, 47, 'slider', 'dcedb56089f5a47c8c00837f30c787e9.jpg', '', ''),
(137, 47, 'slider', '3c85d904fa21be96db5a29ee24d569c0.jpg', '', ''),
(138, 47, 'slider', 'c1974f4e3d93d4b914250a73d770cb7c.jpg', '', ''),
(139, 51, 'slider', '7f4f97391a782241a4244ff56302cbd2.jpg', '', ''),
(140, 51, 'slider', '4074ba5a6745b4aa52fd184aaa029e26.jpg', '', ''),
(141, 51, 'slider', 'd420f03f3f1b4469193c8e15b970c4b6.jpg', '', ''),
(142, 51, 'slider', 'd531df638dfdacf864224d522481e932.jpg', '', ''),
(143, 51, 'slider', 'f84fd17a6ac6fa526a3432675cabfb04.jpg', '', ''),
(144, 52, 'gallery', 'cd6574cdbc6ff76d74eed2133a93f0a0.jpg', '', ''),
(145, 52, 'gallery', 'de1dcb00a1afb9d4e47403fd31abff2f.jpg', '', ''),
(146, 52, 'gallery', 'b9957b1b5628be917ec6c31061d2804d.jpg', '', ''),
(147, 52, 'slider', 'b80decfc3a8e27bda48624ad50f731d6.jpg', '', ''),
(148, 52, 'slider', '41d3ffe71406c4a2f641463fa4063fbe.jpg', '', ''),
(149, 52, 'slider', '8cd26df130e2556e7e12189db7e0275b.jpg', '', ''),
(150, 53, 'gallery', 'fdca59fef117d90b6e805de7a139f067.jpg', '', ''),
(151, 53, 'gallery', '085dc14621cbfc479c1073e79f7cb252.jpg', '', ''),
(152, 53, 'gallery', '039bf88ec8c7acd4f85f368fa57351c0.jpg', '', ''),
(153, 53, 'gallery', '076b33671d1714a9a210b3f1df1090d3.jpg', '', ''),
(154, 53, 'gallery', '0092a0a3d6094f5fe2091befa5d98d37.jpg', '', ''),
(155, 53, 'gallery', 'aa7db3e7e37958bbed739765031a13fb.jpg', '', ''),
(156, 53, 'slider', '2a37c55d071791a3771893d60328a41e.jpg', '', ''),
(157, 53, 'slider', '8432f0f7746c68871b9abba41f31e73d.jpg', '', ''),
(158, 53, 'slider', '02968d7b042f729b0d9a6b0a218c8f46.jpg', '', ''),
(159, 53, 'slider', '1d15096f8bc85fdc3f876aeeaacef806.jpg', '', ''),
(161, 54, 'gallery', '17c5f949ce0717b74c0d98b62179311a.jpg', '', ''),
(162, 54, 'gallery', 'ba39dcf6c22cba4d44f72899c8cb0ad7.jpg', '', ''),
(163, 54, 'gallery', '9c72508eb17cc966eec5fb72eb0faaf8.jpg', '', ''),
(164, 54, 'gallery', '2e9403a5c5944f3d9bd810b8781d39ec.jpg', '', ''),
(165, 54, 'gallery', '16299ab81a557c3f65ce35fe8f951ed3.jpg', '', ''),
(166, 54, 'gallery', 'd1558516aa141ba2963847c30fa271f9.jpg', '', ''),
(167, 54, 'gallery', '8c46d468b0ffb7cf7ff0dc7483952792.jpg', '', ''),
(168, 54, 'gallery', 'fb92b0b2857c470fe50486f6804d0b2c.jpg', '', ''),
(169, 54, 'slider', '9c64a1ceafd3cba67f8b4fa333cdf9ed.jpg', '', ''),
(170, 54, 'slider', 'c99b22494568b76518b56c56576f6bb8.jpg', '', ''),
(171, 54, 'slider', 'a557972741a8277bd731d74c53b022b9.jpg', '', ''),
(172, 54, 'slider', '9054b82cd6433ad7ba36634baac3b50d.jpg', '', ''),
(173, 54, 'slider', 'e43150fcc6b9c5e14c1849db4cad1100.jpg', '', ''),
(176, 55, 'slider', 'b419260bfd419b48f8d1cde4e48baa33.jpg', '', ''),
(177, 55, 'slider', 'a6081b9855abecdaed778ed5e041c307.jpg', '', ''),
(178, 55, 'slider', '760bb97543ce706080640f080f47bee8.jpg', '', ''),
(179, 55, 'slider', 'fb726aa429cbe5da9ae3ce73315f1cb1.jpg', '', ''),
(180, 55, 'slider', 'e61381ab9583981ed89c7969ea2bd36e.jpg', '', ''),
(181, 55, 'slider', 'bc0ce749aec11a8ad3d32cc326e412c5.jpg', '', ''),
(182, 55, 'slider', '2f6efb054059125f57ba4d86ac5099f5.jpg', '', ''),
(183, 55, 'slider', '3e720880c318ec48a8b7d726136adea6.jpg', '', ''),
(184, 55, 'slider', 'ae1c02494f8767a8288dfdb3ab085082.jpg', '', ''),
(185, 55, 'slider', 'd576dd9f5398a9dfdc4da692fdda0af6.jpg', '', ''),
(186, 56, 'slider', 'a9a80dc4326609405dd6a998455bbc4a.jpg', '', ''),
(187, 56, 'slider', 'f2ac17f1af6db88b408e10986b635dd5.jpg', '', ''),
(188, 56, 'slider', 'ca4d0356c14f7fccd546c5b03dfbe18f.jpg', '', ''),
(189, 56, 'gallery', '689b63388ffff5de50192d716587b5a1.jpg', '', ''),
(190, 56, 'gallery', '79e5827b90f797eee20694ce99e90008.jpg', '', ''),
(191, 56, 'gallery', 'eff9c4ed1165048cb64b32b7bb9eb2a1.jpg', '', ''),
(192, 56, 'gallery', '931cfbbf439523e20fa17201e63d7676.jpg', '', ''),
(193, 57, 'gallery', 'e267079d609688c7cb179558aaf43639.jpg', '', ''),
(194, 57, 'gallery', 'c09be3205365bd69149eef1016f65b1e.jpg', '', ''),
(195, 57, 'gallery', '353fdedfb8813255f565ed6aa2a9d65a.jpg', '', ''),
(196, 57, 'gallery', '16f19026d44620d1a14f9971383f0805.jpg', '', ''),
(197, 57, 'gallery', '64c66f31c4d9334b956aa2496e566e06.jpg', '', ''),
(198, 57, 'gallery', '632d2fa3c0357bb9001d77135d5b3dd4.jpg', '', ''),
(199, 57, 'slider', 'c69fa27b33daeb2b0cdcccc6e1f55fc8.jpg', '', ''),
(200, 57, 'slider', '46490b5e4271197bf652e773b053c0ef.jpg', '', ''),
(201, 57, 'slider', 'e3c158267bdf40065b7f3847bc8ad486.jpg', '', ''),
(202, 57, 'slider', '44988d9505c896fb569c0b1be7e9a0b0.jpg', '', ''),
(203, 58, 'gallery', '7fac3c6dae97a0486368fabe4aa9e446.jpg', '', ''),
(204, 58, 'gallery', 'ed2e7401492ece4075dae6374d0254ad.jpg', '', ''),
(205, 58, 'slider', 'fe4905db5af12e0a7ba11717c052e30a.jpg', '', ''),
(206, 58, 'slider', '91a07a9a89228ed3260819de556046e5.jpg', '', ''),
(207, 58, 'slider', '40f32196188f72bac6b59dee36d78353.jpg', '', ''),
(208, 58, 'slider', 'ecf2fcc0cddf6528ed1e8b669fecbd15.jpg', '', ''),
(209, 58, 'slider', '7872be8791f375ee411bd34a02bac9b6.jpg', '', ''),
(210, 59, 'gallery', '85aea0d34ad75ec6f2d9615ce2c0454c.jpg', '', ''),
(211, 59, 'gallery', '87fc251d521e2a8dfb8e9cf265492f5f.jpg', '', ''),
(212, 59, 'gallery', '96f4ebaa001591a5a3c59b72860d4ef4.jpg', '', ''),
(213, 59, 'gallery', 'c97112bf05f1fa3619d24375c8478cfe.jpg', '', ''),
(214, 59, 'slider', '5e446abd8ac9c652691527eb30cbbf63.jpg', '', ''),
(215, 59, 'slider', 'c04919ba45088c4b2ade2d7ee3442ce0.jpg', '', ''),
(216, 59, 'slider', '03fcb41175978788048d041d5f4615c1.jpg', '', ''),
(217, 59, 'slider', '7f566cd51b23c3f5fa8e41a020f6194c.jpg', '', ''),
(218, 59, 'slider', 'e010ec8e05c76bce32002b98bd2fcab8.jpg', '', ''),
(219, 45, 'slider', 'efe7236bfde6682d9028f65c73bc8241.jpg', '', ''),
(220, 45, 'slider', '8fdbe80d64230e42a2c53b52afabef95.jpg', '', ''),
(221, 45, 'slider', '1bad4fcefd20f576cff02f084490419a.jpg', '', ''),
(222, 60, 'gallery', '9b356d13405c4d0eed9cc5acc955fe54.jpg', '', ''),
(223, 60, 'gallery', 'b61b7ed2b4f3b2e9c65e92231f19f9f3.jpg', '', ''),
(224, 60, 'gallery', 'c9a5fc42d71699c48e01adacd1e1f975.jpg', '', ''),
(225, 60, 'gallery', '9a2ccfef6dba216c155300bff5e8834b.jpg', '', ''),
(226, 60, 'gallery', '62c9841ee3b72fe3abb9f0b5edb1e984.jpg', '', ''),
(227, 60, 'slider', '13303324332fd41f54de225e874f842d.jpg', '', ''),
(228, 60, 'slider', 'f74ba3879466726d64d1bfc4965d5ea1.jpg', '', ''),
(229, 60, 'slider', '20822595e8515e590f022da64b6cfbec.jpg', '', ''),
(230, 60, 'slider', '571f341bdb2661421f9b84d331126949.jpg', '', ''),
(231, 60, 'slider', '3f449e37d29f670f1eaaba87fa67b480.jpg', '', ''),
(232, 61, 'gallery', 'a5c5e1d0ee08ce90013cf258617bea53.jpg', '', ''),
(233, 61, 'gallery', '65c9bd642de2f92ff884dd036ceba3ce.jpg', '', ''),
(234, 61, 'gallery', '30e2c0f181c839b3bc95d0ad37c30b01.jpg', '', ''),
(235, 61, 'gallery', '87b99bdd76b19977cfcd785e4cb99eb6.jpg', '', ''),
(236, 61, 'gallery', '2e8cfe07644e87e01b3b6e3991a2d084.jpg', '', ''),
(237, 61, 'gallery', '54833f432fb1e8f25f0f871efab6a1c5.jpg', '', ''),
(238, 61, 'gallery', 'dd8efbb122ba927d8de5ac42cd9d2783.jpg', '', ''),
(239, 61, 'gallery', 'c1dfc854d06faa5df02a88b869231760.jpg', '', ''),
(240, 61, 'slider', 'aab7ba6fbe00503838ed8a7d8d447d62.jpg', '', ''),
(241, 61, 'slider', '7ec79ced1f67f73dca1bca00aec2de70.jpg', '', ''),
(242, 61, 'slider', 'dcfcbf0a0c279f2fdc768863dfcda217.jpg', '', ''),
(243, 61, 'slider', '5dd6ad787be18739527d18255422216a.jpg', '', ''),
(244, 62, 'gallery', '95814a30b9a16bf6d9df118442b7b0e1.jpg', '', ''),
(245, 62, 'gallery', '9dcf9aa70719294cfbaa7c367c9e2cc0.jpg', '', ''),
(246, 62, 'gallery', 'aaf4f4aa5fc0ff1d7cc8fe94782e8d5b.jpg', '', ''),
(247, 62, 'slider', '67d39656337a9600d2affd2f750e3bac.jpg', '', ''),
(248, 62, 'slider', '61b10dc3780e834608030bdf6c3b344e.jpg', '', ''),
(249, 62, 'slider', 'd1d11e1737183d5a4a7e683c1e7cf85b.jpg', '', ''),
(250, 62, 'slider', '9c91bd5de6923b53e158c6b7b40f5d69.jpg', '', ''),
(251, 62, 'slider', '30c6ef814f9f8e9724968920814f1c0e.jpg', '', ''),
(252, 43, 'slider', '3945f6000a82c9e5c1cccd2be4f70125.jpg', '', ''),
(254, 80, 'gallery', '8e44228734643cc0fe513005988e5fe4.jpg', '', ''),
(255, 80, 'gallery', '69ecfce14d8e9cc892329d0164bcdc85.jpg', '', ''),
(256, 80, 'gallery', 'e1c491ffabe1285b593a41f0169f0ed5.jpg', '', ''),
(257, 42, 'slider', '7dbcb373f64314566a998b655aeaa3be.jpg', '', ''),
(258, 42, 'slider', '96347e30cdfdc5e7fb64a7139b685ed9.jpg', '', ''),
(259, 42, 'slider', '0c528ff62e452dd764e2c673c27d683b.jpg', '', ''),
(260, 42, 'slider', '77db3643990f457e63c20bf84863246c.jpg', '', ''),
(261, 42, 'slider', '3ff64bee49d66c6bb3392072031b345d.jpg', '', ''),
(262, 81, 'slider', '025e27a35e7230258e732afa5e0ada17.jpg', '', ''),
(263, 81, 'slider', '9f51ef551ca29b2ab8dadf53db3ffaa1.jpg', '', ''),
(264, 85, 'gallery', 'c6a30aee839c83f71762987e15199248.jpg', '', ''),
(265, 85, 'gallery', '99a5e06223df97ca270487eab69cdb30.jpg', '', ''),
(266, 85, 'gallery', '8646e925c0f30ab954b37b4a70cf811e.jpg', '', ''),
(267, 85, 'gallery', 'd7f1a8c87d3bd021018d62fab45f364f.jpg', '', ''),
(268, 85, 'gallery', '522eddb78ef7edf10c9c443da423c865.jpg', '', ''),
(269, 85, 'gallery', '2e5a0df6533127f5ca0350954e72d739.jpg', '', ''),
(270, 85, 'gallery', 'af84ad13c02c07f53f49ca514c2b9e85.jpg', '', ''),
(271, 85, 'gallery', 'e688b4ccdf93dd680c3e80ffc8b6b2d0.jpg', '', ''),
(272, 86, 'gallery', '72cad617c41f3ef2bfc1e93eb5645444.jpg', '', ''),
(273, 86, 'gallery', '88c1667678bf35fedbd3b06242149b6d.jpg', '', ''),
(274, 86, 'gallery', '4c134fd4daf0f1e2c5de3f661765c053.jpg', '', ''),
(275, 86, 'gallery', '9345839d632e78fa2a49bd483f055739.jpg', '', ''),
(276, 86, 'slider', '4747d6f260e46e3744a0325f5c37a4cc.jpg', '', ''),
(277, 86, 'slider', 'c48e67cbc45a808b4df5573c36c46e90.jpg', '', ''),
(278, 86, 'slider', '21d245d6e4cf0845e432fa7679062454.jpg', '', ''),
(279, 88, 'gallery', '834915e3f651cee2611ea77fb32e9ce8.jpg', '', ''),
(280, 88, 'gallery', 'a9272e38952476c12e9d854505368dca.jpg', '', ''),
(281, 88, 'gallery', 'ff462a87985f51716cae1bff60fa4fa5.jpg', '', ''),
(283, 88, 'gallery', '6c23fa2a868234184cc5aadef1c29274.jpg', '', ''),
(284, 88, 'gallery', '9afe3e7201d304968f830ed77a73d0ee.jpg', '', ''),
(285, 88, 'slider', '6eee92232e809a4f93296ab7c35931e7.jpg', '', ''),
(286, 88, 'slider', 'b6dec32de75844bf8b8b27110f4a2e2b.jpg', '', ''),
(287, 88, 'slider', '64d0a6d2c51acfcb9e462086a590366b.jpg', '', ''),
(288, 88, 'slider', '8af9953e73e241a26fada93f8735d682.jpg', '', ''),
(289, 89, 'gallery', '0fb1397749ed1088d9724b10200d0b6e.jpg', '', ''),
(290, 89, 'gallery', 'fb8ba77cdd0aa6d8e97c6ec7389be921.jpg', '', ''),
(291, 89, 'gallery', '0a44835e4ff5d7401969019a168915b7.jpg', '', ''),
(292, 89, 'gallery', '16b89bc9f8e630d15dba1b5e44abdc96.jpg', '', ''),
(296, 89, 'slider', '10995f735ea71cf4b546082b2fcd1351.jpg', '', ''),
(298, 89, 'slider', 'df3510ea4063a7b5e9f64978a3925c86.jpg', '', ''),
(299, 89, 'slider', '82eb0d8144821c931f1986cf2086211e.jpg', '', ''),
(301, 87, 'slider', '7262d52d265de7ee116894d174d36423.jpg', '', ''),
(302, 87, 'slider', 'e3a0f8dc37e5f4ca8b959dd6c8ca3af5.jpg', '', ''),
(303, 87, 'slider', '07bd9e0794e39a5bd07417b374026d19.jpg', '', ''),
(304, 87, 'slider', '6c38f5d6161d6e785165bd6988fbb92e.jpg', '', ''),
(305, 87, 'slider', '70813dd4b253823101427dc9cfade019.jpg', '', ''),
(306, 87, 'slider', '46a340ef3e4683f1472c604b1aecbf67.jpg', '', ''),
(307, 90, 'slider', 'abddadfecde23eb4b1b53e883a992bcf.jpg', '', ''),
(308, 90, 'slider', '6d227ddd5c67e541747bbc76903329d4.jpg', '', ''),
(309, 83, 'gallery', '985b0615a06a7a73ef3d636d254228bf.jpg', '', ''),
(310, 83, 'gallery', 'af16d7f00c850f9547054689687477fe.jpg', '', ''),
(311, 83, 'gallery', '4988941f36189d9b7679b489b2052cea.jpg', '', ''),
(312, 83, 'gallery', '84e5b0eaf531933b83b7f713775ab3c5.jpg', '', ''),
(313, 91, 'slider', 'e67b3789824a11a3083d7c191259d74d.jpg', '', ''),
(314, 91, 'slider', '5b9994b6595abc880267ef755e168fe6.jpg', '', ''),
(315, 37, 'slider', '1729af72dfa5cf61f59c848520117c21.jpg', '', ''),
(316, 37, 'slider', 'b5e35513e66ba1879770b79643b366c4.jpg', '', ''),
(317, 37, 'slider', '39e48ef749045aefedcb61be2d6c7b17.jpg', '', ''),
(318, 37, 'slider', '19f9768a1e3ca9c05c10d4e765b2b690.jpg', '', ''),
(319, 37, 'slider', 'dd4c1dbec7f9b666ef46278cddcd5976.jpg', '', ''),
(320, 34, 'gallery', 'db39f00038f6d16f3fd49dc1f05a7d85.jpg', '', ''),
(321, 34, 'gallery', '26e243ef3776f31d2d23f2e8f95231d8.jpg', '', ''),
(322, 34, 'gallery', '2dea47498f5932ae051e1d4e42e837a2.jpg', '', ''),
(323, 34, 'gallery', 'dcdac6212ab65a95d7bef7304392cf02.jpg', '', ''),
(324, 34, 'gallery', '161e04f344fe2e5668ce173390b8baf5.jpg', '', ''),
(325, 34, 'gallery', 'e1469aed40246b30f9ced1bdacd215f0.jpg', '', ''),
(326, 34, 'gallery', 'c2f4a36a79be093c1dae5d36b1d676c8.jpg', '', ''),
(327, 34, 'gallery', '3362472cdb2bfbfedb6ec847365f877c.jpg', '', ''),
(328, 34, 'gallery', 'ad98087b013e2a8272abdb241dd8ed97.jpg', '', ''),
(329, 34, 'gallery', '96a73cbef8e5378064515fdf807ba629.jpg', '', ''),
(330, 34, 'gallery', '55a2f4f357cd75486a3f2122bc06a2e9.jpg', '', ''),
(331, 34, 'gallery', '2fc811911bbb2c6c2d54fcf0ad391bb6.jpg', '', ''),
(332, 34, 'gallery', 'fe53e15e65cd3dc58eb98b34feb947eb.jpg', '', ''),
(333, 34, 'gallery', 'e275e9639ea081be479af523ace22491.jpg', '', ''),
(334, 34, 'gallery', '4981ff268bb3e1e6a8898f8b8920cbc3.jpg', '', ''),
(335, 34, 'gallery', '278dd2383e92c3481706e1a7fd0cd471.jpg', '', ''),
(336, 34, 'gallery', 'e0cf93d4a8f15d338ceeccd16f7e5f3e.jpg', '', ''),
(337, 34, 'gallery', '19c3fc7341844150888b98390e290e96.jpg', '', ''),
(338, 34, 'gallery', '9d437ab5bdfc9f4696ec8a54afb613f9.jpg', '', ''),
(339, 34, 'gallery', 'e3bf8720947b9d7a4e0662c48112bd53.jpg', '', ''),
(340, 34, 'gallery', '896050e52c9438e3aa3decb448dfa6c9.jpg', '', ''),
(341, 34, 'gallery', '4e01ab33c3b0ceaccdbc82b068b8086b.jpg', '', ''),
(352, 27, 'slider', '35ba023ccf2ceaf5f800455cb3c73c22.jpg', '', ''),
(353, 27, 'slider', '9df7072c6be22bbf6a0655fe6a21347d.jpg', '', ''),
(354, 27, 'slider', 'f6d39db6086fc7cb2a7a28eacc8e1f0c.jpg', '', ''),
(355, 27, 'slider', '8755b7bd2222f24a0f2f83eb8b31b09a.jpg', '', ''),
(356, 27, 'slider', 'bc6ab49205e5a40485e54de350c649aa.jpg', '', ''),
(357, 27, 'slider', 'dc5550e2fb3a6a5890fa9d111ae23451.jpg', '', ''),
(358, 27, 'slider', '86d3108fdf867ee78269253c1b25f78d.jpg', '', ''),
(359, 27, 'slider', '935566dad1bf0a2a0d3e312503576d90.jpg', '', ''),
(360, 85, 'slider', '818e11909126ac041d4753940aa0a9e4.jpg', '', ''),
(361, 85, 'slider', 'a147e76697204de22db747c2276817fc.jpg', '', ''),
(362, 85, 'slider', '42b0b6e5c2e368458d70ac9eff5756a0.jpg', '', ''),
(363, 85, 'slider', 'cbb42728ce483b0cecdd2a24796be444.jpg', '', ''),
(364, 85, 'slider', '45316c6663172d4a4552c7df69cb0be4.jpg', '', ''),
(365, 38, 'slider', '54b40d3c93d1683c2d7020703f2b99e2.jpg', '', ''),
(366, 38, 'slider', '47d814a89539d20524e6ef8f191e502e.jpg', '', ''),
(367, 38, 'slider', '610844221356d9e5633f98f378c24380.jpg', '', ''),
(368, 102, 'slider', '8fef74a3f05a3c2b0f389e2fce4a5301.jpg', '', ''),
(369, 102, 'slider', 'e5c34cf6e0cc17c360ac99872002e52b.jpg', '', ''),
(370, 102, 'slider', '8352fc75a290628656ab4e8db87f95f6.jpg', '', ''),
(371, 102, 'slider', '5fd314934c1a5d336fe5de515381cd60.jpg', '', ''),
(372, 102, 'slider', '25137d8efe0313e043f70d6db08b9357.jpg', '', ''),
(373, 103, 'slider', '13326794fd4a23ab977bd3740a286206.jpg', '', ''),
(374, 103, 'slider', '87fd7e482faf5f8d9c04720b18c53ecc.jpg', '', ''),
(375, 103, 'slider', 'f31c838dbfcad73fbd40f163dcd96fc8.jpg', '', ''),
(376, 103, 'slider', 'd22fe5be23fd2dd61ddc399aa58a8975.jpg', '', ''),
(377, 103, 'gallery', '92084ed4690c6054582e64b79b874cd5.jpg', '', ''),
(378, 103, 'gallery', '0ffaf4cd244ed4a17bb4e349c583f421.jpg', '', ''),
(379, 103, 'gallery', '95d8f2dfdd5eead8a6cfea7d05a7a5e9.jpg', '', ''),
(380, 103, 'gallery', '1c46c5622a391f70093fe5a0627f8002.jpg', '', ''),
(381, 103, 'gallery', 'cf8ae0efad8a1e059a5ceaec1f2189da.jpg', '', ''),
(382, 103, 'gallery', '06a56030a659ef9cae670814fd4484ea.jpg', '', ''),
(383, 103, 'gallery', 'cdd828116aa0a795f08828c2e256dccc.jpg', '', ''),
(384, 103, 'gallery', 'dc121a3614a27d7aecab3dd65158f666.jpg', '', ''),
(385, 104, 'slider', '66ff8d556fd0fbe0caf3534d93da5612.jpg', '', ''),
(386, 104, 'slider', '4cd170eb48af372e0b496b0cf1759ab1.jpg', '', ''),
(387, 104, 'slider', '7355dc4bf64c2612dcb5ba101964f0ae.jpg', '', ''),
(388, 104, 'slider', '1ef35d48153ce8308ea298a15e79973b.jpg', '', ''),
(389, 104, 'gallery', '70e73530464ff56901f05d4b473e1887.jpg', '', ''),
(390, 104, 'gallery', '1daf588aa9a8435f16d19872dd3551bf.jpg', '', ''),
(391, 104, 'gallery', '070836cc36358a4b6ac5cf9832cf42cf.jpg', '', ''),
(392, 104, 'gallery', '0b9e19d905b4d102c5761e8bfd1ccb74.jpg', '', ''),
(393, 104, 'gallery', 'f4f80766a499b2b6a91dc7c46481fa28.jpg', '', ''),
(394, 104, 'gallery', '20c0e633aef9a18235af5a67bbe98cba.jpg', '', ''),
(395, 104, 'gallery', '8411e812cb199b917bbff3f8675592a5.jpg', '', ''),
(396, 104, 'gallery', '8a281d54e1c21d563d5262b2bec3b410.jpg', '', ''),
(397, 104, 'gallery', '179a80de771c03024362fde1d2a68335.jpg', '', ''),
(398, 104, 'gallery', '48b715aa97921490487f3159430f3dc0.jpg', '', ''),
(399, 104, 'gallery', '4a3a86635f02a9c36a322e13ca13b71d.jpg', '', ''),
(400, 104, 'gallery', '726bf77c705107d48fe131e5be95a3a5.jpg', '', ''),
(401, 105, 'slider', '6ba127c786b4e79a1fca3c57fc5bc2d4.jpg', '', ''),
(402, 105, 'slider', '5cbf627a952067a356802a5229260e4b.jpg', '', ''),
(403, 105, 'slider', '1cf5ed045aaf09b44d5dfe95950c394d.jpg', '', ''),
(404, 105, 'slider', '344d7d3f9f25dce99b1256555d48ee9d.jpg', '', ''),
(405, 105, 'gallery', '4019e2daedd38550a9b02548d9ac1684.jpg', '', ''),
(406, 105, 'gallery', 'ffcda228f6e8cd7647c7ea00f4f51829.jpg', '', ''),
(407, 105, 'gallery', 'edf873365407b4f3fd6192521b1cb3c5.jpg', '', ''),
(408, 105, 'gallery', '00089b79e1e29a8851e63c53e565f734.jpg', '', ''),
(409, 105, 'gallery', '5bc75dfd20d6ce45742222c37795cd1b.jpg', '', ''),
(410, 105, 'gallery', '2d9d915f121c492a1ff2259689cba38c.jpg', '', ''),
(411, 105, 'gallery', 'd5731ef46c4e32a08979889c2dde0438.jpg', '', ''),
(412, 105, 'gallery', '4f8a9be3414fab45ff66d07f27f9bc45.jpg', '', ''),
(413, 105, 'gallery', '98cac62ab57001b9b8e43812675905ac.jpg', '', ''),
(414, 105, 'gallery', '5fc2a900920f9a68a879e7e24c984aa4.jpg', '', ''),
(415, 105, 'gallery', '47910e520292eacd9908bfd13077a15b.jpg', '', ''),
(416, 107, 'slider', '558eb8e7007f1c067db1a1ca4d26a942.jpg', '', ''),
(417, 107, 'slider', '8387ae1fd9e7b398d41d8d8f62128a86.jpg', '', ''),
(418, 107, 'slider', 'a2f9dd69e6dbd85243e22097f2a80835.jpg', '', ''),
(419, 107, 'slider', '927d14e0e81b5e41c0e0b35e5f7f0e85.jpg', '', ''),
(420, 107, 'slider', '0156b1b2cb1ef9c7f70dc467fafb16e8.jpg', '', ''),
(421, 107, 'gallery', '3ceb7b8dd3c20e3bff52242523dad27d.jpg', '', ''),
(422, 107, 'gallery', '4626a3c7596086d4dc56cc67ea56f058.jpg', '', ''),
(423, 107, 'gallery', 'd1fef25d162a5fcbbd2db4d69236f2dc.jpg', '', ''),
(424, 107, 'gallery', '4c794640f0305e611f36dcbbbe30bd0d.jpg', '', ''),
(425, 108, 'slider', '14fb226c31e72954b6e22e19e9ec1eb8.jpg', '', ''),
(426, 108, 'slider', '03845caafcc546e0bbd88aa02102fdd3.jpg', '', ''),
(427, 108, 'slider', 'a49ec5ad96d7165833db49e4b6f559d8.jpg', '', ''),
(428, 108, 'slider', '2b08be754619d88b1076dbebfe377682.jpg', '', ''),
(433, 108, 'gallery', 'f64881dd67c662feaf118693008cdfaa.jpg', '', ''),
(434, 108, 'gallery', '6312b021dd726a1d041389f4f4f6f0ad.jpg', '', ''),
(435, 108, 'gallery', '64869947aecefd17e1220ed13c11edd6.jpg', '', ''),
(436, 108, 'gallery', '4c8254b7c9926788b6b1068ca2034c53.jpg', '', ''),
(437, 35, 'gallery', 'a88d6cb8c184b99284d749536048f36b.jpg', '', ''),
(438, 35, 'gallery', '7a7fa26bc49b6e787b2ac07319437b72.jpg', '', ''),
(439, 35, 'gallery', '3321eb48fcb2a9323c56ab8182ecaf9f.jpg', '', ''),
(440, 35, 'gallery', '8570b4e177c3680bc9b5a9a395ec65bd.jpg', '', ''),
(441, 35, 'gallery', 'fd75fb3e442fe92b37cd8dd0cdd6e00f.jpg', '', ''),
(442, 35, 'gallery', 'c9136d677e5e7e8309244496a80fd2a8.jpg', '', ''),
(443, 35, 'gallery', 'e61aafc19f060c541771a762242e5748.jpg', '', ''),
(444, 35, 'gallery', '5bcfbcd1de68530739da5e9694c8c518.jpg', '', ''),
(445, 35, 'gallery', 'd54636418ab3b1254117e620446f69cc.jpg', '', ''),
(446, 35, 'gallery', 'bec3f28c6407a01337ede3f16602baf9.jpg', '', ''),
(447, 35, 'gallery', 'b0774e9248a688e514d09e080c70e1bf.jpg', '', ''),
(448, 35, 'gallery', '798f3fb659bfe07dc73222843ef7c0d5.jpg', '', ''),
(449, 35, 'gallery', '1fd770a459576826334421fb9e83bacf.jpg', '', ''),
(450, 35, 'gallery', 'd353c3258677209659f3cd9019c2028b.jpg', '', ''),
(451, 35, 'gallery', '5f781cf98f431261899493073d7317a2.jpg', '', ''),
(452, 35, 'gallery', 'ed3508ce03a63cfb46dd0e38bb95fd55.jpg', '', ''),
(453, 35, 'gallery', 'fc52670505a548b0ae1f1fa4f12a7441.jpg', '', ''),
(454, 35, 'gallery', 'bbb13a4da28473565da6d33f41a52b06.jpg', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_home_property_data`
--

CREATE TABLE `tbl_home_property_data` (
  `id` int(11) NOT NULL,
  `prop_name` varchar(100) NOT NULL,
  `prop_description` text NOT NULL,
  `prop_image` varchar(100) NOT NULL,
  `prop_link` varchar(255) NOT NULL,
  `browser_window` varchar(10) DEFAULT '_self'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_home_property_data`
--

INSERT INTO `tbl_home_property_data` (`id`, `prop_name`, `prop_description`, `prop_image`, `prop_link`, `browser_window`) VALUES
(1, 'RANDHOLEE RESORT AND SPA', 'A<span style=\"color: red;\"> upscale </span>hotel built atop a misty and magical mountain is surrounded by a panoramic view of the villages below. Visit this resort for a lovely overview of the beautiful city of Kandy if you are planning a romantic vacation or a honeymoon getaway to Sri Lanka.', 'f7a0a352341c5db3922229f345bd8be2_1015533044.jpg', 'http://freudenbergleisure.lk/hotels/randholee/index.php', '_blank'),
(2, 'ELLEN\'S PLACE', 'Set against an urban landscape of concrete, steel and glass, Ellens’ Place stands out with a unique silhouette and a lush green garden. Experience an aura of living, breathing lushness like no other at, Ellens’, your tropical resort in the capital city of Colombo.', 'a6c4575c84adaf1e481dd07a5d855145_1525863818.jpg', 'http://freudenbergleisure.lk/hotels/ellens/index.php', '_blank'),
(3, 'THE FIRS', 'Elegant and cozy, this exquisite heritage bungalow that hearkens back to the British colonial era in Nuwara Eliya refreshingly different. A combination of old world charm and new world hospitality brings you the best in world class hospitality.', 'c360c827ff8b3e948c09fefaae836b98_2144979674.jpg', 'http://freudenbergleisure.lk/hotels/firs/index.php', '_blank');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(32) NOT NULL,
  `real_name` varchar(50) DEFAULT NULL,
  `company` varchar(50) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `access_level` tinyint(1) DEFAULT '1',
  `property_id` tinyint(1) DEFAULT '1',
  `description` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `real_name`, `company`, `last_login`, `status`, `access_level`, `property_id`, `description`) VALUES
(1, 'admin', '0f0a0d83fbeacb74d06ff36af36cbc87', 'Freudenberg Admin', '3CS', '2017-03-25 10:31:31', 1, 1, 1, NULL),
(2, 'randholee', '0f0a0d83fbeacb74d06ff36af36cbc87', 'Randholee Admin', '3CS', '2017-03-27 15:18:34', 1, 2, 2, 'Randholee Admin'),
(3, 'firs', '0f0a0d83fbeacb74d06ff36af36cbc87', 'Firs Admin', '3CS', '2017-03-27 15:18:34', 1, 3, 3, 'Firs Admin'),
(4, 'ellens', '0f0a0d83fbeacb74d06ff36af36cbc87', 'Ellens Admin', '3CS', '2017-03-27 15:18:34', 1, 4, 4, 'Ellens Admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbldescriptions`
--
ALTER TABLE `tbldescriptions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbldownloaditems`
--
ALTER TABLE `tbldownloaditems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblexperiences`
--
ALTER TABLE `tblexperiences`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblnavigation`
--
ALTER TABLE `tblnavigation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblpages`
--
ALTER TABLE `tblpages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblpromotionimages`
--
ALTER TABLE `tblpromotionimages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblpromotionpages`
--
ALTER TABLE `tblpromotionpages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblpromotions`
--
ALTER TABLE `tblpromotions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblproperties`
--
ALTER TABLE `tblproperties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblslides`
--
ALTER TABLE `tblslides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_home_property_data`
--
ALTER TABLE `tbl_home_property_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbldescriptions`
--
ALTER TABLE `tbldescriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `tbldownloaditems`
--
ALTER TABLE `tbldownloaditems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tblexperiences`
--
ALTER TABLE `tblexperiences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `tblnavigation`
--
ALTER TABLE `tblnavigation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `tblpages`
--
ALTER TABLE `tblpages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=117;

--
-- AUTO_INCREMENT for table `tblpromotionimages`
--
ALTER TABLE `tblpromotionimages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblpromotionpages`
--
ALTER TABLE `tblpromotionpages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblpromotions`
--
ALTER TABLE `tblpromotions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `tblproperties`
--
ALTER TABLE `tblproperties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tblslides`
--
ALTER TABLE `tblslides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=455;

--
-- AUTO_INCREMENT for table `tbl_home_property_data`
--
ALTER TABLE `tbl_home_property_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
