<?php
require_once '../appdata/cms/bootstrap.php';
//error_reporting(E_ALL);

	$propertydata = $db->getRow('SELECT property_slug FROM tblproperties WHERE `id` = ?', array((int)$_POST['downloaditem-property']));
	
	if($_SERVER['REQUEST_METHOD'] == 'POST')
	{
		// upload the image if submitted
		if(isset($_FILES['downloaditem-file']) && file_exists($_FILES['downloaditem-file']['tmp_name']))
		{
			// get type
			$type = end(explode('.', $_FILES['downloaditem-file']['name']));
			$filename = md5(time()) .'_'.mt_rand() .'.'.$type;

			$uploaddir = APP_ROOT . '/uploads/';
			$originalpath = APP_ROOT.'/public_html/files/'.$propertydata->property_slug.'/';

			if(!move_uploaded_file($_FILES['downloaditem-file']['tmp_name'], $uploaddir.$filename))
			{
				echo json_encode(array('status' => false, 'msg' => 'Failed uploading file! '.$filename));
				exit;
			}

			// move file
			rename($uploaddir.$filename, $originalpath.$filename);

		}


		if(isset($filename))
		{
			// a file has uploaded. include that also. for both new promotions and updating details with the image 
			$fields = '`property_id`,`item_title`,`item_file`';
			$placeholders = '?,?,?';
			$values = array((int)$_POST['downloaditem-property'], $_POST['downloaditem-title'], $filename);
		}
		else
		{
			// no file upload has taken place. only used when updating details
			$fields = '`property_id`,`item_title`';
			$placeholders = '?,?';
			$values = array((int)$_POST['downloaditem-property'], $_POST['downloaditem-title']);
		}

		if($_POST['downloaditem-action'] == 'add')
		{

			$addres = $db->addRecord('INSERT INTO tbldownloaditems ('.$fields.') VALUES('.$placeholders.')', $values);
			
			if(!$addres)
			{
				echo json_encode(array('action' => 'add', 'status' => false, 'msg' => 'Failed adding Download Item!'));
				exit;
			}

			echo json_encode(array('action' => 'add', 'status' => true, 'msg' => 'success', 'item_id' => $addres));
			exit;			
	
		}
		else
		{
			$updstr = '';

			foreach(explode(',', $fields) as $field)
			{
				$updstr .= $field .'=?,';
			}

			//$updstr = substr($updstr, strlen($updstr) - 1);
			$updqry = 'UPDATE tbldownloaditems SET '. (substr($updstr, 0, strlen($updstr) - 1)).' WHERE `id` = ?';
			array_push($values, $_POST['downloaditem-id']);

			$updres = $db->updateRecord($updqry, $values);

			
			if(!$updres)
			{
				echo json_encode(array('action' => 'update', 'status' => false, 'msg' => print_r($db->getError(), true), 'qry' => $updqry, 'vals' => $values));
				exit;
			}		

			//$msg = (($updres && $pgupdstatus) ? 'Promotion Details and Page Details Successfully Updated!' : (($updres && !$pgupdstatus) ? 'Promotion Details Updated! Page Details Update Failed!' : ((!$updres && $pgupdstatus) ? 'Promotion Details Update Failed! Page Details Updated!' : 'Promotion Details and Page Details Update Failed!')));

			//$status = (!$updres && !$pgupdstatus) ? false : true;

			echo json_encode(array('action' => 'update', 'status' => true, 'msg' => 'Download Item Updated!', 'posted_values' => $_POST));
			exit;
						
		}

	}
