<?php
require '../appdata/cms/bootstrap.php';

	// get navigation item list
	$navitemid = (int)$_GET['navitem'];

	$navdata = $db->getRow('SELECT tblnavigation.*, tblpages.property_id FROM tblnavigation LEFT JOIN tblpages ON tblnavigation.target_pg = tblpages.id WHERE tblnavigation.id = ?', array($navitemid));

	echo json_encode($navdata);