<div id="nt-example1-container">
    <aside>
        <div class="ctatext-buildadventure ctatext-wrapper" style="padding-top:20px !important; padding-bottom:20px !important;">
            <div class="ctatext-text">
                <div class="hdr-two" style="font-style: italic; margin-bottom: 15px;">Guest Comments</div>
                <ul id="nt-example1">
                    <li>“Relaxed, great rooms, awesome food and hospitable staff. Definitely recommended for your holiday in Colombo...”
                        <br><div class="hotel_name">-Ellen's Place</div></li>
                    <li>“We spent two nights at The Firs with our little daughter. The service was very friendly...”
                        <br><div class="hotel_name">-The Firs</div></li>
                    <li>“Hotel is located on top of a hill. Excellent view of Kandy...”
                        <br><div class="hotel_name">-Randholee Luxury Resort</div></li>
                    <li>“Tranquil setting. Breakfast outdoors, in the garden was a highlight. Staff are very attentive and cater...”
                        <br><div class="hotel_name">-The Firs</div></li>
                    <li>“Breathtaking views of the hills with the infinity pool made it a perfect stay for us....”
                        <br><div class="hotel_name">-Randholee Luxury Resort</div></li>
                    <li>“So after the visit to the tea plantations upon entering the city of Nuwara Eliya, you...”
                        <br><div class="hotel_name">-The Firs</div></li>
                    <li>“Reminded me of our holiday bungalows of good old days in Sri Lanka, where everything was catered to your personal needs...”
                        <br><div class="hotel_name">-Ellen's Place</div></li>
                </ul>
            </div>
        </div>
    </aside>
</div>
