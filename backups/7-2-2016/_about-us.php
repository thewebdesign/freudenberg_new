<style>
@media screen 
  and (device-width: 360px) 
  and (device-height: 640px) 
  and (-webkit-device-pixel-ratio: 3) 
  and (orientation: portrait) {

.booking_room {
    top: 11em !important;
    }

.ctatext-text {
    padding-top: 234px !important;
}
    form button, form input[type="button"], form input[type="reset"], form input[type="submit"], form input[type="search"] {
  
    margin-left: 36% !important;
}
}
@media only screen 
  and (min-device-width: 375px) 
  and (max-device-width: 667px) 
  and (-webkit-min-device-pixel-ratio: 2)
  and (orientation: portrait) { 
.booking_room {
    top: 12em !important;
    }

.ctatext-text {
    padding-top: 253px !important;
}
    form button, form input[type="button"], form input[type="reset"], form input[type="submit"], form input[type="search"] {
  
    margin-left: 42% !important;
}

}
@media only screen 
  and (min-device-width: 414px) 
  and (max-device-width: 736px) 
  and (-webkit-min-device-pixel-ratio: 3)
  and (orientation: portrait) { 
.booking_room {
    top: 13em !important;
    }

.ctatext-text {
    padding-top: 260px !important;
}
    form button, form input[type="button"], form input[type="reset"], form input[type="submit"], form input[type="search"] {
  
    margin-left: 49% !important;
}


}
@media only screen 
  and (min-device-width: 320px) 
  and (max-device-width: 340px)
  and (-webkit-min-device-pixel-ratio: 2)
  and (orientation: portrait) {
.booking_room {
    top: 10em !important;
    }

.ctatext-text {
    padding-top: 240px !important;
}
    form button, form input[type="button"], form input[type="reset"], form input[type="submit"], form input[type="search"] {
  
    margin-left: 27% !important;
}
}
@media only screen 
  and (min-device-width: 768px) 
  and (max-device-width: 1024px) 
  and (orientation: portrait) 
  and (-webkit-min-device-pixel-ratio: 1) {
.booking_room {
    top: 18em !important;
    }

.ctatext-text {
    padding-top: 253px !important;
}
    form button, form input[type="button"], form input[type="reset"], form input[type="submit"], form input[type="search"] {
  
    margin-left: 118% !important;
}

}
</style>﻿
﻿
<!DOCTYPE html>
<html class="no-js">
    <?php include 'includes/header.php'; ?> 

    <body >

        <header id="header" role="banner">
            <?php include 'includes/navigation.php'; ?> 

        </header><!--  #header  -->
        <?php include 'includes/booking.php'; ?> 


        <div class="blur">  
            <div class="node--page_basic mode--full">    
                <aside role="complementary">
                    <header>
                    </header>    
                    <?php include 'includes/slider.php'; ?>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>About Us</li>
                    </breadcrumb>
                </div>      

                <div id="main" role="main">
                    <div class="wrapper">
                        <article role="article">
                            <div class="ctatext-wrapper">
                                <div class="ctatext-text">
                                    <div class="hdr-two">About Us</div>     
                                    <p style="text-align:justify; font-size:16px;">Freudenberg Leisure, one of Sri Lanka’s top hospitality groups has grown significantly from its first hotel in Kandy to its current portfolio of three small upscale hotels in Colombo, Kandy and Nuwara Eliya. With service at the heart of our hospitality, we endeavour to create unique and memorable travel experiences, and to bring joy to our guests.

                                        <br><br>The Randholee Resort in Kandy, is a perfect honeymoon getaway and the ideal hotel to stay at for the Kandy Perehera. Firs is a 5 suite bungalow in Nuwara Eliya. It the former holiday bungalow of Sri Lanka’s first Prime Minister, Hon. D. S. Senanayake. Ellen’s Place, named after Ellen Senanayake, a leader in Sri Lanka’s independence struggle, is an upscale boutique hotel in Colombo, located in close proximity to the Royal Colombo Golf Club.

                                        <br><br>Randholee Resort in Kandy provides unique opportunities to view the Kandy Esala Perahera and  Sri Dalada Maligawa, the Temple of the Sacred Tooth Relic.</p>
                                </div><!--  .ctatext-text  -->
                            </div><!--  .ctatext-wrapper  -->
                        </article>
                    </div><!--  .wrapper  -->
                </div><!--  #main  -->

            </div><!--  #node-details  -->

            <div style="clear:both"></div>.

            <footer id="footer" role="contentinfo">    

                <?php /* ?> <aside role="complementary">
                  <div class="ctatext-buildadventure ctatext-wrapper">
                  <div class="ctatext-text">
                  <h1 class="hdr-seven">Build your own Adventure</h1>
                  <div class="hdr-two">Your Perfect Getaway Awaits</div>
                  <p>Whatever your lifestyle or pace, Freudenberg Leisure offers something unique for everyone.</p>
                  <a class="btn-arrow" href="#">Book Now</a>
                  </div><!--  .ctatext-text  -->
                  </div><!--  .ctatext-wrapper  -->
                  </aside><?php */ ?>


                <?php include 'includes/footer.php'; ?>

                </body>
                </html>