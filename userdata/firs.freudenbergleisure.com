--- 
customlog: 
  - 
    format: combined
    target: /etc/apache2/logs/domlogs/firs.freudenbergleisure.com
documentroot: /home2/fyfxamvcmvss/public_html/hotels/firs
group: fyfxamvcmvss
hascgi: 1
homedir: /home2/fyfxamvcmvss
ifmodulealiasmodule: 
  scriptalias: 
    - 
      path: /home2/fyfxamvcmvss/public_html/hotels/firs/cgi-bin/
      url: /cgi-bin/
ifmoduleconcurrentphpc: {}

ifmoduleincludemodule: 
  directoryhomefyfxamvcmvsspublichtmlhotelsfirs: 
    ssilegacyexprparser: 
      - 
        value: " On"
ifmodulelogconfigmodule: 
  ifmodulelogiomodule: {}

ifmodulemodincludec: 
  directoryhomefyfxamvcmvsspublichtmlhotelsfirs: 
    ssilegacyexprparser: 
      - 
        value: " On"
ifmodulemodsuphpc: 
  group: fyfxamvcmvss
ifmoduleuserdirmodule: 
  ifmodulempmitkc: 
    ifmoduleruidmodule: {}

ip: 128.199.220.102
ipv6: ~
is_addon: 1
no_cache_update: 1
owner: root
phpopenbasedirprotect: 1
port: 82
scriptalias: 
  - 
    path: /home/fyfxamvcmvss/public_html/hotels/firs/cgi-bin/
    url: /cgi-bin/
serveradmin: webmaster@firs.freudenbergleisure.com
serveralias: firs.lk mail.firs.lk mail.thefirsnuwaraeliya.com thefirsnuwaraeliya.com www.firs.freudenbergleisure.com www.firs.lk www.thefirsnuwaraeliya.com
servername: firs.freudenbergleisure.com
ssl: 1
usecanonicalname: 'Off'
user: fyfxamvcmvss
userdirprotect: ''
