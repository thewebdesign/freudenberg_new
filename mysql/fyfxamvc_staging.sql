-- MySQL dump 10.17  Distrib 10.3.14-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: fyfxamvc_staging
-- ------------------------------------------------------
-- Server version	10.3.14-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbldescriptions`
--

DROP TABLE IF EXISTS `tbldescriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbldescriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `sub_title` varchar(255) DEFAULT NULL,
  `main_title` varchar(255) DEFAULT NULL,
  `body_text` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbldescriptions`
--

LOCK TABLES `tbldescriptions` WRITE;
/*!40000 ALTER TABLE `tbldescriptions` DISABLE KEYS */;
INSERT INTO `tbldescriptions` (`id`, `page_id`, `sub_title`, `main_title`, `body_text`) VALUES (4,4,'','Experiences','<p style=\"text-align:justify; font-size:17px;\">Our three indulgence way outs pack a potent dose in terms of unique experiences won’t be anywhere else while enjoying the best hospitality and world class service. When you are staying in the dense, high-rise city of Colombo at Ellen’s Place you can visit Royal Colombo Golf club for a round of golf or enjoy panoramic views and wonderful photo opportunities, shopping and nightlife with a joyful mix of cultures and cuisines. One good way to experience the splendor of Kandy is to stay at the Randholee Resort, a great entry point to view Kandy Perehera, the Temple of the Tooth Relic, the Royal Botanical Gardens or the Hantane Mountain Range. Lastly, for a totally different experience to your urban adventure you must stay at Firs Bungalow in Nuwara Eliya to use the Victoria Golf Course, go horse riding, visit the Hakgala Botanical Gardens, hike on Horton Plains or take in the breathtaking views of World’s End.</p>'),(7,1,'FREUDENBERG LEISURE','Upscale Small Hotel Experiences','<p style=\"text-align:justify; font-size:17px;\">Freudenberg Leisure, with a chain of small upscale hotels in Colombo, the capital of Sri Lanka, Kandy, the UNESCO nominated world heritage city and Nuwara Eliya, the ‘city on the plain’ or ‘city of light’ offers one-of-a-kind experiences. Come to us with your wish list, our hotels will fulfill all your needs.</p><p style=\"text-align:justify; font-size:17px;\">The Randholee Resort and spa is an ideal choice for a fun family holiday, honeymoon getaways or business retreat for discerning modern travelers. Fascinating, unruffled and peaceful, the Firs Nuwara Eliya offers the travelers, serenity and indulgence for a relaxing holiday. Ellen’s Place, named after Ellen Senanayake, a leader in Sri Lanka’s independence struggle, is a upscale hotel in Colombo with unique and delightful facilities of a home away from home.</p>'),(8,6,'ROOMS, SUITES & PRIVATE VILLAS','Accommodation','<p style=\"text-align:justify; font-size:16px;\">We offer delightful accommodation facilities in Colombo, Kandy and Nuwara Eliya. Contact us to arrange a round-trip package  if you want to stay in one or more cities during  your visit to Sri Lanka. Enjoy the rooms and suites at Ellen’s Place in Colombo, at the Randholee Resort or suites at Firs.</p>'),(9,7,'','About Us','<p style=\"text-align:justify; font-size:16px;\">Freudenberg Leisure, one of Sri Lanka’s top hospitality groups has grown significantly from its first hotel in Kandy to its current portfolio of three small upscale hotels in Colombo, Kandy and Nuwara Eliya. With service at the heart of our hospitality, we endeavour to create unique and memorable travel experiences, and to bring joy to our guests.                                   <br><br>The Randholee Resort in Kandy, is a perfect honeymoon getaway and the ideal hotel to stay at for the Kandy Perehera. Firs is a 5 suite bungalow in Nuwara Eliya. It the former holiday bungalow of Sri Lanka’s first Prime Minister, Hon. D. S. Senanayake. Ellen’s Place, named after Ellen Senanayake, a leader in Sri Lanka’s independence struggle, is an upscale boutique hotel in Colombo, located in close proximity to the Royal Colombo Golf Club.                                      <br><br>Randholee Resort in Kandy provides unique opportunities to view the Kandy Esala Perahera and  Sri Dalada Maligawa, the Temple of the Sacred Tooth Relic.</p>');
/*!40000 ALTER TABLE `tbldescriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblexperiences`
--

DROP TABLE IF EXISTS `tblexperiences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblexperiences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tagline` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `destination_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblexperiences`
--

LOCK TABLES `tblexperiences` WRITE;
/*!40000 ALTER TABLE `tblexperiences` DISABLE KEYS */;
INSERT INTO `tblexperiences` (`id`, `tagline`, `description`, `image`, `destination_id`) VALUES (3,'The Galle Face Green','An urban park facing the glorious Indian Ocean','',1),(4,'Independence Arcade','The ideal place for shopping, fun and food in the heart of Colombo','75b409b5d4ca80a6f4f408985777327f_1005672626.jpg',1),(5,'The Gangaramaya Temple','Witness the Gangaramaya, a temple with unique architecture and beautiful surroundings in the heart of Colombo','6f6d088972c7f8923c07ed24277028a3_1493578512.jpg',1);
/*!40000 ALTER TABLE `tblexperiences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblnavigation`
--

DROP TABLE IF EXISTS `tblnavigation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblnavigation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `tagline` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `target_pg` int(11) DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblnavigation`
--

LOCK TABLES `tblnavigation` WRITE;
/*!40000 ALTER TABLE `tblnavigation` DISABLE KEYS */;
INSERT INTO `tblnavigation` (`id`, `page_id`, `title`, `tagline`, `description`, `target_pg`, `image`) VALUES (26,20,'Nw It11','uweur','989AAA Ami',1,'a09dbe68ab60f4120d7ae002d5e8a3dc_1423072598.jpg'),(27,20,'fdsa','fdsffds','fdsa42',1,'3e579558471c53e53ae2411f94b1e4df_553633886.gif'),(28,20,'fdsa','fdsffds','fdsa245452',1,'3e579558471c53e53ae2411f94b1e4df_553633886.gif'),(29,20,'Nw It','uweur','4353',1,'a2fd060e62fbfc3b4c526165fc31c1ff_217917146.jpg'),(30,20,'Nw It','uweur','989AAA',1,'446151f193822a1a7f4175fa82c88bda_1748846107.jpg'),(31,20,'fsf','fdsfdrret3453reresgd','fdsaf',1,'0b6e3e91a678902705574d3486926ea3_2074833753.png'),(32,1,'Item 1','Romance &amp; Relaxation','<em>Romance, Privacy and Opulence</em>',1,'056c3703e6488b7dd5fd335d2b44e7b1_1538788571.jpg'),(33,1,'item 2','Randholee Promotions','<em>Exciting Deals & Offers</em>',1,'3a8a3410a7719b1bbc9d6d32b5608f11_691499249.jpg'),(34,1,'item 3','Firs Promotions','<em>Great Savings and Promotions</em>',1,'330c6889ba7711a971332c68f3ac1e79_1897156157.jpg');
/*!40000 ALTER TABLE `tblnavigation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblpages`
--

DROP TABLE IF EXISTS `tblpages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblpages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_name` varchar(50) NOT NULL,
  `page_template` varchar(50) NOT NULL,
  `child_of` int(11) NOT NULL DEFAULT 0,
  `property_id` int(11) NOT NULL DEFAULT 0,
  `page_slug` varchar(25) NOT NULL,
  `section_desc` tinyint(1) DEFAULT 1,
  `section_nav` tinyint(1) DEFAULT 0,
  `section_slider` tinyint(1) DEFAULT 0,
  `section_gallery` tinyint(1) DEFAULT 0,
  `allow_subpages` tinyint(1) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblpages`
--

LOCK TABLES `tblpages` WRITE;
/*!40000 ALTER TABLE `tblpages` DISABLE KEYS */;
INSERT INTO `tblpages` (`id`, `page_name`, `page_template`, `child_of`, `property_id`, `page_slug`, `section_desc`, `section_nav`, `section_slider`, `section_gallery`, `allow_subpages`) VALUES (1,'Home','standard-page.tpl',0,1,'home',1,1,0,0,0),(4,'Experiences','standard-page.tpl',0,1,'exp',1,0,0,0,0),(6,'Accommodation','standard-page.tpl',0,1,'accommodation',1,0,1,0,0),(7,'About Us','standard-page.tpl',0,1,'aboutus',1,0,0,0,0),(8,'Terms and Conditions','standard-page.tpl',0,1,'tandc',1,0,0,0,0),(9,'Privacy Policy','standard-page.tpl',0,1,'pp',1,0,0,0,0),(10,'Downloads','standard-page.tpl',0,1,'downloads',1,0,0,0,0),(11,'About Us','standard-page.tpl',0,2,'aboutus',1,0,0,0,0),(12,'Cuisine','standard-page.tpl',0,2,'cuisine',1,0,0,0,0),(13,'Accommodation','standard-page.tpl',0,2,'accommodation',1,0,0,0,0),(14,'Home','standard-page.tpl',0,2,'home',1,0,0,0,0),(15,'Facilities','standard-page.tpl',0,2,'facilities',1,0,0,0,0),(16,'Honeymoon','standard-page.tpl',0,2,'honeymoon',1,0,0,0,0),(18,'Sub Pg','standard-page.tpl',10,1,'subpg',1,0,0,0,0),(19,'Sub PG 2','standard-page.tpl',10,1,'sub2',1,0,0,0,0),(20,'Sub Acc','standard-page.tpl',6,1,'subacc',1,0,0,0,0),(21,'New Page','standard-page.tpl',0,1,'newpage',1,0,0,0,0),(22,'Asub','standard-page.tpl',13,2,'asub',1,0,0,0,0),(23,'Home','standard-page.tpl',0,4,'home',1,0,0,0,0);
/*!40000 ALTER TABLE `tblpages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblproperties`
--

DROP TABLE IF EXISTS `tblproperties`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblproperties` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `property_name` varchar(100) NOT NULL,
  `property_desc` text NOT NULL,
  `property_image` varchar(100) DEFAULT NULL,
  `property_tpl` varchar(50) NOT NULL,
  `property_slug` varchar(50) NOT NULL,
  `property_address` varchar(100) DEFAULT NULL,
  `property_telephone` varchar(50) DEFAULT NULL,
  `property_fax` varchar(50) DEFAULT NULL,
  `property_email` varchar(50) DEFAULT NULL,
  `property_logo` varchar(50) DEFAULT NULL,
  `property_map_longitude` varchar(100) DEFAULT NULL,
  `property_map_latitude` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblproperties`
--

LOCK TABLES `tblproperties` WRITE;
/*!40000 ALTER TABLE `tblproperties` DISABLE KEYS */;
INSERT INTO `tblproperties` (`id`, `property_name`, `property_desc`, `property_image`, `property_tpl`, `property_slug`, `property_address`, `property_telephone`, `property_fax`, `property_email`, `property_logo`, `property_map_longitude`, `property_map_latitude`) VALUES (1,'Freudenberg Leisure','desc',NULL,'freudenberg-dashboard.tpl','freudenberg',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'Randholee Resorts & Spa','desc',NULL,'property-dashboard.tpl','randholee',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'The Firs','desc',NULL,'property-dashboard.tpl','firs',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(4,'Ellens Place','desc',NULL,'property-dashboard.tpl','ellens',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tblproperties` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) NOT NULL,
  `password` varchar(32) NOT NULL,
  `real_name` varchar(50) DEFAULT NULL,
  `company` varchar(50) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT 0,
  `access_level` tinyint(1) DEFAULT 1,
  `property_id` tinyint(1) DEFAULT 1,
  `description` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `real_name`, `company`, `last_login`, `status`, `access_level`, `property_id`, `description`) VALUES (1,'vikum','21232f297a57a5a743894a0e4a801fc3','Vikum','3CS','2017-03-25 10:31:31',1,1,1,'Freudenberg Admin'),(2,'vikum-r','21232f297a57a5a743894a0e4a801fc3','Vikum (Randholee)','3CS','2017-03-27 15:18:34',1,2,2,'Randholee Admin'),(3,'vikum-f','21232f297a57a5a743894a0e4a801fc3','Vikum (Firs)','3CS','2017-03-27 15:18:34',1,2,3,'Firs Admin'),(4,'vikum-e','21232f297a57a5a743894a0e4a801fc3','Vikum (Ellens)','3CS','2017-03-27 15:18:34',1,2,4,'Ellens Admin');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'fyfxamvc_staging'
--

--
-- Dumping routines for database 'fyfxamvc_staging'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-16 17:00:18
