﻿<?php 
$page = 'accommodation';
include 'includes/header.php'; 
?> 

    <body>
        <header id="header" role="banner">   
            <?php include 'includes/navigation.php'; ?> 
        </header><!--  #header  -->

        <?php include 'includes/booking.php' ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">
                    <?php include 'includes/_slider.php' ?>
                </aside>  
                
                    <div id="route">
                        <breadcrumb class="menu">
                            <li><a href="index.php">Home</a></li>
                            <li><span class="arrow"> &gt; </span>Accommodation</li>
                        </breadcrumb>
                    </div>      
 

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper"> 
                            <div class="ctatext-text pad_top">
                                <h1 class="hdr-seven">Rooms, Suites &amp; Private Villas</h1>          
                                <div class="hdr-two">Accommodation</div>          
                                <p style="text-align:justify; font-size:16px;">We offer delightful accommodation facilities in Colombo, Kandy and Nuwara Eliya. Contact us to arrange a round-trip package  if you want to stay in one or more cities during  your visit to Sri Lanka. Enjoy the rooms and suites at Ellen’s Place in Colombo, at the Randholee Resort or suites at Firs.</p>          
                                <!--<a href="#" class="btn-underline">Book Your Getaway</a>-->     
                            </div><!--  .ctatext-text  --> 
                        </div><!--  .ctatext-wrapper  -->



                        <div class="highlight-panels">
                            <ul>
                                <li class="highlight" style="background: #ebebeb url('assets/images/randholee/acc_front_1.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="hotels/randholee/accommodation.php"></a>   
                                    <div class="highlight-background"></div><!--  .highlight-content-inside  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Randholee Resort and Spa</h2>     
                                            <div class="hdr-two fadeitem">Warm and Welcoming</div>      
                                            <p class="fadeitem">The Kandyan paintings and the rich mahogany furniture add to this enchanting atmosphere.</p>            
                                            <a href="" class="btn-arrow fadeitem">View All</a>      
                                        </div><!--  .highlight-content-inside  -->      
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->  


                                <li class="highlight" style="background: #ebebeb url('assets/images/firs/2.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="hotels/firs/accommodation.php"></a>   
                                    <div class="highlight-background"></div><!--  .highlight-content-inside  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">The Firs</h2>      
                                            <div class="hdr-two fadeitem">Old World Charm and Elegance</div>      
                                            <p class="fadeitem">An air of luxury that create an atmosphere of ultimate serenity and respite</p>            
                                            <a href="#" class="btn-arrow fadeitem">View All</a>      
                                        </div><!--  .highlight-content-inside  -->      
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->  


                                <li class="highlight" style="background: #ebebeb url('assets/images/ellens/IMG_1080.jpg') no-repeat 50% 50%; background-size: cover;">
                                    <a class="highlight-hotspot" href="hotels/ellens/accommodation.php"></a>   
                                    <div class="highlight-background"></div><!--  .highlight-content-inside  -->
                                    <div class="highlight-content">
                                        <div class="highlight-content-inside">
                                            <h2 class="hdr-four">Ellen's Place</h2>      
                                            <div class="hdr-two fadeitem">An Ample Living Space</div>      
                                            <p class="fadeitem">Offers guests luxurious accommodations perfected by a myriad of amenities and blissful comforts</p>            
                                            <a href="#" class="btn-arrow fadeitem">View All</a>      
                                        </div><!--  .highlight-content-inside  -->      
                                    </div><!--  .highlight-content  -->
                                </li><!--  .highlight  -->        </ul>
                        </div><!--  .highligh-panels  -->

                        <!--<aside role="complementary">
                                  <div class="ctatext-wrapper">
                            <div class="ctatext-text">            
                                          <h1 class="hdr-seven">Seasonal Room, Suite &amp; Villa Rates</h1>                   
                                          <div class="hdr-two">Your Perfect Getaway Awaits</div>                       
                                            <p style="text-align:justify; font-size:16px;">Guests may also take advantage of a number of in-room amenities and services including complimentary Wi-Fi internet access, personalised butler service, 24 hour in-room dining, as well as access to CD/DVD libraries.</p>                    
                                          <a href="#" class="btn-arrow">Book Now</a>                      
                                          </div>
                          </div>        
                                </aside> -->              
                    </article>           
                </main>   
            </div><!--  #node-details  -->

            <div style="clear:both"></div>.
            <?php include 'trip-advisor.php'; ?>
            <footer id="footer" role="contentinfo">  

                <?php include 'includes/footer.php'; ?> 

                </body>
                </html>
