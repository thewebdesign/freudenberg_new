﻿<?php 
$page = 'promotions';
include 'includes/header.php'; 
?>
    <style>
        .ctacard-wrapper {clear: both;overflow: hidden;padding-top: 0px !important;padding-bottom: 0px !important;}
        .ctacard-wrapper .ctacard-card:nth-child(2) {margin-left: 0% !important; margin-right: 0% !important; }
        .ctacard-wrapper .ctacard-card:nth-child(3) {margin-left: 5%; margin-right: 5% }
        #route{margin-left:21.4%}	


         .hdr-seven-ect {
    text-align: left;
    font-size: 15px;
    color: #fff;
    text-transform: uppercase;
    background-color: #0077c1;
    padding: 5px;
}
@media (min-width: 600px)
.ctatext-wrapper {
    padding-top: 20px;
    padding-bottom: 60px;
}
.ctatext-wrapper {
    clear: both;
    padding-top: 60px;
    padding-bottom: 35px;
}
@media (min-width: 900px)
.node--page_basic article, .node--blog article, .node--activities article, .node--package article, .node--treatment article, .node-type-webform article, .page-error-404 article, .page-error-403 article {
     width: 100% !important; 
    margin-left: 0px !important; 
  padding-top: 0px !important; 
    padding-bottom: 10px !important;
}
@media (min-width: 1200px)
.node--page_basic article, .node--blog article, .node--activities article, .node--package article, .node--treatment article, .node-type-webform article, .page-error-404 article, .page-error-403 article {
    width: 100% !important;
    margin-left: 0px !important;
    margin-right:0px !important;
}
article, aside, details, figcaption, figure, footer, header, hgroup, menu, nav, section, summary {
    display: block;
}
.ctatext-text li {
    
    font-size: 14px;
    text-decoration: none;
    text-align: left;
    margin-left: auto;
        padding-bottom: 6px;
    }
    .priv-poly {
    width: 100%;
    height: auto;
    display: block;
    margin-bottom: 20px;
    padding: 0;
    list-style: disc;
}
mark {
    background-color: rgba(110, 237, 243, 0.97);
    color: black;
}

        #fade{
            display: none;
            position: fixed;
            top: 0%;
            left: 0%;
            width: 100%;
            height: 100%;
            background-color: #000;
            z-index:+111111;

            opacity:.80;
            filter: alpha(opacity=70);
        }
        #light{

            display: none;
            margin:0 auto;
            width: 50%;



        }

        .promo{
            position:fixed;

            z-index:+11111111;
            top: 75px;
            overflow:visible;
            -moz-animation: fadein 2s; /* Firefox */
            -webkit-animation: fadein 2s; /* Safari and Chrome */
            -o-animation: fadein 2s; /* Opera */
        }
        @keyframes fadein {
            from {
                opacity:0;
            }
            to {
                opacity:1;
            }
        }
        @-moz-keyframes fadein { /* Firefox */
            from {
                opacity:0;
            }
            to {
                opacity:1;
            }
        }
        @-webkit-keyframes fadein { /* Safari and Chrome */
            from {
                opacity:0;
            }
            to {
                opacity:1;
            }
        }
        @-o-keyframes fadein { /* Opera */
            from {
                opacity:0;
            }
            to {
                opacity: 1;
            }
        }


        .clo{
            position:fixed;
            z-index:1003;
            margin-top: 4%;
            margin-left: 33%;
            width: initial;

        }



    </style>

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">

            <h1 class="hide-visual">Freudenberg Leisure</h1>  
            <?php include 'includes/navigation.php'; ?> 

        </header><!--  #header  -->

        <?php include 'includes/booking.php'; ?> 

        <div class="blur">  

            <div class="node--page_basic mode--full">  
                <aside role="complementary">
                    <?php include 'includes/slider.php'; ?>
                </aside>    

                <!--<div id="route" style="margin-left: 21.4% !important;">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Special Offers</li>
                    </breadcrumb>
                </div>-->

                <main id="main" role="main">

                    <article role="article" style="background-image:url(assets/img/promo-bg.png); width: 100%;padding-left: 0px;margin-left: 0;">

                        <div class="ctatext-wrapper">

                            <div class="ctatext-text pad_top">         

                                <h1 class="hdr-seven" style="text-align:center; font-size:17px; padding:10px; font-weight:100;">LONG STAY OFFER</h1>

                                <div class="hdr-two">6 Days &amp; 5 Nights In Sri Lanka For £650 (Twin Sharing)</div>          

                                <!--<h1 class="hdr-seven" style="text-align:left; font-size:22px; padding:10px; font-weight:100;"><em>Deluxe Mountain View & Deluxe Rooms</em></h1>
                                <p style="text-align:justify; font-size:14px;">Book four consecutive nights at Randholee. Receive our best available rate and your fourth night free!</p>-->
                                <h1 class="hdr-seven hdr-seven-ect">Day 01</h1>
                                <ul class="priv-poly">
                                   <li> Arrival at Colombo International Airport, after clearing the immigration we shall meet by your representative and transfer to Colombo and check in at Ellen’s place.
                                     Site scene in Colombo in the after noon </li>
                                </ul>
                                
                                <h1 class="hdr-seven hdr-seven-ect">Day 02 & 03</h1>
                                <ul class="priv-poly">
                                    <li>After breakfast check out from the hotel and proceed for sightseeing tour of Kandy (a UNESCO World Heritage Site). The Kandyan kingdom, the last bastion of the Sri Lankan political power against the colonial forces, is the home for the Temple of the Sacred Tooth Relic and the site of one of the world’s most impressive annual festivals – the Esala Perahera. The Temple contains the most sacred relic of the Buddhists today and the most precious of Sinhalese pride. The upper storey houses the sacred relic and a library containing 800 year old palm leaf religious manuscripts. The Pooja (ceremonies of veneration) are held at dawn and dusk daily and these are moments when the Temple comes to life with pilgrims making offerings of flowers amidst clouds of incense and the beating of traditional ritual drums.  The city is visually rich with its narrow streets lined with characterful old buildings. The market has a good display of traditional arts and craft, textiles and gems & jewellery. The lake built by the last Sinhala King Sri Wickrama Rajasinghe in 1798 is still a focal attraction.</li>
                                    <li>
                                    Proceed to Pussellawa. Visit the Anjaneear Temple in Pussellawa housing a 16 feet image of Lord Hanuman. The Sri Baktha Hanuman Temple was built with Hanuman as the presiding deity on this hill of 
                                    Ramboda, where Hanuman was believed to be searching for Sita Devi.</li>
                                    <li>
                                    Proceed to Nuwara Eliya on route witnessing the Ramboda waterfalls and visiting a tea plantation and a tea factory. There will be the opportunity to observe all about the process of manufacturing and grading tea and taste a cup of fresh Ceylon Tea in the factory itself. Check in at the hotel and rest of the day free at leisure. Overnight stay at the hotel in Nuwara Eliya.</li>

                                    
                                </ul>
                                
                                <!--<p style="text-align:justify; font-size:14px;">Room rates starting from USD 140/- Per room per night</p>-->

                                <h1 class="hdr-seven hdr-seven-ect">Day 4  Nuwara Eliya </h1>
                                <ul class="priv-poly">
                                    <li>After Breakfast check out from the hotel and proceed for the tour of Sita Amman Temple. According to legend, the temple is believed to mark the spot where Sita was held captive by King Rawana. Ancient statues of Rama and Sita can be seen with paintings, pillars decorated with sculptures, all depicting the tale of Rama and Sita.  The rear of the temple overlooks a stream, while from its front Hakgala peaks can be viewed.</li>
                                    <li>Thereafter, visit the Hakgala Botanic Gardens. This Botanic Garden was one of the pleasure gardens of King Ravana of the epic Ramayanaya.  According to the epic, this was here where the heart-warming meeting took place between Sita and Hanuman, who brought her Rama’s ring.</li>
                                    <li>
                                    Thereafter visit Isthripura. This exotic cave still boasts the picturesque cascading waterfall that was in existence 2000 years ago. It was within this cave that Sita was held with over 1500
                                     women to attend to her every need.</li>
                                     <li>
                                     Thereafter proceed to Welimada and visit the Divurumpola Temple. It is believed that this Temple has been since the time of Rama, Rawana. Still revered by thousands, this was believed to be the place where Sita performed her Agni Pariksha (proving her chastity) to her husband Rama.  Since that time this place has been respected and worshiped by people. It is said that this place was given the name ‘Divurumpola’ (the place of making a wow) not only in legends, but it has been inscribed in the moonstone guarding the Devale in the premises.</li>

                                </ul>
                                
                                <h1 class="hdr-seven hdr-seven-ect">Day 05 & 06</h1>
                                <ul class="priv-poly">
                                    <li>Heading back from Nuwara eliya to Colombo rest at Ellen’s place departure after breakfast on the day 06 and transfer to airport. </li>
                                      &nbsp;
                                      <li style="list-style-type: square;font-size:18px;">For more information</li>
                                      
                                      <li class="fa fa-envelope-o" style="font-size:20px;">&emsp;mgr.sales@freudenbergleisure.com  (Maheel)</li>
                                      <li class="fa fa-phone" style="font-size:20px;">&emsp;+94 777 385 064</li>
                                </ul>

                              
                                
                                
                                
                                <!--<h1 class="hdr-seven hdr-seven-ect">CONDITIONS</h1>
                                <ul class="priv-poly">
                                    <li>Rates indicated above include a 10% service charge, & other government taxes.</li>
                                    <li>Rates above are indicative rates per room per night and are subject to change without notice.</li>
                                    <li>This offer is on a non-refundable cancellation policy. If canceled, modified or in case of no-show, the    total price of the reservation will be charged.</li>
                                    <li>Total amount of the reservation will be charged at the time of the reservation.</li>
                                    <li>Bookable period is from  21st October 2015 to 29th February 2016 with the stay period falling 30 days forward, until  29th February 2016.</li>
                                    <li>The travel/stay period for this offer will be 6th January 2015 to 31st August 2015.</li>
                                    <li>This promotion cannot be combined with any other promotion or discount and is subject to availability.</li>
                                </ul>-->


                            </div><!--  .ctatext-text  -->

                        </div><!--  .ctatext-wrapper  -->                          

                    </article>            

                </main>



            </div>  

            <footer id="footer" role="contentinfo"> 

                <?php include 'trip-advisor.php'; ?>
                <?php /* ?>    <aside role="complementary">
                  <div class="ctatext-buildadventure ctatext-wrapper">
                  <div class="ctatext-text">
                  <h1 class="hdr-seven">Build your own Adventure</h1>
                  <div class="hdr-two">Your Perfect Getaway Awaits</div>
                  <p>Whatever your lifestyle or pace, Freudenberg Leisure offers something unique for everyone.</p>
                  <a class="btn-arrow" href="#">Book Now</a>
                  </div><!--  .ctatext-text  -->
                  </div><!--  .ctatext-wrapper  -->
                  </aside><?php */ ?>

                <?php include 'includes/footer.php'; ?>

                <script>


                    function lightbox_open() {
                        window.scrollBy(0, 0);
                        document.getElementById('light').style.display = 'block';
                        document.getElementById('fade').style.display = 'block';

                    }

                    function lightbox_close() {
                        document.getElementById('light').style.display = 'none';

                        document.getElementById('fade').style.display = 'none';
                    }


                </script>

                </body>

                </html>