﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_randholee.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_randholee.php'; ?> 
        </header><!--  #header  -->

        <?php // include '../../includes/booking_randholee.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">

                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/delux-non-view/delux_nv.jpg') no-repeat 50% 50%; background-size: cover;"></div>    
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/delux-non-view/delux_nv1.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/delux-non-view/delux_nv2.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    </div>
                    
                    <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span><a href="accommodation.php">Accommodation</a></li>
                        <li><span class="arrow"> &gt; </span>Deluxe</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">         
                                <div class="hdr-two">Deluxe</div>          
                                <p style="text-align:justify; font-size:18px;">Step into an extraordinary world fashioned for uninterrupted peace. Nestled in the heart of Mount Pleasant and surrounded by its beauty. The earthy tones of the rooms, the Kandyan paintings and the rich mahogany furniture add to this enchanting atmosphere. The palatial beds are fitted with soft cotton sheets that are cool and crisp to the touch. Our spacious bathrooms include a large bathtub and a commodious counter for your comfort.</p><br>

                                <h1 class="hdr-seven" style="text-align:left; font-size:13px;"> ROOM INCLUDES:</h1>
                                <ul class="accom-list">
                                    <li>Air-conditioning</li>
                                    <li>Tea and coffee making facilities</li>
                                    <li>In-room dining</li>
                                    <li>Colour TV with cable</li>
                                    <li>DVD Player (on request)</li>
                                    <li>In-room safe</li>
                                    <li>Writing tables and chairs</li>
                                    <li>Bathroom with tub and shower</li>
                                    <li>Hot and cold water</li>
                                </ul>
                                <ul class="accom-list">
                                    <li>Wireless Internet Access/ ADSL</li>
                                    <li>Laundry service</li>
                                    <li>Baby cot available (on request)</li>
                                    <li>Wardrobe with hangers</li>
                                    <li>Iron and board available (on request)</li>
                                    <li>Hair-dryer available (on request)</li>
                                </ul>        
                                <div style="clear:both"></div>
                                <?php include 'inner_slider.php'; ?> 
                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->

            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_randolee.php'; ?> 
            </footer>    
    </body>
</html>
