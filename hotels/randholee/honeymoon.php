<!DOCTYPE html>
<html class="no-js">
<?php include '../../includes/header_randholee.php';?> 
<style>
.ctatext-text li {
    list-style:disc;
    font-size: 14px;
    text-decoration: none;
    text-align: left;
    margin-left: auto;
    width: 80%;
    line-height: 2em;
}
</style>
<body class="node-type-accommodation-list">
<header id="header" role="banner">
  <?php include '../../includes/navigation_randholee.php';?> 
  </header><!--  #header  -->
  
<?php include '../../includes/booking_randholee.php';?> 

<div class="blur">  
  
  <div class="node--accommodation_list mode--full">
    <aside role="complementary">
  
    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
      <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/wed_slider_1.jpg') no-repeat 50% 50%; background-size: cover;"></div>    
      <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/wed_slider_2.jpg') no-repeat 50% 50%; background-size: cover;"></div>
    </div>
    
    <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>
  </aside>
  
   
  <main id="main" role="main" style="background:url(assets/img/honey-bg.png);height:668px;">
  <div id="route">
        <breadcrumb class="menu">
            <li><a href="index.php">Home</a></li>
            <li><span class="arrow"> &gt; </span>Honeymoon</li>
        </breadcrumb>
    </div>
    <article role="article">
      <div class="ctatext-wrapper honey-bg">
        <div class="ctatext-text">
          <h1 class="hdr-seven" style="color: #C74E4D !important;">Honeymoon of a lifetime</h1>       
          <div class="hdr-two">Honeymoon</div>          
          <p style="text-align:justify; font-size:18px;">The excellent mix of adventure and relaxation at  Randholee Luxury Resort makes the perfect honeymoon combination. Begin your honeymoon in the hill country city of Kandy; here you can discover the temple of sacred tooth relic, remote villages and lush jungle clad mountains. This scenic honeymoon’s a classic! Randholee is a wonderful place to start your vacation with your beloved, and the best place in the paradise island that should be on everyone’s honeymoon wish list!</p><br/>
<div class="honey-heart"><img src="assets/img/hearts-together.png"></div>
<!--<a href="#" class="btn-underline">Book Your Getaway</a>-->
<h1 class="hdr-seven" style="text-align:left; font-size:13px; color: #C74E4D !important;">Package includes:</h1>
  <ul class="honey-list">
    <li>Exotic welcome cocktail/mocktail on arrival.</li>
    <!-- <li>Champagne Breakfast for the couple.</li> -->
    <li>Framed honeymoon photograph or a romantic souvenir will be given as memorabilia prior to departure from the hotel.</li>
    <li>A private candlelit four-course dinner will be served by the pool (with an option of ordering dinner from the special ‘Honeymooner’s ala carte'</li>
    <li>A one-year anniversary dinner and special concessions.</li>
    <li>A bottle of wine will be served during dinner.</li>
    <li>Decorated room with scented candles & Chocolate coated strawberries.</li>
    <li>Liqueur scented evening chocolates.</li>
  </ul>  
            
          </div><!--  .ctatext-text  -->
      </div><!--  .ctatext-wrapper  -->                          
    </article>      
          
  </main>   
</div><!--  #node-details  -->
  
    <div style="clear:both"></div>
  <footer id="footer" role="contentinfo">  
  <?php include '../../includes/footer_randolee.php';?> 
   </footer>    
</body>
</html>
