﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_randholee.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_randholee.php'; ?> 
        </header><!--  #header  -->

        <?php // include '../../includes/booking_randholee.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">

                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/fitness/fit4.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/fitness/fit1.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/fitness/fit3.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item lazyOwl" style="background: #ebebeb url('assets/images/sliders/facili/fitness/fit2.jpg') no-repeat 50% 50%; background-size: cover;"></div>

                    </div>
                    
                    <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a> 
                </aside>  
                
                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span><a href="facilities.php">Facilities</a></li>
                        <li><span class="arrow"> &gt; </span>Fitness Centre</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">         
                                <div class="hdr-two">Fitness Centre</div>          
                                <p style="text-align:justify; font-size:16px;">Equipped with weight lifting machines, treadmills and steppers, Randholee Luxury Resort offers you a state-of-the-art gym to get a fabulous workout during your holidays.</p>  
                                <?php include 'inner_slider.php'; ?> 

                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->

            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_randolee.php'; ?> 
            </footer>    
    </body>
</html>
