﻿<?php 
$pg = ['property' => 'randholee', 'page' => 'home'];
include '../../includes/header_randholee.php'; 
?>
    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <h1 class="hide-visual">Randholee Luxury Resort - Randholee Luxury Resort Home Page</h1>  
            <?php include '../../includes/navigation_randholee.php'; ?> 			
        </header><!--  #header  -->

        <?php include '../../includes/booking_randholee.php'; ?> 

        <div class="blur">  

            <div class="node--page_basic mode--full">  
                <aside role="complementary">
                    <header>
                        <h1 class="hide-visual">Randholee Luxury Resort Slideshow</h1>
                    </header> 
                    <?php include '../../includes/slider_randholee.php'; ?>
                </aside>     

                <aside role="complementary">
                    <header>
                        <h1 class="hide-visual">Randholee Luxury Resort Experiences</h1>
                    </header>

                    <div id="main" role="main">
				
                    <div class="trip-advisor-logo" id="demo">
						<img class="trip-ad-close" src="assets/img/cls.png" onclick="myFunction()"  >
					<a href="https://www.tripadvisor.co.uk/Hotel_Review-g304138-d1594031-Reviews-Randholee_Luxury_Resort-Kandy_Central_Province.html" target="_blank"><img class="trip-ad" src="assets/images/trip-advisor-randholee.png"></a>
					
					</div>

                        <div class="wrapper">
                            <article role="article">
                                <div class="ctatext-wrapper">
                                    <div class="ctatext-text">
                                        <?php require '../../includes/showdescription.php'; ?>
                                    </div><!--  .ctatext-text  -->   
                                </div><!--  .ctatext-wrapper  -->
                            </article>
                        </div><!--  .wrapper  -->
                    </div><!--  #main  -->

                    <div class="experience-thumblist highlight-panels">
                        <ul>  
                            <div class="experience-thumblist highlight-panels">
                                <?php 
                                $mnusec = '';
                                require '../../includes/shownavigation-5.php'; 
                                ?>
                            </div><!--  .experience-thumblist .highlight-panels  --> 
                        </ul>
                    </div><!--  .experience-thumblist .highlight-panels  -->
                </aside>    

            </div><!--  #node-details  -->

            <footer id="footer" role="contentinfo"> 

                <?php include 'trip-advisor.php'; ?>
                <?php /* ?>                <aside role="complementary">
                  <div class="ctatext-buildadventure ctatext-wrapper">
                  <div class="ctatext-text">
                  <h1 class="hdr-seven">Build your own Adventure</h1>
                  <div class="hdr-two">Your Perfect Getaway Awaits</div>
                  <p>Whatever your lifestyle or pace, Freudenberg Leisure offers something unique for everyone.</p>
                  <a class="btn-arrow" href="#">Book Now</a>
                  </div><!--  .ctatext-text  -->
                  </div><!--  .ctatext-wrapper  -->
                  </aside><?php */ ?>

                <div style="clear:both"></div>

                <?php include '../../includes/footer_randolee.php'; ?>


				
				
		<script type="text/javascript">
function myFunction() {
    document.getElementById("demo").style.display = "none";
}
</script>

                </body>
				
				

                </html>