﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_randholee.php'; ?> 

    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_randholee.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_randholee.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">

                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/suites/suite_nv.jpg') no-repeat 50% 50%; background-size: cover;"></div>    
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/suites/suite_nv1.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/suites/suite_nv2.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                    </div>
                    
                    <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span><a href="accommodation.php">Accommodation</a></li>
                        <li><span class="arrow"> &gt; </span>Suite</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">         
                                <div class="hdr-two">Suite</div>          
                                <p style="text-align:justify; font-size:18px;">Relax in the contemporary comforts of your new rooms coloured with black, pink and red wall pars, a bright and airy haven to uplift your body and spirit with panoramic view of mountains. Luxuriate in the deep-soaking bathtub, and relax with an array of convenient amenities including free WiFI and 24-hour room service. Enjoy a fantastic slice of Sri Lanka’s heritage city of Kandy –skyline.<br><br>
Carefully designed with modern facilities this sanctuary is the perfect combination of convenience and comfort, offering all your living needs in one space. The Room has a king size bed, unwind on the plush sofa for movie night with two 48-inch LCD TV, satellite/cable channels , then indulge in a relaxing bathtub soak before retreating under the covers to refresh — there’s plenty to explore in and around Randhoolee resort</p><br>

                                <h1 class="hdr-seven" style="text-align:left; font-size:13px;"> ROOM INCLUDES:</h1>
                                <p style="text-align:left; font-size:13px;">King Size Bed<br>Panoramic view of Mountains</p>
                                <ul class="accom-list" style="float:none !important;">
                                    <li>Tea and coffee making facilities</li>
                                    <li>Writing table</li>
                                    <li>Minibar</li>
                                    <li>Safety box</li>
                                    <li>220V electrical sockets</li>
                                    <li>Private Balcony</li>
                                    <li>Separate sitting area with a coffee table</li>
                                    <li>Wall to wall carpet</li>
                                </ul>
                                <br>
                                <h1 class="hdr-seven" style="text-align:left; font-size:13px;">Communication and Technology:</h1>
                                <ul class="accom-list" style="float:none !important;">
                                    <li>Free WiFi</li>
                                    <li>IDD Call</li>
                                </ul>
                                <br>
                                <h1 class="hdr-seven" style="text-align:left; font-size:13px;">Entertainment:</h1>
                                <ul class="accom-list" style="float:none !important;">
                                    <li>Two 48-inch LCD TVs</li>
                                    <li>Satellite/cable channels</li>
                                </ul>
                                <br>
                                <h1 class="hdr-seven" style="text-align:left; font-size:13px;">Bathroom Facilities:</h1>
                                <ul class="accom-list" style="float:none !important;">
                                    <li>Spacious s bathroom</li>
                                    <li>Jacuzzi bathtub and walk-in shower</li>
                                    <li>Hair dryer</li>
                                    <li>Shaver point</li>
                                    <li>Bathrobe and slippers</li>
                                </ul>
                                <br>
                                <h1 class="hdr-seven" style="text-align:left; font-size:13px;">Housekeeping and In-room services:</h1>
                                <ul class="accom-list" style="float:none !important;">
                                    <li>Complimentary bottled water</li>
                                    <li>Daily housekeeping service</li>
                                    <li>Fruit Basket and Chocolates</li>
                                    <li>DVD player on request</li>
                                    <li>Umbrella on request</li>
                                    <li>Ironing facilities on request</li>
                                </ul>
                                    
                                <div style="clear:both"></div>
                                <?php include 'inner_slider.php'; ?> 
                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->

            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_randolee.php'; ?> 
            </footer>    
    </body>
</html>
