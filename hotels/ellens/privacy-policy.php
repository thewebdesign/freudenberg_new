<?php 
$pg = ['property' => 'ellens', 'page' => 'privacy-policy'];
include '../../includes/header_ellens.php';
?>
    <body class="node-type-accommodation-list">
        <header id="header" role="banner">
            <h1 class="hide-visual">Ellen's Place | Facilities</h1>    
            <?php include '../../includes/navigation_ellens.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_firs.php'; ?>

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">

                            <!--      <div id="route">
                                    <breadcrumb class="menu">
                                        <li><a href="index.php">Home</a></li>
                                        <li><span class="arrow"> &gt; </span>Privacy Policy</li>
                                    </breadcrumb>
                                  </div>-->

                            <div class="ctatext-text">         
                                <?php require '../../includes/showdescription.php'; ?>
                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->

            <div style="clear:both"></div>
            <footer id="footer" role="contentinfo">  
                <?php include '../../includes/footer_ellens.php'; ?> 
            </footer>    
    </body>
</html>
