<style>.ctatext-wrapper{padding-top: 20px;padding-bottom: 20px;}</style>
<div id="nt-example1-container">
    <aside role="complementary">
        <div class="ctatext-buildadventure ctatext-wrapper" style="background:#f2f2f2;">
            <div class="col-md-12 ctatext-text">
                <div class="hdr-two" style="font-style: italic; color:#484848;">Guest Comments</div>
                <ul id="nt-example1">
                    <li>“Service is authentic and the rooms are spacious...”
                        <br><div style="font-weight:700; font-style:italic;text-indent:450px">-AvdM69, Thailand</div></li>

                    <li>“The Firs is an old bungalow with a lot of history behind...”
                        <br><div style="font-weight:700; font-style:italic;text-indent:450px">-McFeedy, Canada</div></li>

                    <li>“This is our favorite among all the places we stayed at in Sri Lanka...”
                        <br><div style="font-weight:700; font-style:italic;text-indent:450px">-Sroamatas, Mauritius</div></li>

                </ul>
            </div>
        </div>
    </aside>
</div>