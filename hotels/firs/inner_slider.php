<div class="slider-wrapper theme-default">
    <div id="slider" class="nivoSlider">
        <?php if (basename($_SERVER['PHP_SELF']) == 'bar.php') { ?>
            <div class="item"><img src="assets/images/sliders/facili/bar/inner/1.jpg"></div>
            <div class="item"><img src="assets/images/sliders/facili/bar/inner/2.jpg"></div>
            <div class="item"><img src="assets/images/sliders/facili/bar/inner/3.jpg"></div>
            <div class="item"><img src="assets/images/sliders/facili/bar/inner/4.jpg"></div>
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'restaurant.php') { ?>
            <div class="item"><img src="assets/images/sliders/facili/rest/inner/1.jpg"></div>
            <div class="item"><img src="assets/images/sliders/facili/rest/inner/2.jpg"></div>
            <div class="item"><img src="assets/images/sliders/facili/rest/inner/3.jpg"></div>
            <div class="item"><img src="assets/images/sliders/facili/rest/inner/6.jpg"></div>
            <div class="item"><img src="assets/images/sliders/facili/rest/inner/7.jpg"></div>
            <div class="item"><img src="assets/images/sliders/facili/rest/inner/5.jpg"></div>
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'room-services.php') { ?>
            <div class="item"><img src="assets/images/sliders/facili/room/inner/1.jpg"></div>
            <div class="item"><img src="assets/images/sliders/facili/room/inner/2.jpg"></div>
            <div class="item"><img src="assets/images/sliders/facili/room/inner/3.jpg"></div>
            <div class="item"><img src="assets/images/sliders/facili/room/inner/4.jpg"></div>
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'location.php') { ?>
            <div class="item"><img src="assets/images/sliders/location/1.jpg"></div>
            <div class="item"><img src="assets/images/sliders/location/2.jpg"></div>
            <div class="item"><img src="assets/images/sliders/location/3.jpg"></div>
            <div class="item"><img src="assets/images/sliders/location/4.jpg"></div>
        <?php } elseif (basename($_SERVER['PHP_SELF']) == 'accommodation.php') { ?>
            <div class="item"><img src="assets/images/sliders/acc/inner/1.jpg"></div>
            <div class="item"><img src="assets/images/sliders/acc/inner/2.jpg"></div>
            <div class="item"><img src="assets/images/sliders/acc/inner/3.jpg"></div>
            <div class="item"><img src="assets/images/sliders/acc/inner/4.jpg"></div>
            <div class="item"><img src="assets/images/sliders/acc/inner/5.jpg"></div>  
            <div class="item"><img src="assets/images/sliders/acc/inner/6.jpg"></div>
            <div class="item"><img src="assets/images/sliders/acc/inner/7.jpg"></div>
            <div class="item"><img src="assets/images/sliders/acc/inner/8.jpg"></div>  
            <?php } ?>
    </div>
</div>