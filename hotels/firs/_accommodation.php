﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_firs.php'; ?> 

    <body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-6 node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_firs.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_firs.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">

                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/acc_slider_1.jpg') no-repeat 50% 50%; background-size: cover;"></div>    
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/acc_slider_2.jpg') no-repeat 50% 50%; background-size: cover;"></div>      
                        <!--<div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/acc_slider_3.jpg') no-repeat 50% 50%; background-size: cover;"></div>-->
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/acc_slider_4.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/acc_slider_5.jpg') no-repeat 50% 50%; background-size: cover;"></div>
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/acc/acc_slider_6.jpg') no-repeat 50% 50%; background-size: cover;"></div>  
                    </div>

                    <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span>Accommodation</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">
                                <h1 class="hdr-seven">Five Tastefully - Furnished &amp; Upscale Suites</h1>          
                                <div class="hdr-two">Accommodation</div>          
                                <p style="text-align:justify; font-size:17px;">Accommodation options at The Firs consist of  five tastefully furnished and upscale suites that emanate old world charm and elegance. Exquisitely designed, each room reflects a sense of quiet comfort and an air of Richness that create an atmosphere of ultimate serenity and respite. Every minute detail has been delicately refined to suit you just right.<br><br>

                                    In-room amenities at The Firs include LCD televisions with satellite channels, room heaters, tea and coffee making facilities, as well as complimentary Wi-Fi Internet access. All suites are located on the ground floor and are capable of accommodating up to two or three people, with an option of adding an extra bed if necessary.</p> 

                                <h1 class="hdr-seven" style="text-align:left; font-size:14px; text-transform:none;">Each accommodation option at The Firs offers the following in-room amenities and services:</h1>

                                <ul class="accom-list">
                                    <li>Complimentary Mineral Water</li>
                                    <li>Room Service</li>
                                    <li>Laundry Service</li>
                                    <li>Daily Housekeeping</li>
                                    <li>In-room Dining</li>
                                    <!--<li>Complimentary Butler Service</li>-->
                                    <li>Spacious Wardrobe</li>
                                    <li>IDD Activated Telephones</li>
                                    <li>Room Heaters</li>
                                    <li>LCD Televisions</li>
                                    <li>Satellite Channels</li>
                                    <li>DVD Players</li>
                                    <li>Complimentary Newspaper</li>

                                </ul>
                                <ul class="accom-list">
                                    <!-- <li>Access to CD/DVD libraries</li> -->
                                    <li>Writing Table and Chair</li>
                                    <li>Bathroom Toiletries</li>
                                    <li>Jacuzzi</li>
                                    <li>Extra Blankets and Towels</li>
                                    <li>Iron and Board</li>
                                    <li>Extra Bed</li>
                                    <li>Baby Crib</li>
                                    <li>Tea and Coffee-Making Facilities</li>
                                    <li>Complimentary Wi-Fi Internet Access</li>
                                </ul> 

                                <div style="clear:both;"></div> 
                                <?php include 'inner_slider.php'; ?>

                                <div style="clear:both;"></div> 

                                <p style="text-align:justify; font-size:17px;">Bask in the opulence and grandeur of one of The Firs' magnificent Suites. Located on the ground floor, both Suites can accommodate up to 3 guests, with the option of adding an extra bed if necessary, and offer spectacular views of the surrounding tea estates and greenery in the distance. Guests may also take advantage of a number of in-room amenities and services including complimentary Wi-Fi internet access, personalised butler service and in-room dining.</p>

                                <h1 class="hdr-seven" style="text-align:left; font-size:14px; text-transform:none;">Suite options at The Firs include:</h1>

                                <ul class="accom-list">
                                    <li>The Stephan Suite</li>
                                    <li>The Molly Suite</li>
                                    <li>The Reinhart Room</li>
                                    <li>The Winfried Room</li>
                                    <li>The Siegmund Room</li>
                                </ul>

                                <?php /* ?><table style="width: 100%;" border="0" cellspacing="5" cellpadding="5">
                                  <tbody>
                                  <tr style="text-align: center; color: #333;" bgcolor="#909B71">
                                  <td width="110" height="50"><strong><span>Total Number</span></strong></td>
                                  <td style="text-align: center;" width="110"><strong>Location</strong></td>
                                  <td width="100"><strong>Occupancy</strong></td>
                                  <td width="120"><strong>Bed Arrangement</strong></td>
                                  <td width="100"><strong>Extra Bed</strong></td>
                                  <td width="200"><strong>Bathrooms</strong></td>
                                  </tr>
                                  <tr style="text-align: center;" bgcolor="#fff">
                                  <td height="100">Stephan Suite - 1<br>Molly Suite - 1</td>
                                  <td>&nbsp;Ground Floor</td>
                                  <td>2 Occupants;<br>Maximum 3</td>
                                  <td>King Bed</td>
                                  <td>&nbsp;Available</td>
                                  <td>Fully Equipped with Jacuzzi &amp; Bathroom Toiletries</td>
                                  </tr>
                                  <tr style="text-align: center; background-color: #fff;">
                                  <td style="height: 100px;">Reinhart - 1<br>Winfried - 1<br>Siegmund - 1</td>
                                  <td>&nbsp;Ground Floor</td>
                                  <td>2 Occupants;<br>Maximum 3</td>
                                  <td>Twin Bed / Double Bed</td>
                                  <td>&nbsp;Available</td>
                                  <td>Fully Equipped with Jacuzzi &amp; Bathroom Toiletries</td>
                                  </tr>
                                  </tbody>
                                  </table><?php */ ?>      
                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          

                        <!--  .highligh-panels  -->          

                        </aside>               
                    </article>   

                </main>  
                <?php include 'trip-advisor.php'; ?> 
            </div><!--  #node-details  -->
            <?php include '../../includes/footer_firs.php'; ?> 
        </div><!--  .blur  -->      
    </body>
</html>
