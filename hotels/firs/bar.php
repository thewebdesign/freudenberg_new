﻿<!DOCTYPE html>
<html class="no-js">
    <?php include '../../includes/header_firs.php'; ?> 

    <style>
        table {border:none !important; text-align: left !important; }
        table td {border:none !important;text-align: left !important;}
    </style>

    <body class="html not-front not-logged-in no-sidebars page-node page-node- page-node-6 node-type-accommodation-list">
        <header id="header" role="banner">
            <?php include '../../includes/navigation_firs.php'; ?> 
        </header><!--  #header  -->

        <?php include '../../includes/booking_firs.php'; ?> 

        <div class="blur">  

            <div id="node-6" class="node--accommodation_list mode--full">
                <aside role="complementary">

                    <div id="slidepanel" class="single-demo owl-carousel owl-theme">
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/facili/bar/bar_3.jpg') no-repeat 50% 50%; background-size: cover;"></div>    
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/facili/bar/bar_2.jpg') no-repeat 50% 50%; background-size: cover;"></div> 
                        <div class="tallpanelitem item" style="background: #ebebeb url('assets/images/sliders/facili/bar/bar_1.jpg') no-repeat 50% 50%; background-size: cover;"></div>     
                    </div>

                    <a href="#main" id="scroll-down" style="display: block;"><i class="fa fa-angle-down" style="font-size:50px; color:#fff"></i></a>
                </aside>  

                <div id="route">
                    <breadcrumb class="menu">
                        <li><a href="index.php">Home</a></li>
                        <li><span class="arrow"> &gt; </span><a href="facilities.php">Facilities</a></li>
                        <li><span class="arrow"> &gt; </span>Bar</li>
                    </breadcrumb>
                </div>

                <main id="main" role="main">
                    <article role="article">
                        <div class="ctatext-wrapper">
                            <div class="ctatext-text">         
                                <div class="hdr-two">Bar</div>          
                                <p style="text-align:justify; font-size:16px;">Sip on a tantalising cocktail as you sit back and unwind on a quiet evening at The Firs. Uniquely located in the hotel's attic, the bar is capable of accommodating up to 10 people and is the ideal venue to spend a few leisurely moments relaxing or chatting with other guests over a glass of wine. Choose from an extensive range of foreign and local liquors, as well as a array of the finest wines, champagnes and beers offered by The Firs. Adventurous guests are invited to sample the taste of arrack - a local alcoholic drink made from the fermented sap of coconut flowers.</p>  

                                <h1 class="hdr-seven" style="text-align:left; font-size:13px;">Bar Overview</h1>
                                <table style="width: 75%;" border="0" cellspacing="2" cellpadding="0">
                                    <tbody>
                                        <tr>
                                            <td><strong>Bar Capacity:</strong></td>
                                            <td align="right">10 Guests</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Location:</strong></td>
                                            <td align="right">Attic</td>
                                        </tr>
                                        <tr>
                                            <td><strong>Available Beverages:</strong></td>
                                            <td align="right">Foreign & Local Liquor, Beer and Wine</td>
                                        </tr>                
                                    </tbody>
                                </table>

                                <div style="clear:both;"></div> 
                                <?php include 'inner_slider.php'; ?>
                            </div><!--  .ctatext-text  -->
                        </div><!--  .ctatext-wrapper  -->                          
                    </article>      

                </main>   
            </div><!--  #node-details  -->
        </div>

        <div style="clear:both"></div>
        <?php include 'trip-advisor.php'; ?> 
        <footer id="footer" role="contentinfo">  
            <?php include '../../includes/footer_firs.php'; ?> 
        </footer>    
    </body>
</html>
